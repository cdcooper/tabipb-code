      MODULE treecode3d_procedures


C r8 is 8-byte (double precision) real

      INTEGER,PARAMETER :: r8=SELECTED_REAL_KIND(12)

C global variables for taylor expansions

      INTEGER :: torder,torderlim,torder2
      REAL(KIND=r8),ALLOCATABLE,DIMENSION(:) :: cf
      REAL(KIND=r8),ALLOCATABLE,DIMENSION(:) :: cf1,cf2,cf3
      REAL(KIND=r8),ALLOCATABLE,DIMENSION(:,:,:) :: a,b

C global variables to track tree levels 
 
      INTEGER :: minlevel,maxlevel
      
C global variables used when computing potential/force

      INTEGER :: orderoffset
      REAL(KIND=r8),DIMENSION(3) :: tarpos,tarq
C ######################################      
      REAL(KIND=r8) :: tarchr,peng_old(2)
      REAL(KIND=r8),ALLOCATABLE,DIMENSION(:,:)::peng_old_arr
      REAL(KIND=r8),ALLOCATABLE,DIMENSION(:),TARGET::peng1,peng2,m1,m2

C ######################################

C global variables for position and charge storage 
C NOTE: arrays ARE NOT COPIED in this version!!  orderarr is still valid

      INTEGER,ALLOCATABLE,DIMENSION(:)  :: orderarr
      REAL(KIND=r8),ALLOCATABLE,DIMENSION(:) :: xcopy,ycopy,zcopy,qcopy

C node pointer and node type declarations

      TYPE tnode_pointer
           TYPE(tnode), POINTER :: p_to_tnode
      END TYPE tnode_pointer
      TYPE tnode
           INTEGER          :: numpar,ibeg,iend
           REAL(KIND=r8)    :: x_min,y_min,z_min
           REAL(KIND=r8)    :: x_max,y_max,z_max
           REAL(KIND=r8)    :: x_mid,y_mid,z_mid
           REAL(KIND=r8)    :: radius,aspect
           INTEGER          :: level,num_children,exist_ms,cell_id
           REAL(KIND=r8),DIMENSION(:,:,:,:),POINTER :: ms
           TYPE(tnode_pointer), DIMENSION(8) :: child
      END TYPE tnode
C#######################################
      TYPE(tnode), POINTER::troot
C#######################################
      CONTAINS

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE SETUP(x,y,z,q,numpars,order,iflag,xyzminmax)
      IMPLICIT NONE
C
C SETUP allocates and initializes arrays needed for the Taylor expansion.
C Also, global variables are set and the Cartesian coordinates of
C the smallest box containing the particles is determined. The particle
C postions and charges are copied so that they can be restored upon exit.
C
      INTEGER,INTENT(IN) :: numpars,order,iflag
      REAL(KIND=r8),DIMENSION(numpars),INTENT(IN) :: x,y,z,q
      REAL(KIND=r8),INTENT(INOUT),DIMENSION(6) :: xyzminmax

C local variables

      INTEGER :: err,i,j,k
      REAL(KIND=r8) :: t1

C global integers and reals:  TORDER, TORDERLIM and THETASQ
C############################################################      
      torder = order+2
      torder2=order
C############################################################
      IF (iflag .EQ. 1) THEN
          orderoffset=0
      ELSE
          orderoffset = 1
      END IF
      torderlim = torder+orderoffset

C allocate global Taylor expansion variables

      ALLOCATE(cf(0:torder), STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error allocationg Taylor variables! '
         STOP
      END IF

      ALLOCATE(cf1(torderlim),cf2(torderlim),cf3(torderlim),STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error allocating Taylor variables! '
         STOP
      END IF

      ALLOCATE(a(-2:torderlim,-2:torderlim,-2:torderlim),
     &         b(-2:torderlim,-2:torderlim,-2:torderlim),STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error allocating Taylor variables! '
         STOP
      END IF

      a=0.0_r8
      b=0.0_r8

      DO i=0,torder
         cf(i) = REAL(i,KIND=r8)+1.0_r8
      END DO

      DO i=1,torderlim
         t1=1.0_r8/REAL(i,KIND=r8)
         cf1(i)=t1
         cf2(i)=1.0_r8-0.5_r8*t1
         cf3(i)=1.0_r8-t1
      END DO

C find bounds of Cartesian box enclosing the particles

      xyzminmax(1)=MINVAL(x(1:numpars))
      xyzminmax(2)=MAXVAL(x(1:numpars))
      xyzminmax(3)=MINVAL(y(1:numpars))
      xyzminmax(4)=MAXVAL(y(1:numpars))
      xyzminmax(5)=MINVAL(z(1:numpars))
      xyzminmax(6)=MAXVAL(z(1:numpars))

C######################################################### 
      ALLOCATE(orderarr(numpars),STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error allocating copy variables! '
         STOP
      END IF

      DO i=1,numpars
         orderarr(i)=i
      END DO  

      ALLOCATE(peng_old_arr(2,numpars),peng1(numpars),peng2(numpars),
     &         m1(numpars), m2(numpars), STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error peng_old_arr! '
         STOP
      END IF

      RETURN
      END SUBROUTINE SETUP

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      RECURSIVE SUBROUTINE CREATE_TREE(p,ibeg,iend,x,y,z,q,maxparnode,
     &                                 xyzmm,level,numpars)
      IMPLICIT NONE
C
C CREATE_TREE recursively create the tree structure. Node P is
C input, which contains particles indexed from IBEG to IEND. After
C the node parameters are set subdivision occurs if IEND-IBEG+1 > MAXPARNODE.
C Real array XYZMM contains the min and max values of the coordinates
C of the particle in P, thus defining the box.   
C
      TYPE(tnode),POINTER :: p
      INTEGER,INTENT(IN) :: ibeg,iend,level,maxparnode,numpars
      REAL(KIND=r8),DIMENSION(numpars),INTENT(INOUT) :: x,y,z,q
      REAL(KIND=r8),DIMENSION(6),INTENT(IN) :: xyzmm

C local variables

      REAL(KIND=r8) :: x_mid,y_mid,z_mid,xl,yl,zl,lmax,t1,t2,t3
      INTEGER, DIMENSION(8,2) :: ind
      REAL(KIND=r8), DIMENSION(6,8) :: xyzmms
      INTEGER :: i,j,limin,limax,err,loclev,numposchild
      REAL(KIND=r8), DIMENSION(6) ::  lxyzmm
      
C allocate pointer 

      ALLOCATE(p,STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error allocating pointer! '
         STOP
      END IF

	
C set node fields: number of particles, exist_ms
C and xyz bounds

      p%numpar=iend-ibeg+1
      p%exist_ms=0
      p%cell_id=0

      p%x_min=xyzmm(1)
      p%x_max=xyzmm(2)
      p%y_min=xyzmm(3)
      p%y_max=xyzmm(4)
      p%z_min=xyzmm(5)
      p%z_max=xyzmm(6)        
C compute aspect ratio

      xl=p%x_max-p%x_min
      yl=p%y_max-p%y_min
      zl=p%z_max-p%z_min

      lmax=MAX(xl,yl,zl)
      t1=lmax
      t2=MIN(xl,yl,zl)
      IF (t2 .NE. 0.0_r8) THEN
         p%aspect=t1/t2
      ELSE
         p%aspect=0.0_r8
      END IF

C midpoint coordinates , RADIUS and SQRADIUS 

      p%x_mid=(p%x_max+p%x_min)/2.0_r8
      p%y_mid=(p%y_max+p%y_min)/2.0_r8
      p%z_mid=(p%z_max+p%z_min)/2.0_r8
      t1=p%x_max-p%x_mid
      t2=p%y_max-p%y_mid
      t3=p%z_max-p%z_mid
      p%radius=SQRT(t1*t1+t2*t2+t3*t3)

C set particle limits, tree level of node, and nullify children pointers

      p%ibeg=ibeg
      p%iend=iend
      p%level=level
      IF (maxlevel .LT. level) THEN
         maxlevel=level
      END IF
      p%num_children=0
      DO i=1,8
         NULLIFY(p%child(i)%p_to_tnode)
      END DO

      IF (p%numpar .GT. maxparnode) THEN
C
C set IND array to 0 and then call PARTITION routine.  IND array holds indices
C of the eight new subregions. Also, setup XYZMMS array in case SHRINK=1
C
         xyzmms(1,1)=p%x_min
         xyzmms(2,1)=p%x_max
         xyzmms(3,1)=p%y_min
         xyzmms(4,1)=p%y_max
         xyzmms(5,1)=p%z_min
         xyzmms(6,1)=p%z_max
         ind=0 !Weihua
         ind(1,1)=ibeg
         ind(1,2)=iend
         x_mid=p%x_mid
         y_mid=p%y_mid
         z_mid=p%z_mid

         CALL PARTITION_8(x,y,z,q,xyzmms,xl,yl,zl,lmax,numposchild,
     &                    x_mid,y_mid,z_mid,ind,numpars)

C########################################################
C Shrink the box
C      if (1==2) then
         do i=1,8
             if (ind(i,1) < ind(i,2)) then
                 xyzmms(1,i)=minval(x(ind(i,1):ind(i,2)))
                 xyzmms(2,i)=maxval(x(ind(i,1):ind(i,2)))
                 xyzmms(3,i)=minval(y(ind(i,1):ind(i,2)))
                 xyzmms(4,i)=maxval(y(ind(i,1):ind(i,2)))
                 xyzmms(5,i)=minval(z(ind(i,1):ind(i,2)))
                 xyzmms(6,i)=maxval(z(ind(i,1):ind(i,2)))
             endif       
         end do
C      endif
C########################################################
C
C create children if indicated and store info in parent
C
         loclev=level+1
         DO i=1,numposchild
            IF (ind(i,1) .LE. ind(i,2)) THEN
               p%num_children=p%num_children+1
               lxyzmm=xyzmms(:,i)
               CALL CREATE_TREE(p%child(p%num_children)%p_to_tnode,
     &                          ind(i,1),ind(i,2),x,y,z,q,
     &                          maxparnode,lxyzmm,loclev,numpars)
            END IF
         END DO
      ELSE
         IF (level .LT. minlevel) THEN
            minlevel=level
         END IF
      END IF   

      END SUBROUTINE CREATE_TREE      

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE PARTITION_8(x,y,z,q,xyzmms,xl,yl,zl,lmax,numposchild,
     &                       x_mid,y_mid,z_mid,ind,numpars)
      IMPLICIT NONE
C
C PARTITION_8 determines the particle indices of the eight sub boxes
C containing the particles after the box defined by particles I_BEG
C to I_END is divided by its midpoints in each coordinate direction.
C The determination of the indices is accomplished by the subroutine
C PARTITION. A box is divided in a coordinate direction as long as the
C resulting aspect ratio is not too large. This avoids the creation of
C "narrow" boxes in which Talyor expansions may become inefficient.
C On exit the INTEGER array IND (dimension 8 x 2) contains
C the indice limits of each new box (node) and NUMPOSCHILD the number 
C of possible children.  If IND(J,1) > IND(J,2) for a given J this indicates
C that box J is empty.
C
      INTEGER, INTENT(IN) :: numpars
      REAL(KIND=r8),DIMENSION(numpars),INTENT(INOUT) :: x,y,z,q
      INTEGER, DIMENSION(8,2),INTENT(INOUT) :: ind
      REAL(KIND=r8),DIMENSION(6,8),INTENT(INOUT) :: xyzmms
      REAL(KIND=r8), INTENT(IN) :: x_mid,y_mid,z_mid,xl,yl,zl,lmax
      INTEGER,INTENT(INOUT) :: numposchild

C local variables

      INTEGER :: temp_ind,i
      REAL(KIND=r8) :: critlen

      numposchild=1
      critlen=lmax/sqrt(2.0_r8)

      IF (xl .GE. critlen) THEN
         CALL PARTITION(x,y,z,q,orderarr,ind(1,1),ind(1,2),
     &                  x_mid,temp_ind,numpars)
         ind(2,1)=temp_ind+1
         ind(2,2)=ind(1,2)
         ind(1,2)=temp_ind
         xyzmms(:,2)=xyzmms(:,1)
         xyzmms(2,1)=x_mid
         xyzmms(1,2)=x_mid
         numposchild=2*numposchild
      END IF 
 
      IF (yl .GE. critlen) THEN
         DO i=1,numposchild
            CALL PARTITION(y,x,z,q,orderarr,ind(i,1),ind(i,2),
     &                     y_mid,temp_ind,numpars)
            ind(numposchild+i,1)=temp_ind+1
            ind(numposchild+i,2)=ind(i,2)
            ind(i,2)=temp_ind
            xyzmms(:,numposchild+i)=xyzmms(:,i)
            xyzmms(4,i)=y_mid
            xyzmms(3,numposchild+i)=y_mid
         END DO
         numposchild=2*numposchild
      END IF

      IF (zl .GE. critlen) THEN
         DO i=1,numposchild
            CALL PARTITION(z,x,y,q,orderarr,ind(i,1),ind(i,2),
     &                     z_mid,temp_ind,numpars)
            ind(numposchild+i,1)=temp_ind+1
            ind(numposchild+i,2)=ind(i,2)
            ind(i,2)=temp_ind
            xyzmms(:,numposchild+i)=xyzmms(:,i)
            xyzmms(6,i)=z_mid
            xyzmms(5,numposchild+i)=z_mid
         END DO
         numposchild=2*numposchild
      END IF

      RETURN 
      END SUBROUTINE PARTITION_8

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      RECURSIVE SUBROUTINE FIND_TWIGS(p, i_twig, twig_beg, 
     &                              twig_end, twig_ctr, twig_rad, Ntwig)
C
C     This subroutine finds the twigs and places the begin and end 
C     indices in the x, y, z array into twig_beg and twig_end
C
      IMPLICIT NONE

      INTEGER :: i_twig, Ntwig, i
      INTEGER,DIMENSION(Ntwig),INTENT(INOUT) :: twig_beg, twig_end
      REAL*8, dimension(3,Ntwig), intent(inout):: twig_ctr
      REAL*8, dimension(Ntwig), intent(inout):: twig_rad
      TYPE(tnode),POINTER :: p   

      IF (p%num_children .EQ. 0) THEN
        twig_beg(i_twig) = p%ibeg
        twig_end(i_twig) = p%iend
        twig_ctr(1,i_twig) = p%x_mid
        twig_ctr(2,i_twig) = p%y_mid
        twig_ctr(3,i_twig) = p%z_mid
        twig_rad(i_twig) = p%radius
        i_twig = i_twig + 1
      ELSE
        DO i=1,p%num_children
          CALL FIND_TWIGS(p%child(i)%p_to_tnode,i_twig, 
     &                  twig_beg, twig_end, twig_ctr,twig_rad,Ntwig)
        END DO  
      END IF 

      RETURN
      END SUBROUTINE FIND_TWIGS
      
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      RECURSIVE SUBROUTINE COUNT_TWIGS(p,Ntwig)
C
C     This subroutine counts the number of tree twigs 
C     (nodes with less than NCRIT particles)
C
      IMPLICIT NONE

      INTEGER :: Ntwig, i
      TYPE(tnode),POINTER :: p   

      IF (p%num_children .EQ. 0) THEN
        Ntwig = Ntwig + 1
      ELSE
        DO i=1,p%num_children
          CALL COUNT_TWIGS(p%child(i)%p_to_tnode,Ntwig)
        END DO  
      END IF 

      RETURN
      END SUBROUTINE COUNT_TWIGS

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      

      RECURSIVE SUBROUTINE COMPUTE_VECTORIZED_INTERACTIONS(p,peng,
     & cell_beg,cell_end,cell_ctr,cell_rad,x,y,z,
     & q,tpoten,kappa,theta,numpars,kk,eps,der_cof)
C
C     This subroutine computes the treecode interaction for all particles
C     inside a single twig, and saves it in peng
C
      use treecode, only: tr_area,tr_q,tchg
      IMPLICIT NONE

      INTEGER,INTENT(IN) :: numpars,kk(3,16),cell_beg, cell_end
      TYPE(tnode),POINTER :: p   
      REAL(KIND=r8) :: cell_ctr(3), temp_area
      REAL(KIND=r8),DIMENSION(2,numpars),INTENT(INOUT)::peng
      REAL(KIND=r8),DIMENSION(numpars),INTENT(INOUT) :: x,y,z
      REAL(KIND=r8),DIMENSION(numpars,16,2),INTENT(IN) :: q
      REAL(KIND=r8),DIMENSION(2*numpars),INTENT(IN) :: tpoten
      REAL(KIND=r8),INTENT(IN):: kappa,theta,eps
      REAL(KIND=r8) ::tempq(16,2)
      REAL(KIND=r8),INTENT(IN):: 
     & der_cof(0:torder2,0:torder2,0:torder2,16)
      REAL(KIND=r8),INTENT(IN):: cell_rad

C local variables

      REAL(KIND=r8) :: tx,ty,tz,dist,penglocal(2),kapa(2)
      real(kind=r8) :: SL(4),pt_comp(16,2)
      INTEGER :: i,j,k,ijk(3),ikp,indx,err

C determine DISTSQ for MAC test
      tx=p%x_mid-cell_ctr(1)
      ty=p%y_mid-cell_ctr(2)
      tz=p%z_mid-cell_ctr(3)
      dist=SQRT(tx*tx+ty*ty+tz*tz)

C Check MAC
      IF (p%radius .LT. (dist)*theta) THEN

C       Compute moment if required
        IF (p%exist_ms .EQ. 0) THEN
          ALLOCATE(p%ms(16,0:torder,0:torder,0:torder),STAT=err)

          IF (err .NE. 0) THEN
            WRITE(6,*) 'Error allocating node moments! '
            STOP
          END IF

        CALL COMP_MS(p,x,y,z,q(:,:,1),numpars)

        p%exist_ms=1
        END IF   
       
        DO i=cell_beg,cell_end
          tarpos(1) = x(i)  ! tarpos is globally defined (this has to change)
          tarpos(2) = y(i)     
          tarpos(3) = z(i)     
          tarq=tr_q(:,i)
          tempq=tchg(i,:,:)

          penglocal = 0.0_r8
          CALL COMPP_TREE_PB(kk,p,penglocal,
     &                       kappa,theta,eps,tempq,der_cof)
          peng(:,i) = peng(:,i) + penglocal
        ENDDO

      ELSE

C If MAC fails check to see if there are children. If not, perform direct 
C calculation.  If there are children, call routine recursively for each.
C
         IF (p%num_children .EQ. 0) THEN
            DO i=cell_beg,cell_end
              tarpos(1) = x(i)  ! tarpos is globally defined 
              tarpos(2) = y(i)  ! (this has to change)   
              tarpos(3) = z(i)  
              tarq=tr_q(:,i)
            
              x(i) = x(i) + 1.23456 ! Remove singularity
              temp_area = tr_area(i)
              tr_area(i)=0.d0
                
              penglocal = 0.0_r8
              CALL COMPP_DIRECT_PB(penglocal,p%ibeg,p%iend,
     &                        x,y,z,tpoten,kappa,numpars,eps)
              peng(:,i) = peng(:,i) + penglocal

              x(i) = x(i) - 1.23456  ! Restore singularity
              tr_area(i) = temp_area
            ENDDO
         ELSE
            DO i=1,p%num_children
               CALL COMPUTE_VECTORIZED_INTERACTIONS(
     &          p%child(i)%p_to_tnode,peng,cell_beg,cell_end,cell_ctr,
     &          cell_rad,x,y,z,q,tpoten,kappa,theta,numpars,kk,eps,
     &          der_cof)
            END DO  
         END IF 
      END IF

      RETURN
      END SUBROUTINE COMPUTE_VECTORIZED_INTERACTIONS

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      
      
      RECURSIVE SUBROUTINE INTERACTION_LIST(p, cell_ctr, theta,
     &                              direct_long, cluster_long, n_direct,
     &                              n_cluster, cluster_center_long,
     &                              cluster_beg_long, Ntwig,Ncell_inter)
C
C    This subroutine computes the interaction list for one twig and 
C    saves it in direct_long and cluster_long, with the offsets to the
C    beginning of the interaction list of each twig in direct_off and
C    cluster_off (in INTERACTION_LIST_DRIVER)
C
      IMPLICIT NONE

      TYPE(tnode),POINTER :: p   
      REAL(KIND=r8) :: cell_ctr(3), dist, tx, ty, tz
      INTEGER :: Ntwig, i
      INTEGER,INTENT(INOUT):: n_direct, n_cluster, Ncell_inter
      INTEGER,DIMENSION(2,Ntwig*Ntwig),INTENT(INOUT):: direct_long
      INTEGER,DIMENSION(Ntwig*Ntwig),INTENT(INOUT):: cluster_long
      REAL(KIND=r8),DIMENSION(3,Ntwig*Ntwig),INTENT(INOUT)::  
     &                                      cluster_center_long
      INTEGER,DIMENSION(2,Ntwig*Ntwig),INTENT(INOUT):: cluster_beg_long
      REAL(KIND=r8),INTENT(IN):: theta


C determine DISTSQ for MAC test
      tx=p%x_mid-cell_ctr(1)
      ty=p%y_mid-cell_ctr(2)
      tz=p%z_mid-cell_ctr(3)
      dist=SQRT(tx*tx+ty*ty+tz*tz)

C Check MAC
      IF (p%radius .LT. (dist)*theta) THEN
        
        IF (p%cell_id .EQ. 0) THEN
          Ncell_inter = Ncell_inter + 1
          p%cell_id = Ncell_inter
          cluster_center_long(1,Ncell_inter) = p%x_mid
          cluster_center_long(2,Ncell_inter) = p%y_mid
          cluster_center_long(3,Ncell_inter) = p%z_mid
          cluster_beg_long(1,Ncell_inter) = p%ibeg
          cluster_beg_long(2,Ncell_inter) = p%iend
        END IF

        cluster_long(n_cluster) = p%cell_id
        n_cluster = n_cluster + 1

      ELSE

C If MAC fails check to see if there are children. If not, save in direct_long
C
         IF (p%num_children .EQ. 0) THEN
          direct_long(1,n_direct) = p%ibeg 
          direct_long(2,n_direct) = p%iend
          n_direct = n_direct + 1
         ELSE
            DO i=1,p%num_children
               CALL INTERACTION_LIST(
     &          p%child(i)%p_to_tnode,cell_ctr, theta,
     &          direct_long, cluster_long, n_direct, n_cluster, 
     &          cluster_center_long, cluster_beg_long, 
     &          Ntwig, Ncell_inter)
            END DO  
         END IF 
      END IF

      RETURN

      END SUBROUTINE INTERACTION_LIST
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      

      SUBROUTINE VECTORIZED_TREECODE(troot, kappa, eps, tpoten)
      use treecode
C
C     This subroutine loops over twigs and calls the treecode for each twig
C     and saves the result in peng
C
      
      IMPLICIT NONE

      INTEGER :: i,j,cell_beg,cell_end,k,l,m
      REAL(KIND=r8),DIMENSION(2*numpars) :: tpoten, tpoten_old
      TYPE(tnode),POINTER :: troot
      REAL*8, dimension(2,numpars):: peng
      REAL(KIND=r8) :: cell_ctr(3),cell_rad,pre1,pre2
      REAL(KIND=r8),INTENT(IN):: kappa,eps

C     Initialize
      peng = 0.0_r8

      pre1=0.5d0*(1.d0+eps)
      pre2=0.5d0*(1.d0+1.d0/eps)
      tpoten_old=tpoten
      call pb_kernel(tpoten_old)
      peng_old_arr(1,:)=tpoten_old(:numpars)
      peng_old_arr(2,:)=tpoten_old(numpars+1:)
      DO i=1,Ntwig
        cell_beg = twig_beg(i)
        cell_end = twig_end(i)
        cell_ctr = twig_ctr(:,i)
        cell_rad = twig_rad(i)
        CALL COMPUTE_VECTORIZED_INTERACTIONS(troot, peng, cell_beg, 
     &                  cell_end, cell_ctr, cell_rad, x, y, z, schg,
     &                  tpoten_old,kappa,theta,numpars,kk,eps,
     &                  der_cof)
      ENDDO

      tpoten(:numpars)=pre1*peng_old_arr(1,:)-peng(1,:)
      tpoten(numpars+1:)=pre2*peng_old_arr(2,:)-peng(2,:)

      END SUBROUTINE VECTORIZED_TREECODE

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      
      SUBROUTINE GPU_TREECODE(troot, kappa, eps, tpoten)
      use treecode
C
C     This subroutine loops over twigs and calls the treecode for each twig
C     and saves the result in peng
C
      
      IMPLICIT NONE

      INTEGER :: i,j,cell_beg,cell_end,k,l,m,err,tt
      REAL(KIND=r8),DIMENSION(2*numpars) :: tpoten, tpoten_old
      TYPE(tnode),POINTER :: troot
      REAL*8, dimension(2,numpars):: peng
      REAL(KIND=r8) :: cell_ctr(3),cell_rad,pre1,pre2
      REAL(KIND=r8),INTENT(IN):: kappa,eps

C     Initialize
      peng = 0.0_r8

      pre1=0.5d0*(1.d0+eps)
      pre2=0.5d0*(1.d0+1.d0/eps)
      tpoten_old=tpoten
      call pb_kernel(tpoten_old)

      DO i=1,16
        DO j=1,numpars
          tchg1((i-1)*numpars+j) = tchg(j,i,1)
          tchg2((i-1)*numpars+j) = tchg(j,i,2)
        ENDDO
      ENDDO

      peng_old_arr(1,:)=tpoten_old(:numpars)
      peng_old_arr(2,:)=tpoten_old(numpars+1:)

      call CUDA_MEMCPY_HTD_REAL(tchg1_d, tchg1, 
     &                          16*numpars*sizeof(tchg1(1)))
      call CUDA_MEMCPY_HTD_REAL(tchg2_d, tchg2, 
     &                          16*numpars*sizeof(tchg2(1)))

      CALL COMPUTE_MOMENTS_PACKED(cluster_center_pack,
     &  cluster_beg_list,x,y,z,schg,numpars,Ncell_inter,n_cluster,
     &  cluster_list,cluster_ms_pack,index_3Dto1Dmap,Nm)

      DO i=1,16
        DO j=1,Nm*(n_cluster-1)
            cluster_ms_pack_1D((i-1)*Nm*(n_cluster-1) + j) 
     &                                  = cluster_ms_pack(i,j)
        ENDDO
      ENDDO

      call CUDA_MEMCPY_HTD_REAL(cluster_ms_d, cluster_ms_pack_1D, 
     &                                  16*Nm*(n_cluster-1)
     &                                  *sizeof(cluster_ms_pack_1D(1)))

C      CALL  COMPUTE_CLUSTER_PACKED(x,y,z,peng,cluster_off,
C     &          twig_beg, twig_end, cluster_center_pack,cluster_ms_pack,
C     &          index_3Dto1Dmap,index_1Dto3Dmap, kappa, theta, Ntwig, 
C     &          Ncell_inter, n_cluster, der_cof_1D, kk, eps, Nm)

      peng1(:) = peng(1,:)
      peng2(:) = peng(2,:)
      m1(:) = peng_old_arr(1,:)
      m2(:) = peng_old_arr(2,:)

      call CUDA_MEMCPY_HTD_REAL(peng1_d, peng1, 
     &                          numpars*sizeof(peng1(1)))
      call CUDA_MEMCPY_HTD_REAL(peng2_d, peng2,
     &                           numpars*sizeof(peng2(1)))
      call CUDA_MEMCPY_HTD_REAL(m1_d, m1, numpars*sizeof(m1(1)))
      call CUDA_MEMCPY_HTD_REAL(m2_d, m2, numpars*sizeof(m2(1)))


      call cluster_gpu(peng1_d, peng2_d, twig_beg_d, sizeTar_d,
     &          cluster_off_d, cluster_center_x_d, cluster_center_y_d,
     &          cluster_center_z_d, cluster_ms_d, x_d, y_d, z_d, 
     &          kk1_d, kk2_d, kk3_d, tchg1_d, tchg2_d,
     &          index_1Dx_d, index_1Dy_d, index_1Dz_d, index_3Dto1D_d, 
     &          der_cof_1D_d, kappa, Ntwig, eps, maxparnode, torder2,  
     &          Nm, numpars, n_cluster, coeff_lap_d, coeff_yuk_d, b_d)

      call direct_gpu(peng1_d, peng2_d, direct_off_d, direct_list_beg_d,
     &          direct_list_end_d, sizeTar_d, twig_beg_d, x_d, y_d, z_d,
     &          m1_d, m2_d, nx_d, ny_d, nz_d, Area_d, numpars, kappa, 
     &          eps, maxparnode, Ntwig)

      call CUDA_MEMCPY_DTH_REAL(peng1, peng1_d, 
     &                          numpars*sizeof(peng1(1)))
      call CUDA_MEMCPY_DTH_REAL(peng2, peng2_d, 
     &                          numpars*sizeof(peng2(1)))

      tpoten(:numpars)=pre1*peng_old_arr(1,:)-peng1
      tpoten(numpars+1:)=pre2*peng_old_arr(2,:)-peng2

      END SUBROUTINE GPU_TREECODE

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      
      SUBROUTINE PRECOMPUTED_LIST_TREECODE(troot, kappa, eps, tpoten)
      use treecode
C
C     This subroutine loops over twigs and calls the treecode for each twig
C     and saves the result in peng
C
      
      IMPLICIT NONE

      INTEGER :: i,j,cell_beg,cell_end,k,l,m,err,tt
      REAL(KIND=r8),DIMENSION(2*numpars) :: tpoten, tpoten_old
      TYPE(tnode),POINTER :: troot
      REAL*8, dimension(2,numpars):: peng
      REAL(KIND=r8) :: cell_ctr(3),cell_rad,pre1,pre2
      REAL(KIND=r8),INTENT(IN):: kappa,eps

C     Initialize
      peng = 0.0_r8

      pre1=0.5d0*(1.d0+eps)
      pre2=0.5d0*(1.d0+1.d0/eps)
      tpoten_old=tpoten
      call pb_kernel(tpoten_old)
      peng_old_arr(1,:)=tpoten_old(:numpars)
      peng_old_arr(2,:)=tpoten_old(numpars+1:)

      CALL COMPUTE_MOMENTS_VECTORIZED(cluster_center_list,
     &  cluster_beg_list,x,y,z,schg,numpars,Ncell_inter,cluster_ms_beg)

      CALL  COMPUTE_CLUSTER_INTERACTION(x,y,z,peng,cluster_off,
     &          twig_beg, twig_end, cluster_list, cluster_center_list,
     &          cluster_ms_beg, kappa, theta, Ntwig, Ncell_inter, 
     &          n_cluster, der_cof, kk, eps)

      CALL COMPUTE_DIRECT_VECTORIZED(x,y,z,twig_beg,twig_end,peng,
     &                          direct_list, direct_off, Ntwig, 
     &                          tpoten_old, kappa, numpars, eps)

      tpoten(:numpars)=pre1*peng_old_arr(1,:)-peng(1,:)
      tpoten(numpars+1:)=pre2*peng_old_arr(2,:)-peng(2,:)

      END SUBROUTINE PRECOMPUTED_LIST_TREECODE


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      

      SUBROUTINE INTERACTION_LIST_DRIVER(troot)
      use treecode
C
C     This subroutine loops over twigs and calls the interaction 
C     calculation for each.
C     direct_list : direct interaction list
C     cluster_list: cluster-particle interaction list
C     direct_off  : offset to direct_list for each twig
C     cluster_off : offset to cluster_list for each twig
C     sizeTar     : number of target particles per twig
C     all these variables are defined in the 'treecode' module (need to
C     change this).
C
      
      IMPLICIT NONE

      INTEGER :: i, err
      TYPE(tnode),POINTER :: troot
      REAL(KIND=r8) :: cell_ctr(3)

C     Local arrays
      INTEGER,DIMENSION(2,Ntwig*Ntwig):: direct_long, cluster_beg_long
      INTEGER,DIMENSION(Ntwig*Ntwig):: cluster_long
      REAL(KIND=r8),DIMENSION(3,Ntwig*Ntwig) :: cluster_center_long
      
      ALLOCATE(direct_off(Ntwig+1),cluster_off(Ntwig+1),STAT=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating offset arrays!'
        STOP
      END IF

      n_direct = 1
      n_cluster = 1
      Ncell_inter = 0
      direct_off(1) = 1
      cluster_off(1) = 1
      DO i=1,Ntwig
        sizeTar(i) = twig_end(i)-twig_beg(i) + 1
        cell_ctr = twig_ctr(:,i)
        CALL INTERACTION_LIST(troot, cell_ctr, theta, direct_long, 
     &          cluster_long,n_direct, n_cluster,
     &          cluster_center_long, cluster_beg_long, 
     &          Ntwig, Ncell_inter)
        direct_off(i+1) = n_direct  ! Start of list for NEXT twig
        cluster_off(i+1) = n_cluster! Start of list for NEXT twig
      ENDDO
      

      ALLOCATE(direct_list(2,n_direct-1),cluster_list(n_cluster-1),
     &  direct_list_beg(n_direct-1), direct_list_end(n_direct-1), 
     &  STAT=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating interaction list arrays!'
        STOP
      END IF

      direct_list = direct_long(:,:n_direct-1)
      cluster_list = cluster_long(:n_cluster-1)

      direct_list_beg = direct_list(1,:)
      direct_list_end = direct_list(2,:)

      ALLOCATE(cluster_center_list(3,Ncell_inter), STAT=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating sorted cluster center arrays!'
        STOP
      END IF

      ALLOCATE(cluster_center_pack(3,n_cluster-1),
     &          cluster_center_x(n_cluster-1),
     &          cluster_center_y(n_cluster-1),
     &          cluster_center_z(n_cluster-1), STAT=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating packed cluster center arrays!'
        STOP
      END IF

      ALLOCATE(cluster_beg_list(2,Ncell_inter), STAT=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating cluster interaction list!'
        STOP
      END IF

      cluster_center_list = cluster_center_long(:,:Ncell_inter)
      cluster_beg_list = cluster_beg_long(:,:Ncell_inter)

      ALLOCATE(cluster_ms_beg(Ncell_inter,16,
     &          0:torder,0:torder,0:torder))
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating sorted moments array!'
        STOP
      END IF

      ALLOCATE(exist_cell_ms(Ncell_inter))
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating sorted moments array!'
        STOP
      END IF

C     Save cluster centers in a packed array (that repeats data storage)
      DO i=1,(n_cluster-1)
        cluster_center_pack(:,i) = cluster_center_list(:,
     &                                     cluster_list(i))
        cluster_center_x(i) = cluster_center_pack(1,i)
        cluster_center_y(i) = cluster_center_pack(2,i)
        cluster_center_z(i) = cluster_center_pack(3,i)
      ENDDO

      CALL GENERATE_INDEX_MAP(torder2) 

      ALLOCATE(cluster_ms_pack_1D(16*(n_cluster-1)*Nm), stat=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating packed moments array!'
        STOP
      END IF

      ALLOCATE(cluster_ms_pack(16,(n_cluster-1)*Nm), stat=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating packed moments array!'
        STOP
      END IF
    
      END SUBROUTINE INTERACTION_LIST_DRIVER 

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      

      SUBROUTINE COMPUTE_DIRECT_VECTORIZED(x,y,z,twig_beg,twig_end,peng,
     &                          direct_list, direct_off, Ntwig, 
     &                          tpoten, kappa, numpars, eps)
      
      use treecode, only: tr_area,tr_q,tchg, n_direct

      IMPLICIT NONE

      INTEGER:: t, i, j, list_beg, list_end, p_ibeg, p_iend,
     &          target_twig_beg, target_twig_end, Ntwig, numpars
      REAL*8:: penglocal(2), temp_area, eps, kappa
      INTEGER,DIMENSION(Ntwig),INTENT(IN):: twig_beg, twig_end
      INTEGER,DIMENSION(Ntwig+1),INTENT(IN):: direct_off
      INTEGER,DIMENSION(2,n_direct),INTENT(IN):: direct_list
      REAL*8,DIMENSION(numpars),INTENT(INOUT):: x,y,z
      REAL(KIND=r8),DIMENSION(2,numpars),INTENT(INOUT)::peng
      REAL(KIND=r8),DIMENSION(2*numpars) :: tpoten

      DO t=1,Ntwig ! Loop over twigs
        target_twig_beg = twig_beg(t)
        target_twig_end = twig_end(t)

        list_beg = direct_off(t)  !Beginning of interaction list for twig t 
        list_end = direct_off(t+1)-1!End of interaction list for twig t

        DO i=target_twig_beg,target_twig_end ! loop over targets in the twig
         
          tarpos(1) = x(i)  ! tarpos is globally defined 
          tarpos(2) = y(i)  ! (this has to change)   
          tarpos(3) = z(i)  
          tarq=tr_q(:,i)
        
          x(i) = x(i) + 1.23456 ! Remove singularity
          temp_area = tr_area(i)
          tr_area(i)=0.d0
            
          DO j=list_beg,list_end
            p_ibeg = direct_list(1,j) ! beginning of source cell
            p_iend = direct_list(2,j) ! end of source cell
            penglocal = 0.0_r8
            CALL COMPP_DIRECT_PB(penglocal,p_ibeg,p_iend,
     &                   x,y,z,tpoten,kappa,numpars,eps)
            peng(:,i) = peng(:,i) + penglocal
          ENDDO

          x(i) = x(i) - 1.23456  ! Restore singularity
          tr_area(i) = temp_area
        ENDDO
      ENDDO

      END SUBROUTINE COMPUTE_DIRECT_VECTORIZED
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC      

      RECURSIVE SUBROUTINE COMPP_TREE(p,peng,x,y,z,q,tpoten,kappa,theta,
     &                                numpars,kk,eps,tempq,der_cof)
      IMPLICIT NONE

      INTEGER,INTENT(IN) :: numpars,kk(3,16)
      TYPE(tnode),POINTER :: p   
      REAL(KIND=r8),INTENT(INOUT) :: peng(2)     
      REAL(KIND=r8),DIMENSION(numpars),INTENT(IN) :: x,y,z
      REAL(KIND=r8),DIMENSION(numpars,16,2),INTENT(IN) :: q
      REAL(KIND=r8),DIMENSION(2*numpars),INTENT(IN) :: tpoten
      REAL(KIND=r8),INTENT(IN):: kappa,theta,eps,tempq(16,2)
      REAL(KIND=r8),INTENT(IN):: 
     & der_cof(0:torder2,0:torder2,0:torder2,16)

C local variables

      REAL(KIND=r8) :: tx,ty,tz,dist,penglocal(2),kapa(2)
      real(kind=r8) :: SL(4),pt_comp(16,2)
      INTEGER :: i,j,k,ijk(3),ikp,indx,err

C determine DISTSQ for MAC test
      tx=p%x_mid-tarpos(1)
      ty=p%y_mid-tarpos(2)
      tz=p%z_mid-tarpos(3)
      dist=SQRT(tx*tx+ty*ty+tz*tz)

C intialize potential energy and force 
      peng=0.0_r8

C If MAC is accepted and there is more than 1 particle in the 
C box use the expansion for the approximation.
	!print *,p%radius,dist*theta,p%numpar
	!pause
      IF ((p%radius .LT. dist*theta) .AND.
     &    (p%numpar .GT. 40)) THEN
C@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@         
        IF (p%exist_ms .EQ. 0) THEN
		ALLOCATE(p%ms(16,0:torder,0:torder,0:torder),STAT=err)
C		ALLOCATE(p%ms(0:torder,0:torder,0:torder,16),STAT=err)
		IF (err .NE. 0) THEN
			WRITE(6,*) 'Error allocating node moments! '
			STOP
		END IF
C#####################################################################
C Generate the moments if not allocated yet 
		CALL COMP_MS(p,x,y,z,q(:,:,1),numpars)
C#####################################################################			
		p%exist_ms=1
	END IF   
       
       CALL  COMPP_TREE_PB(kk,p,peng,kappa,theta,eps,tempq,der_cof)
C       CALL COMPP_DIRECT_PB(penglocal,p%ibeg,p%iend,
C     &                        x,y,z,tpoten,kappa,numpars,eps)
C       write(*,*) peng(1),penglocal(1),(peng(1)-penglocal(1))/peng(1)
C       write(*,*) peng(2),penglocal(2),(peng(2)-penglocal(2))/peng(2)
C       pause
C       peng=penglocal
      ELSE

C If MAC fails check to see if there are children. If not, perform direct 
C calculation.  If there are children, call routine recursively for each.
C
         IF (p%num_children .EQ. 0) THEN
            CALL COMPP_DIRECT_PB(penglocal,p%ibeg,p%iend,
     &                        x,y,z,tpoten,kappa,numpars,eps)
            peng=penglocal
         ELSE
            DO i=1,p%num_children
               CALL COMPP_TREE(p%child(i)%p_to_tnode,penglocal,x,y,z,q,
     &               tpoten,kappa,theta,numpars,kk,eps,tempq,der_cof)
               peng=peng+penglocal
            END DO  
         END IF 
      END IF

      RETURN
      END SUBROUTINE COMPP_TREE

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE COMPUTE_CLUSTER_INTERACTION(x,y,z,peng,cluster_off,
     &          twig_beg, twig_end, cluster_list, cluster_center_list,
     &          cluster_ms_beg, kappa, theta, Ntwig, Ncell_inter, 
     &          n_cluster, der_cof, kk, eps)
C
C     Loops over target twigs and interaction list to call
C     CLUSTER_PARTICLE_INTERACTION to get the interaction 
C     between them
C
      use treecode, only: tr_area,tr_q,tchg,numpars
      IMPLICIT NONE

C     Input variables
      INTEGER,INTENT(IN):: Ntwig, Ncell_inter, kk(3,16),n_cluster
      REAL(KIND=r8),INTENT(IN):: kappa, theta, eps
      INTEGER,DIMENSION(Ntwig+1),INTENT(IN):: cluster_off
      INTEGER,DIMENSION(Ntwig),INTENT(IN):: twig_beg, twig_end
      INTEGER,DIMENSION(n_cluster),INTENT(IN):: cluster_list
      REAL(KIND=r8),DIMENSION(3,Ncell_inter),INTENT(IN)::
     &                          cluster_center_list
      REAL(KIND=r8),DIMENSION(Ncell_inter,16,0:torder,0:torder,
     &                  0:torder),INTENT(IN):: cluster_ms_beg
      REAL*8,DIMENSION(numpars),INTENT(IN):: x,y,z
      REAL(KIND=r8),DIMENSION(2,numpars),INTENT(INOUT)::peng
      REAL(KIND=r8),INTENT(IN):: 
     & der_cof(0:torder2,0:torder2,0:torder2,16)

C     Local variables
      REAL(KIND=r8):: penglocal(2), cluster_center(3), 
     &              cluster_moment(16,0:torder,0:torder,0:torder)
      INTEGER:: target_twig_beg, target_twig_end, source_cell_pointer,
     &          i, t, list_beg, list_end, source_cell 
      REAL(KIND=r8) ::tempq(16,2)


      DO t=1,Ntwig

        target_twig_beg = twig_beg(t)
        target_twig_end = twig_end(t)

        list_beg = cluster_off(t)  !Beginning of interaction list for twig t 
        list_end = cluster_off(t+1)-1!End of interaction list for twig t

        DO source_cell_pointer=list_beg,list_end

          source_cell = cluster_list(source_cell_pointer)
          cluster_center = cluster_center_list(:,source_cell)
          cluster_moment = cluster_ms_beg(source_cell,:,:,:,:)

          DO i=target_twig_beg,target_twig_end
        
            tarpos(1) = x(i)
            tarpos(2) = y(i)
            tarpos(3) = z(i)
            tarq=tr_q(:,i)
            tempq=tchg(i,:,:)

            penglocal = 0.0_r8
            CALL CLUSTER_PARTICLE_INTERACTION(kk,cluster_moment,
     &         cluster_center,penglocal,kappa,theta,eps,tempq,der_cof)

            peng(:,i) = peng(:,i) + penglocal

          END DO
        END DO
      END DO

      END SUBROUTINE COMPUTE_CLUSTER_INTERACTION

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE COMPUTE_CLUSTER_PACKED(x,y,z,peng,cluster_off,
     &          twig_beg, twig_end, cluster_center_pack,
     &          cluster_ms_pack, index_3Dto1Dmap, index_1Dto3Dmap, 
     &          kappa, theta, Ntwig, Ncell_inter, n_cluster, der_cof_1D,
     &          kk, eps, Nm)
C
C     Loops over target twigs and interaction list to call
C     CLUSTER_PARTICLE_INTERACTION to get the interaction 
C     between them
C     Different to COMPUTE_CLUSTER_INTERACTION, this function uses
C     packed versions of cluster_ms_beg and cluster_center_list that
C     are in the order of the interaction list, repeating some data
C     storage. This is better for GPU purposes
C  
      use treecode, only: tr_area,tr_q,tchg,numpars
      IMPLICIT NONE

C     Input variables
      INTEGER,INTENT(IN):: Ntwig, Ncell_inter, kk(3,16),n_cluster,Nm
      REAL(KIND=r8),INTENT(IN):: kappa, theta, eps
      INTEGER,DIMENSION(Ntwig+1),INTENT(IN):: cluster_off
      INTEGER,DIMENSION(Ntwig),INTENT(IN):: twig_beg, twig_end
      REAL(KIND=r8),DIMENSION(3,n_cluster),INTENT(IN)::
     &                          cluster_center_pack
      REAL(KIND=r8),DIMENSION(16,(n_cluster-1)*Nm),INTENT(IN):: 
     &                                          cluster_ms_pack
      REAL*8,DIMENSION(numpars),INTENT(IN):: x,y,z
      REAL(KIND=r8),DIMENSION(2,numpars),INTENT(INOUT)::peng
      REAL(KIND=r8),INTENT(IN):: 
     & der_cof_1D(16*Nm)
      INTEGER,DIMENSION(0:torder2,0:torder2,0:torder2),INTENT(IN)
     &                                          :: index_3Dto1Dmap
      INTEGER,DIMENSION(3,Nm),INTENT(IN):: index_1Dto3Dmap
    

C     Local variables
      REAL(KIND=r8):: penglocal(2), cluster_center(3), 
     &              cluster_moment(16,Nm)
      INTEGER:: target_twig_beg, target_twig_end, source_cell_pointer,
     &          i, t, list_beg, list_end, source_cell,counter,i1,j1,k1
      REAL(KIND=r8) ::tempq(16,2)


      DO t=1,Ntwig

        target_twig_beg = twig_beg(t)
        target_twig_end = twig_end(t)

        list_beg = cluster_off(t)  !Beginning of interaction list for twig t 
        list_end = cluster_off(t+1)-1!End of interaction list for twig t

        DO source_cell_pointer=list_beg,list_end

          cluster_center = cluster_center_pack(:,source_cell_pointer)

          cluster_moment(:,:) = cluster_ms_pack(:,
     &                                  (source_cell_pointer-1)*Nm+1:
     &                                  (source_cell_pointer-1)*Nm+Nm)

          DO i=target_twig_beg,target_twig_end
        
            tarpos(1) = x(i)
            tarpos(2) = y(i)
            tarpos(3) = z(i)
            tarq=tr_q(:,i)
            tempq=tchg(i,:,:)

            penglocal = 0.0_r8
            CALL CLUSTER_INTERACTION_PACKED(kk,cluster_moment,
     &        cluster_center,penglocal,kappa,theta,eps,tempq,der_cof_1D,
     &        index_3Dto1Dmap, index_1Dto3Dmap, Nm)

            peng(:,i) = peng(:,i) + penglocal

          END DO
        END DO
      END DO

      END SUBROUTINE COMPUTE_CLUSTER_PACKED
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE CLUSTER_INTERACTION_PACKED(kk,cluster_moment,
     &         cluster_center,peng,kappa,theta,eps,tempq,der_cof_1D,
     &         index_3Dto1Dmap, index_1Dto3Dmap, Nm)
C
C     Computes the cluster particle interaction for a single particle
C     and a single source cell
C
      IMPLICIT NONE
      

      INTEGER,INTENT(IN) :: kk(3,16), Nm
      REAL(KIND=r8),INTENT(INOUT) :: peng(2)
      REAL(KIND=r8),INTENT(IN):: kappa,theta,eps,tempq(16,2),
     & der_cof_1D(16*Nm),cluster_center(3)
      REAL(KIND=r8),DIMENSION(16,Nm),INTENT(IN):: cluster_moment
      INTEGER,DIMENSION(0:torder2,0:torder2,0:torder2),INTENT(IN)
     &                                          :: index_3Dto1Dmap
      INTEGER,DIMENSION(3,Nm),INTENT(IN):: index_1Dto3Dmap

C local variables

      REAL(KIND=r8) :: kapa(2),cell_center(3), a_1D(Nm)
      real(kind=r8) :: SL(4),pt_comp(16,2)
      INTEGER :: i,j,k,ikp,indx,kk1,kk2,kk3,index_1D,index_3D(3),
     &           kk_1D(Nm)

      
       
      kapa=(/0.d0,kappa/)
      do ikp=1,2
C Get the fundamental solution of Poisson equation and PB equation			
      CALL COMPUTE_COEFF_VECTORIZED(cluster_center,kapa(ikp))
      do indx=1,16
        peng=0.0d0
        
C        DO k=0,torder2
C          DO j=0,torder2-k
C            DO i=0,torder2-k-j
C              index_1D = index_3Dto1Dmap(i,j,k)
C              peng(ikp)=peng(ikp)+der_cof(i,j,k,indx)
C     &              *a(i+kk(1,indx),j+kk(2,indx),k+kk(3,indx))
C     &              *cluster_moment(indx,index_1D)
C            END DO
C          END DO
C        END DO
C################
C        index_1D = 0
C        DO i=0,torder2
C          DO j=0,torder2-i
C            DO k=0,torder2-i-j
C              index_1D = index_1D + 1
C              a_1D(index_1D) = a(i,j,k)
C              kk_1D(index_1D) = kk(i,j,k)
C            ENDDO
C          ENDDO
C        ENDDO

        DO index_1D=1,Nm
          index_3D(:) = index_1Dto3Dmap(:,index_1D) 
          peng(ikp)=peng(ikp)
     &              +der_cof_1D((indx-1)*Nm + index_1D)
     &              *a(index_3D(1)+kk(1,indx),
     &                 index_3D(2)+kk(2,indx),
     &                 index_3D(3)+kk(3,indx))
     &              *cluster_moment(indx,index_1D)
          
        ENDDO
C#################
        pt_comp(indx,ikp)=tempq(indx,ikp)*peng(ikp)
      enddo
      enddo
      sL(1)=pt_comp(1,1)-pt_comp(1,2)
      sL(2)=eps*(sum(pt_comp(2:4,2)))-sum(pt_comp(2:4,1))
      sL(3)=-(sum(pt_comp(5:7,1))-1/eps*sum(pt_comp(5:7,2)))
      sL(4)=sum(pt_comp(8:16,2))-sum(pt_comp(8:16,1))
 
      peng(1)=sL(1)+sL(2)
      peng(2)=sL(3)+sL(4)

      END SUBROUTINE CLUSTER_INTERACTION_PACKED

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE CLUSTER_PARTICLE_INTERACTION(kk,cluster_moment,
     &         cluster_center,peng,kappa,theta,eps,tempq,der_cof)
C
C     Computes the cluster particle interaction for a single particle
C     and a single source cell
C
      IMPLICIT NONE
      

      INTEGER,INTENT(IN) :: kk(3,16)
      REAL(KIND=r8),INTENT(INOUT) :: peng(2)
      REAL(KIND=r8),INTENT(IN):: kappa,theta,eps,tempq(16,2),
     & der_cof(0:torder2,0:torder2,0:torder2,16),cluster_center(3)
      REAL(KIND=r8),DIMENSION(16, 0:torder,0:torder,0:torder), 
     &              INTENT(IN):: cluster_moment
C local variables

      REAL(KIND=r8) :: kapa(2),cell_center(3)
      real(kind=r8) :: SL(4),pt_comp(16,2)
      INTEGER :: i,j,k,ikp,indx,kk1,kk2,kk3

      
       
      kapa=(/0.d0,kappa/)
      do ikp=1,2
C Get the fundamental solution of Poisson equation and PB equation			
      CALL COMPUTE_COEFF_VECTORIZED(cluster_center,kapa(ikp))
      do indx=1,16
        peng=0.0d0
        DO k=0,torder2
          DO j=0,torder2-k
            DO i=0,torder2-k-j
              peng(ikp)=peng(ikp)+der_cof(i,j,k,indx)
     &              *a(i+kk(1,indx),j+kk(2,indx),k+kk(3,indx))
     &              *cluster_moment(indx,i,j,k)
            END DO
          END DO
        END DO

        pt_comp(indx,ikp)=tempq(indx,ikp)*peng(ikp)
      enddo
      enddo
      sL(1)=pt_comp(1,1)-pt_comp(1,2)
      sL(2)=eps*(sum(pt_comp(2:4,2)))-sum(pt_comp(2:4,1))
      sL(3)=-(sum(pt_comp(5:7,1))-1/eps*sum(pt_comp(5:7,2)))
      sL(4)=sum(pt_comp(8:16,2))-sum(pt_comp(8:16,1))
 
      peng(1)=sL(1)+sL(2)
      peng(2)=sL(3)+sL(4)

      END SUBROUTINE CLUSTER_PARTICLE_INTERACTION

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE GENERATE_INDEX_MAP(torder2)
      use treecode
      IMPLICIT NONE
      
      INTEGER, INTENT(IN):: torder2

C     Local variables
      INTEGER:: i, j, k, l, counter, err

      Nm = (torder2 + 1) * (torder2 + 2) * (torder2 + 3) / 6

C     Allocate arrays
      allocate(index_1Dto3Dmap(3,Nm), stat=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating index map! '
        STOP
      END IF
      
      allocate(index_3Dto1Dmap(0:torder2,0:torder2,0:torder2), stat=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating index map! '
        STOP
      END IF

      allocate(der_cof_1D(16*Nm), stat=err)
      IF (err .NE. 0) THEN
        WRITE(6,*) 'Error allocating der_cof_1D! '
        STOP
      END IF

      index_1Dto3Dmap = 0.0_r8 
      index_3Dto1Dmap = 0.0_r8
      counter = 0
      DO i=0,torder2
        DO j=0,torder2-i
          DO k=0,torder2-i-j
            counter = counter + 1
            index_1Dto3Dmap(1,counter) = i 
            index_1Dto3Dmap(2,counter) = j 
            index_1Dto3Dmap(3,counter) = k 
            index_3Dto1Dmap(i,j,k)     = counter 
            
            DO l=1,16 !flatten der_cof array
                      ! see that der_cof has order (P+2) but
                      ! der_cof only P. We actually don't need
                      ! the +2, so we are good here.
                der_cof_1D((l-1)*Nm + counter) = der_cof(i,j,k,l)
            ENDDO
          END DO
        END DO
      END DO


      END SUBROUTINE GENERATE_INDEX_MAP
    

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE COMPP_TREE_PB(kk,p,peng,kappa,theta,eps,tempq,der_cof)
      IMPLICIT NONE
      
      INTEGER,INTENT(IN) :: kk(3,16)
      TYPE(tnode),POINTER :: p
      REAL(KIND=r8),INTENT(INOUT) :: peng(2)
C      REAL(KIND=r8),DIMENSION(numpars),INTENT(IN) :: x,y,z
C      REAL(KIND=r8),DIMENSION(numpars,16,2),INTENT(IN) :: q
C      REAL(KIND=r8),DIMENSION(2*numpars),INTENT(IN) :: tpoten
      REAL(KIND=r8),INTENT(IN):: kappa,theta,eps,tempq(16,2),
     & der_cof(0:torder2,0:torder2,0:torder2,16)
C local variables

      REAL(KIND=r8) :: kapa(2)
      real(kind=r8) :: SL(4),pt_comp(16,2)
      INTEGER :: i,j,k,ikp,indx,kk1,kk2,kk3

      
       
        kapa=(/0.d0,kappa/)
        do ikp=1,2
C Get the fundermental solution of Poisson equation and PB equation			
		CALL COMP_TCOEFF(p,kapa(ikp))
		do indx=1,16
		    !kk1=kk(1,indx)
                    !kk2=kk(2,indx)
                    !kk3=kk(3,indx)
                    peng=0.0d0
		    DO k=0,torder2
		        DO j=0,torder2-k
		            DO i=0,torder2-k-j
		                 peng(ikp)=peng(ikp)+der_cof(i,j,k,indx)
     &		                 *a(i+kk(1,indx),j+kk(2,indx),k+kk(3,indx))
     &		                 *p%ms(indx,i,j,k)
  		            END DO
		        END DO
		    END DO
		    pt_comp(indx,ikp)=tempq(indx,ikp)*peng(ikp)
		enddo
        enddo
        sL(1)=pt_comp(1,1)-pt_comp(1,2)
        sL(2)=eps*(sum(pt_comp(2:4,2)))-sum(pt_comp(2:4,1))
        sL(3)=-(sum(pt_comp(5:7,1))-1/eps*sum(pt_comp(5:7,2)))
        sL(4)=sum(pt_comp(8:16,2))-sum(pt_comp(8:16,1))
   		  
        peng(1)=sL(1)+sL(2)
        peng(2)=sL(3)+sL(4)
      END SUBROUTINE COMPP_TREE_PB

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE COMP_TCOEFF_NEW(p,kappa)
      IMPLICIT NONE
C
C COMP_TCOEFF computes the Taylor coefficients of the potential
C using a recurrence formula.  The center of the expansion is the
C midpoint of the node P.  TARPOS and TORDERLIM are globally defined.
C
      TYPE(tnode),POINTER :: p
      REAL(KIND=r8),INTENT(IN)  :: kappa

C local varaibles

      REAL(KIND=r8) :: dx,dy,dz,ddx,ddy,ddz,dist,fac,cf1_new(torderlim)
      REAL(KIND=r8) :: kappax,kappay,kappaz
      INTEGER :: i,j,k

C################################
C Temp variables for saving time
      real*8:: a0,b0,ab0,cft1,cft2,cft3
C################################      

C setup variables
      cf1_new=cf1*kappa 
     
      dx=tarpos(1)-p%x_mid
      dy=tarpos(2)-p%y_mid
      dz=tarpos(3)-p%z_mid

      ddx=2.0_r8*dx
      ddy=2.0_r8*dy
      ddz=2.0_r8*dz

      kappax=kappa*dx
      kappay=kappa*dy
      kappaz=kappa*dz

      dist=dx*dx+dy*dy+dz*dz
      fac=1.0_r8/dist
      dist=SQRT(dist)

C 0th coeff or function val

      b(0,0,0)=EXP(-kappa*dist)
      a(0,0,0)=b(0,0,0)/dist

C 2 indices are 0

C##############################
      a0=a(0,0,0)
      b0=b(0,0,0)
      ab0=fac*(a0+kappa*b0)
C#############################
      b(1,0,0)=kappax*a0
      b(0,1,0)=kappay*a0
      b(0,0,1)=kappaz*a0

      a(1,0,0)=dx*(ab0)
      a(0,1,0)=dy*(ab0)
      a(0,0,1)=dz*(ab0)


      DO i=2,torderlim
C#############################
         cft1=cf1_new(i)
         cft2=cf2(i)
         cft3=cf3(i)
C#############################
         b(i,0,0)=cft1*(dx*a(i-1,0,0)-a(i-2,0,0))
         b(0,i,0)=cft1*(dy*a(0,i-1,0)-a(0,i-2,0))
         b(0,0,i)=cft1*(dz*a(0,0,i-1)-a(0,0,i-2))

         a(i,0,0)=fac*(ddx*cft2*a(i-1,0,0)-cft3*a(i-2,0,0)+
     &            cft1*(dx*b(i-1,0,0)-b(i-2,0,0)))
         a(0,i,0)=fac*(ddy*cft2*a(0,i-1,0)-cft3*a(0,i-2,0)+
     &            cft1*(dy*b(0,i-1,0)-b(0,i-2,0)))
         a(0,0,i)=fac*(ddz*cft2*a(0,0,i-1)-cft3*a(0,0,i-2)+
     &            cft1*(dz*b(0,0,i-1)-b(0,0,i-2)))
      END DO

C 1 index 0, 1 index 1, other >=1

      b(1,1,0)=kappax*a(0,1,0)
      b(1,0,1)=kappax*a(0,0,1)
      b(0,1,1)=kappay*a(0,0,1)

      a(1,1,0)=fac*(dx*a(0,1,0)+ddy*a(1,0,0)+kappax*b(0,1,0))
      a(1,0,1)=fac*(dx*a(0,0,1)+ddz*a(1,0,0)+kappax*b(0,0,1))
      a(0,1,1)=fac*(dy*a(0,0,1)+ddz*a(0,1,0)+kappay*b(0,0,1))

      DO i=2,torderlim-1
         b(1,0,i)=kappax*a(0,0,i)
         b(0,1,i)=kappay*a(0,0,i)
         b(0,i,1)=kappaz*a(0,i,0)
         b(1,i,0)=kappax*a(0,i,0)
         b(i,1,0)=kappay*a(i,0,0)
         b(i,0,1)=kappaz*a(i,0,0)

         a(1,0,i)=fac*(dx*a(0,0,i)+ddz*a(1,0,i-1)-a(1,0,i-2)+
     &            kappax*b(0,0,i)) 
         a(0,1,i)=fac*(dy*a(0,0,i)+ddz*a(0,1,i-1)-a(0,1,i-2)+
     &            kappay*b(0,0,i))
         a(0,i,1)=fac*(dz*a(0,i,0)+ddy*a(0,i-1,1)-a(0,i-2,1)+
     &            kappaz*b(0,i,0))
         a(1,i,0)=fac*(dx*a(0,i,0)+ddy*a(1,i-1,0)-a(1,i-2,0)+
     &            kappax*b(0,i,0))
         a(i,1,0)=fac*(dy*a(i,0,0)+ddx*a(i-1,1,0)-a(i-2,1,0)+
     &            kappay*b(i,0,0))
         a(i,0,1)=fac*(dz*a(i,0,0)+ddx*a(i-1,0,1)-a(i-2,0,1)+
     &            kappaz*b(i,0,0))         
      END DO

C 1 index 0, others >= 2

      DO i=2,torderlim-2
            
C#############################
         cft1=cf1_new(i)
         cft2=cf2(i)
         cft3=cf3(i)
C#############################
         DO j=2,torderlim-i
            b(i,j,0)=cft1*(dx*a(i-1,j,0)-a(i-2,j,0))
            b(i,0,j)=cft1*(dx*a(i-1,0,j)-a(i-2,0,j))
            b(0,i,j)=cft1*(dy*a(0,i-1,j)-a(0,i-2,j))

            a(i,j,0)=fac*(ddx*cft2*a(i-1,j,0)+ddy*a(i,j-1,0)
     &               -cft3*a(i-2,j,0)-a(i,j-2,0)+
     &               cft1*(dx*b(i-1,j,0)-b(i-2,j,0)))
            a(i,0,j)=fac*(ddx*cft2*a(i-1,0,j)+ddz*a(i,0,j-1)
     &               -cft3*a(i-2,0,j)-a(i,0,j-2)+
     &               cft1*(dx*b(i-1,0,j)-b(i-2,0,j)))
            a(0,i,j)=fac*(ddy*cft2*a(0,i-1,j)+ddz*a(0,i,j-1)
     &               -cft3*a(0,i-2,j)-a(0,i,j-2)+
     &               cft1*(dy*b(0,i-1,j)-b(0,i-2,j)))
         END DO
      END DO

C 2 indices 1, other >= 1
C b(1,1,1) is correct, but a little tricky!
C      b(1,1,1)=5.0*dz*fac*b(1,1,0)

      b(1,1,1)=kappax*a(0,1,1)
      a(1,1,1)=fac*(dx*a(0,1,1)+ddy*a(1,0,1)+ddz*a(1,1,0)+
     &         kappax*b(0,1,1))

      DO i=2,torderlim-2
         b(1,1,i)=kappax*a(0,1,i)
         b(1,i,1)=kappax*a(0,i,1)
         b(i,1,1)=kappay*a(i,0,1)

         a(1,1,i)=fac*(dx*a(0,1,i)+ddy*a(1,0,i)+ddz*a(1,1,i-1)
     &           -a(1,1,i-2)+kappax*b(0,1,i))
         a(1,i,1)=fac*(dx*a(0,i,1)+ddy*a(1,i-1,1)+ddz*a(1,i,0)
     &           -a(1,i-2,1)+kappax*b(0,i,1))
         a(i,1,1)=fac*(dy*a(i,0,1)+ddx*a(i-1,1,1)+ddz*a(i,1,0)
     &           -a(i-2,1,1)+kappay*b(i,0,1))
      END DO

C 1 index 1, others >=2

      DO i=2,torderlim-3
         DO j=2,torderlim-i
            b(1,i,j)=kappax*a(0,i,j)
            b(i,1,j)=kappay*a(i,0,j)
            b(i,j,1)=kappaz*a(i,j,0)

            a(1,i,j)=fac*(dx*a(0,i,j)+ddy*a(1,i-1,j)+ddz*a(1,i,j-1)
     &              -a(1,i-2,j)-a(1,i,j-2)+kappax*b(0,i,j))
            a(i,1,j)=fac*(dy*a(i,0,j)+ddx*a(i-1,1,j)+ddz*a(i,1,j-1)
     &              -a(i-2,1,j)-a(i,1,j-2)+kappay*b(i,0,j))
            a(i,j,1)=fac*(dz*a(i,j,0)+ddx*a(i-1,j,1)+ddy*a(i,j-1,1)
     &              -a(i-2,j,1)-a(i,j-2,1)+kappaz*b(i,j,0))

         END DO
      END DO

C all indices >=2

      DO k=2,torderlim-4
         DO j=2,torderlim-2-k
            DO i=2,torderlim-k-j
               b(i,j,k)=cf1_new(i)*(dx*a(i-1,j,k)-a(i-2,j,k))

               a(i,j,k)=fac*(ddx*cf2(i)*a(i-1,j,k)+ddy*a(i,j-1,k)
     &                 +ddz*a(i,j,k-1)-cf3(i)*a(i-2,j,k)
     &                 -a(i,j-2,k)-a(i,j,k-2)+
     &                 cf1_new(i)*(dx*b(i-1,j,k)-b(i-2,j,k)))
            END DO
         END DO
      END DO

      RETURN
      END SUBROUTINE COMP_TCOEFF_NEW

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE COMPUTE_COEFF_VECTORIZED(cell_center,kappa)
      IMPLICIT NONE
C
C COMP_TCOEFF computes the Taylor coefficients of the potential
C using a recurrence formula.  The center of the expansion is the
C midpoint of the node P.  TARPOS and TORDERLIM are globally defined.
C
      REAL(KIND=r8),INTENT(IN)  :: kappa
      REAL(KIND=r8),DIMENSION(3),INTENT(IN)::cell_center

C local varaibles
      REAL(KIND=r8) :: dx,dy,dz,ddx,ddy,ddz,dist,fac,cf1_new(torderlim)
      REAL(KIND=r8) :: kappax,kappay,kappaz
      INTEGER :: i,j,k

C setup variables
      cf1_new=cf1*kappa 
     
      dx=tarpos(1)-cell_center(1)
      dy=tarpos(2)-cell_center(2)
      dz=tarpos(3)-cell_center(3)

      ddx=2.0_r8*dx
      ddy=2.0_r8*dy
      ddz=2.0_r8*dz

      kappax=kappa*dx
      kappay=kappa*dy
      kappaz=kappa*dz

      dist=dx*dx+dy*dy+dz*dz
      fac=1.0_r8/dist
      dist=SQRT(dist)

C 0th coeff or function val

      b(0,0,0)=EXP(-kappa*dist)
      a(0,0,0)=b(0,0,0)/dist

C 2 indices are 0

      b(1,0,0)=kappax*a(0,0,0)
      b(0,1,0)=kappay*a(0,0,0)
      b(0,0,1)=kappaz*a(0,0,0)

      a(1,0,0)=fac*dx*(a(0,0,0)+kappa*b(0,0,0))
      a(0,1,0)=fac*dy*(a(0,0,0)+kappa*b(0,0,0))
      a(0,0,1)=fac*dz*(a(0,0,0)+kappa*b(0,0,0))


      DO i=2,torderlim
         b(i,0,0)=cf1_new(i)*(dx*a(i-1,0,0)-a(i-2,0,0))
         b(0,i,0)=cf1_new(i)*(dy*a(0,i-1,0)-a(0,i-2,0))
         b(0,0,i)=cf1_new(i)*(dz*a(0,0,i-1)-a(0,0,i-2))

         a(i,0,0)=fac*(ddx*cf2(i)*a(i-1,0,0)-cf3(i)*a(i-2,0,0)+
     &            cf1_new(i)*(dx*b(i-1,0,0)-b(i-2,0,0)))
         a(0,i,0)=fac*(ddy*cf2(i)*a(0,i-1,0)-cf3(i)*a(0,i-2,0)+
     &            cf1_new(i)*(dy*b(0,i-1,0)-b(0,i-2,0)))
         a(0,0,i)=fac*(ddz*cf2(i)*a(0,0,i-1)-cf3(i)*a(0,0,i-2)+
     &            cf1_new(i)*(dz*b(0,0,i-1)-b(0,0,i-2)))
      END DO

C 1 index 0, 1 index 1, other >=1

      b(1,1,0)=kappax*a(0,1,0)
      b(1,0,1)=kappax*a(0,0,1)
      b(0,1,1)=kappay*a(0,0,1)

      a(1,1,0)=fac*(dx*a(0,1,0)+ddy*a(1,0,0)+kappax*b(0,1,0))
      a(1,0,1)=fac*(dx*a(0,0,1)+ddz*a(1,0,0)+kappax*b(0,0,1))
      a(0,1,1)=fac*(dy*a(0,0,1)+ddz*a(0,1,0)+kappay*b(0,0,1))

      DO i=2,torderlim-1
         b(1,0,i)=kappax*a(0,0,i)
         b(0,1,i)=kappay*a(0,0,i)
         b(0,i,1)=kappaz*a(0,i,0)
         b(1,i,0)=kappax*a(0,i,0)
         b(i,1,0)=kappay*a(i,0,0)
         b(i,0,1)=kappaz*a(i,0,0)

         a(1,0,i)=fac*(dx*a(0,0,i)+ddz*a(1,0,i-1)-a(1,0,i-2)+
     &            kappax*b(0,0,i)) 
         a(0,1,i)=fac*(dy*a(0,0,i)+ddz*a(0,1,i-1)-a(0,1,i-2)+
     &            kappay*b(0,0,i))
         a(0,i,1)=fac*(dz*a(0,i,0)+ddy*a(0,i-1,1)-a(0,i-2,1)+
     &            kappaz*b(0,i,0))
         a(1,i,0)=fac*(dx*a(0,i,0)+ddy*a(1,i-1,0)-a(1,i-2,0)+
     &            kappax*b(0,i,0))
         a(i,1,0)=fac*(dy*a(i,0,0)+ddx*a(i-1,1,0)-a(i-2,1,0)+
     &            kappay*b(i,0,0))
         a(i,0,1)=fac*(dz*a(i,0,0)+ddx*a(i-1,0,1)-a(i-2,0,1)+
     &            kappaz*b(i,0,0))         
      END DO

C 1 index 0, others >= 2

      DO i=2,torderlim-2
         DO j=2,torderlim-i
            b(i,j,0)=cf1_new(i)*(dx*a(i-1,j,0)-a(i-2,j,0))
            b(i,0,j)=cf1_new(i)*(dx*a(i-1,0,j)-a(i-2,0,j))
            b(0,i,j)=cf1_new(i)*(dy*a(0,i-1,j)-a(0,i-2,j))

            a(i,j,0)=fac*(ddx*cf2(i)*a(i-1,j,0)+ddy*a(i,j-1,0)
     &               -cf3(i)*a(i-2,j,0)-a(i,j-2,0)+
     &               cf1_new(i)*(dx*b(i-1,j,0)-b(i-2,j,0)))
            a(i,0,j)=fac*(ddx*cf2(i)*a(i-1,0,j)+ddz*a(i,0,j-1)
     &               -cf3(i)*a(i-2,0,j)-a(i,0,j-2)+
     &               cf1_new(i)*(dx*b(i-1,0,j)-b(i-2,0,j)))
            a(0,i,j)=fac*(ddy*cf2(i)*a(0,i-1,j)+ddz*a(0,i,j-1)
     &               -cf3(i)*a(0,i-2,j)-a(0,i,j-2)+
     &               cf1_new(i)*(dy*b(0,i-1,j)-b(0,i-2,j)))
         END DO
      END DO

C 2 indices 1, other >= 1
C b(1,1,1) is correct, but a little tricky!
C      b(1,1,1)=5.0*dz*fac*b(1,1,0)

      b(1,1,1)=kappax*a(0,1,1)
      a(1,1,1)=fac*(dx*a(0,1,1)+ddy*a(1,0,1)+ddz*a(1,1,0)+
     &         kappax*b(0,1,1))

      DO i=2,torderlim-2
         b(1,1,i)=kappax*a(0,1,i)
         b(1,i,1)=kappax*a(0,i,1)
         b(i,1,1)=kappay*a(i,0,1)

         a(1,1,i)=fac*(dx*a(0,1,i)+ddy*a(1,0,i)+ddz*a(1,1,i-1)
     &           -a(1,1,i-2)+kappax*b(0,1,i))
         a(1,i,1)=fac*(dx*a(0,i,1)+ddy*a(1,i-1,1)+ddz*a(1,i,0)
     &           -a(1,i-2,1)+kappax*b(0,i,1))
         a(i,1,1)=fac*(dy*a(i,0,1)+ddx*a(i-1,1,1)+ddz*a(i,1,0)
     &           -a(i-2,1,1)+kappay*b(i,0,1))
      END DO

C 1 index 1, others >=2

      DO i=2,torderlim-3
         DO j=2,torderlim-i
            b(1,i,j)=kappax*a(0,i,j)
            b(i,1,j)=kappay*a(i,0,j)
            b(i,j,1)=kappaz*a(i,j,0)

            a(1,i,j)=fac*(dx*a(0,i,j)+ddy*a(1,i-1,j)+ddz*a(1,i,j-1)
     &              -a(1,i-2,j)-a(1,i,j-2)+kappax*b(0,i,j))
            a(i,1,j)=fac*(dy*a(i,0,j)+ddx*a(i-1,1,j)+ddz*a(i,1,j-1)
     &              -a(i-2,1,j)-a(i,1,j-2)+kappay*b(i,0,j))
            a(i,j,1)=fac*(dz*a(i,j,0)+ddx*a(i-1,j,1)+ddy*a(i,j-1,1)
     &              -a(i-2,j,1)-a(i,j-2,1)+kappaz*b(i,j,0))

         END DO
      END DO

C all indices >=2

      DO k=2,torderlim-4
         DO j=2,torderlim-2-k
            DO i=2,torderlim-k-j
               b(i,j,k)=cf1_new(i)*(dx*a(i-1,j,k)-a(i-2,j,k))

               a(i,j,k)=fac*(ddx*cf2(i)*a(i-1,j,k)+ddy*a(i,j-1,k)
     &                 +ddz*a(i,j,k-1)-cf3(i)*a(i-2,j,k)
     &                 -a(i,j-2,k)-a(i,j,k-2)+
     &                 cf1_new(i)*(dx*b(i-1,j,k)-b(i-2,j,k)))
            END DO
         END DO
      END DO

      RETURN
      END SUBROUTINE COMPUTE_COEFF_VECTORIZED

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE COMP_TCOEFF(p,kappa)
      IMPLICIT NONE
C
C COMP_TCOEFF computes the Taylor coefficients of the potential
C using a recurrence formula.  The center of the expansion is the
C midpoint of the node P.  TARPOS and TORDERLIM are globally defined.
C
      TYPE(tnode),POINTER :: p
      REAL(KIND=r8),INTENT(IN)  :: kappa

C local varaibles
      REAL(KIND=r8) :: dx,dy,dz,ddx,ddy,ddz,dist,fac,cf1_new(torderlim)
      REAL(KIND=r8) :: kappax,kappay,kappaz
      INTEGER :: i,j,k

C setup variables
      cf1_new=cf1*kappa 
     
      dx=tarpos(1)-p%x_mid
      dy=tarpos(2)-p%y_mid
      dz=tarpos(3)-p%z_mid

      ddx=2.0_r8*dx
      ddy=2.0_r8*dy
      ddz=2.0_r8*dz

      kappax=kappa*dx
      kappay=kappa*dy
      kappaz=kappa*dz

      dist=dx*dx+dy*dy+dz*dz
      fac=1.0_r8/dist
      dist=SQRT(dist)

C 0th coeff or function val

      b(0,0,0)=EXP(-kappa*dist)
      a(0,0,0)=b(0,0,0)/dist

C 2 indices are 0

      b(1,0,0)=kappax*a(0,0,0)
      b(0,1,0)=kappay*a(0,0,0)
      b(0,0,1)=kappaz*a(0,0,0)

      a(1,0,0)=fac*dx*(a(0,0,0)+kappa*b(0,0,0))
      a(0,1,0)=fac*dy*(a(0,0,0)+kappa*b(0,0,0))
      a(0,0,1)=fac*dz*(a(0,0,0)+kappa*b(0,0,0))


      DO i=2,torderlim
         b(i,0,0)=cf1_new(i)*(dx*a(i-1,0,0)-a(i-2,0,0))
         b(0,i,0)=cf1_new(i)*(dy*a(0,i-1,0)-a(0,i-2,0))
         b(0,0,i)=cf1_new(i)*(dz*a(0,0,i-1)-a(0,0,i-2))

         a(i,0,0)=fac*(ddx*cf2(i)*a(i-1,0,0)-cf3(i)*a(i-2,0,0)+
     &            cf1_new(i)*(dx*b(i-1,0,0)-b(i-2,0,0)))
         a(0,i,0)=fac*(ddy*cf2(i)*a(0,i-1,0)-cf3(i)*a(0,i-2,0)+
     &            cf1_new(i)*(dy*b(0,i-1,0)-b(0,i-2,0)))
         a(0,0,i)=fac*(ddz*cf2(i)*a(0,0,i-1)-cf3(i)*a(0,0,i-2)+
     &            cf1_new(i)*(dz*b(0,0,i-1)-b(0,0,i-2)))
      END DO

C 1 index 0, 1 index 1, other >=1

      b(1,1,0)=kappax*a(0,1,0)
      b(1,0,1)=kappax*a(0,0,1)
      b(0,1,1)=kappay*a(0,0,1)

      a(1,1,0)=fac*(dx*a(0,1,0)+ddy*a(1,0,0)+kappax*b(0,1,0))
      a(1,0,1)=fac*(dx*a(0,0,1)+ddz*a(1,0,0)+kappax*b(0,0,1))
      a(0,1,1)=fac*(dy*a(0,0,1)+ddz*a(0,1,0)+kappay*b(0,0,1))

      DO i=2,torderlim-1
         b(1,0,i)=kappax*a(0,0,i)
         b(0,1,i)=kappay*a(0,0,i)
         b(0,i,1)=kappaz*a(0,i,0)
         b(1,i,0)=kappax*a(0,i,0)
         b(i,1,0)=kappay*a(i,0,0)
         b(i,0,1)=kappaz*a(i,0,0)

         a(1,0,i)=fac*(dx*a(0,0,i)+ddz*a(1,0,i-1)-a(1,0,i-2)+
     &            kappax*b(0,0,i)) 
         a(0,1,i)=fac*(dy*a(0,0,i)+ddz*a(0,1,i-1)-a(0,1,i-2)+
     &            kappay*b(0,0,i))
         a(0,i,1)=fac*(dz*a(0,i,0)+ddy*a(0,i-1,1)-a(0,i-2,1)+
     &            kappaz*b(0,i,0))
         a(1,i,0)=fac*(dx*a(0,i,0)+ddy*a(1,i-1,0)-a(1,i-2,0)+
     &            kappax*b(0,i,0))
         a(i,1,0)=fac*(dy*a(i,0,0)+ddx*a(i-1,1,0)-a(i-2,1,0)+
     &            kappay*b(i,0,0))
         a(i,0,1)=fac*(dz*a(i,0,0)+ddx*a(i-1,0,1)-a(i-2,0,1)+
     &            kappaz*b(i,0,0))         
      END DO

C 1 index 0, others >= 2

      DO i=2,torderlim-2
         DO j=2,torderlim-i
            b(i,j,0)=cf1_new(i)*(dx*a(i-1,j,0)-a(i-2,j,0))
            b(i,0,j)=cf1_new(i)*(dx*a(i-1,0,j)-a(i-2,0,j))
            b(0,i,j)=cf1_new(i)*(dy*a(0,i-1,j)-a(0,i-2,j))

            a(i,j,0)=fac*(ddx*cf2(i)*a(i-1,j,0)+ddy*a(i,j-1,0)
     &               -cf3(i)*a(i-2,j,0)-a(i,j-2,0)+
     &               cf1_new(i)*(dx*b(i-1,j,0)-b(i-2,j,0)))
            a(i,0,j)=fac*(ddx*cf2(i)*a(i-1,0,j)+ddz*a(i,0,j-1)
     &               -cf3(i)*a(i-2,0,j)-a(i,0,j-2)+
     &               cf1_new(i)*(dx*b(i-1,0,j)-b(i-2,0,j)))
            a(0,i,j)=fac*(ddy*cf2(i)*a(0,i-1,j)+ddz*a(0,i,j-1)
     &               -cf3(i)*a(0,i-2,j)-a(0,i,j-2)+
     &               cf1_new(i)*(dy*b(0,i-1,j)-b(0,i-2,j)))
         END DO
      END DO

C 2 indices 1, other >= 1
C b(1,1,1) is correct, but a little tricky!
C      b(1,1,1)=5.0*dz*fac*b(1,1,0)

      b(1,1,1)=kappax*a(0,1,1)
      a(1,1,1)=fac*(dx*a(0,1,1)+ddy*a(1,0,1)+ddz*a(1,1,0)+
     &         kappax*b(0,1,1))

      DO i=2,torderlim-2
         b(1,1,i)=kappax*a(0,1,i)
         b(1,i,1)=kappax*a(0,i,1)
         b(i,1,1)=kappay*a(i,0,1)

         a(1,1,i)=fac*(dx*a(0,1,i)+ddy*a(1,0,i)+ddz*a(1,1,i-1)
     &           -a(1,1,i-2)+kappax*b(0,1,i))
         a(1,i,1)=fac*(dx*a(0,i,1)+ddy*a(1,i-1,1)+ddz*a(1,i,0)
     &           -a(1,i-2,1)+kappax*b(0,i,1))
         a(i,1,1)=fac*(dy*a(i,0,1)+ddx*a(i-1,1,1)+ddz*a(i,1,0)
     &           -a(i-2,1,1)+kappay*b(i,0,1))
      END DO

C 1 index 1, others >=2

      DO i=2,torderlim-3
         DO j=2,torderlim-i
            b(1,i,j)=kappax*a(0,i,j)
            b(i,1,j)=kappay*a(i,0,j)
            b(i,j,1)=kappaz*a(i,j,0)

            a(1,i,j)=fac*(dx*a(0,i,j)+ddy*a(1,i-1,j)+ddz*a(1,i,j-1)
     &              -a(1,i-2,j)-a(1,i,j-2)+kappax*b(0,i,j))
            a(i,1,j)=fac*(dy*a(i,0,j)+ddx*a(i-1,1,j)+ddz*a(i,1,j-1)
     &              -a(i-2,1,j)-a(i,1,j-2)+kappay*b(i,0,j))
            a(i,j,1)=fac*(dz*a(i,j,0)+ddx*a(i-1,j,1)+ddy*a(i,j-1,1)
     &              -a(i-2,j,1)-a(i,j-2,1)+kappaz*b(i,j,0))

         END DO
      END DO

C all indices >=2

      DO k=2,torderlim-4
         DO j=2,torderlim-2-k
            DO i=2,torderlim-k-j
               b(i,j,k)=cf1_new(i)*(dx*a(i-1,j,k)-a(i-2,j,k))

               a(i,j,k)=fac*(ddx*cf2(i)*a(i-1,j,k)+ddy*a(i,j-1,k)
     &                 +ddz*a(i,j,k-1)-cf3(i)*a(i-2,j,k)
     &                 -a(i,j-2,k)-a(i,j,k-2)+
     &                 cf1_new(i)*(dx*b(i-1,j,k)-b(i-2,j,k)))
            END DO
         END DO
      END DO

      RETURN
      END SUBROUTINE COMP_TCOEFF

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE COMPUTE_MOMENTS_VECTORIZED(cluster_center_list,
     &      cluster_beg_list,x,y,z,q,numpars,Ncell_inter,cluster_ms_beg)
      IMPLICIT NONE
C
C COMP_MS computes the moments for node P needed in the Taylor approximation
C
      INTEGER,INTENT(IN) :: numpars,Ncell_inter
      REAL(KIND=r8),DIMENSION(numpars),INTENT(IN) :: x,y,z
      REAL(KIND=r8),DIMENSION(3,Ncell_inter),
     &                          INTENT(IN):: cluster_center_list
C##################################################################
      REAL(KIND=r8),DIMENSION(numpars,16),INTENT(IN) ::q
C     q casts a 3d array (schg) into a 2d array!
C##################################################################
      REAL(KIND=r8),DIMENSION(Ncell_inter,16,
     & 0:torder,0:torder,0:torder), INTENT(INOUT):: cluster_ms_beg
      INTEGER,DIMENSION(2,Ncell_inter),INTENT(IN):: cluster_beg_list

C local variables

      INTEGER :: i,k1,k2,k3,j
      REAL(KIND=r8) :: dx,dy,dz,tx,ty,tz,txyz
     
      DO j=1,Ncell_inter
          cluster_ms_beg(j,:,:,:,:)=0.0_r8
          DO i=cluster_beg_list(1,j),cluster_beg_list(2,j)
             dx=x(i)-cluster_center_list(1,j)
             dy=y(i)-cluster_center_list(2,j)
             dz=z(i)-cluster_center_list(3,j)
             tz=1.0_r8
             DO k3=0,torder
                ty=1.0_r8
                DO k2=0,torder-k3
                   tx=1.0_r8 
                   DO k1=0,torder-k3-k2
C    ####################################################################
                    txyz=tx*ty*tz
                    cluster_ms_beg(j,1:7,k1,k2,k3)=
     &                  cluster_ms_beg(j,1:7,k1,k2,k3)+q(i,1:7)*txyz
                    cluster_ms_beg(j,8:14:3,k1,k2,k3)=
     &              cluster_ms_beg(j,8:14:3,k1,k2,k3)+q(i,8:14:3)*txyz
C    #################################################################### 
                    tx=tx*dx
                   END DO
                   ty=ty*dy
                END DO
                tz=tz*dz
             END DO
          END DO
C    ####################################################################
          cluster_ms_beg(j,9,:,:,:) =cluster_ms_beg(j,8,:,:,:)
          cluster_ms_beg(j,10,:,:,:)=cluster_ms_beg(j,8,:,:,:)
          cluster_ms_beg(j,12,:,:,:)=cluster_ms_beg(j,11,:,:,:)
          cluster_ms_beg(j,13,:,:,:)=cluster_ms_beg(j,11,:,:,:)
          cluster_ms_beg(j,15,:,:,:)=cluster_ms_beg(j,14,:,:,:)
          cluster_ms_beg(j,16,:,:,:)=cluster_ms_beg(j,14,:,:,:)
      END DO
C####################################################################
      RETURN
      END SUBROUTINE COMPUTE_MOMENTS_VECTORIZED

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC

      SUBROUTINE COMPUTE_MOMENTS_PACKED(cluster_center_pack,
     &     cluster_beg_list,x,y,z,q,numpars,Ncell_inter,n_cluster, 
     &     cluster_list, cluster_ms_pack, index_3Dto1Dmap,Nm)
      IMPLICIT NONE
C
C COMP_MS computes the moments for node P needed in the Taylor approximation
C THey are stored in a pakced format in the same order as the
C interaction list 'cluster_list'. Hence, there is repeated data
C storage, but it fits the GPU better.
C
      INTEGER,INTENT(IN) :: numpars,Ncell_inter, n_cluster,Nm
      REAL(KIND=r8),DIMENSION(numpars),INTENT(IN) :: x,y,z
      REAL(KIND=r8),DIMENSION(3,n_cluster),
     &                          INTENT(IN):: cluster_center_pack
C##################################################################
      REAL(KIND=r8),DIMENSION(numpars,16),INTENT(IN) ::q
C     q casts a 3d array (schg) into a 2d array!
C##################################################################
      REAL(KIND=r8),DIMENSION(16,(n_cluster-1)*Nm), INTENT(INOUT):: 
     &                                          cluster_ms_pack
      INTEGER,DIMENSION(2,Ncell_inter),INTENT(IN):: cluster_beg_list
      INTEGER,DIMENSION(n_cluster-1),INTENT(IN):: cluster_list
      INTEGER,DIMENSION(0:torder2,0:torder2,0:torder2),INTENT(IN)::
     &                                          index_3Dto1Dmap

C local variables

      INTEGER :: i,k1,k2,k3,j, index_1D
      REAL(KIND=r8) :: dx,dy,dz,tx,ty,tz,txyz
     
      cluster_ms_pack(:,:)=0.0_r8
      DO j=1,(n_cluster-1)
          DO i=cluster_beg_list(1,cluster_list(j)),
     &          cluster_beg_list(2,cluster_list(j))
             dx=x(i)-cluster_center_pack(1,j)
             dy=y(i)-cluster_center_pack(2,j)
             dz=z(i)-cluster_center_pack(3,j)
             tz=1.0_r8
             DO k3=0,torder2
                ty=1.0_r8
                DO k2=0,torder2-k3
                   tx=1.0_r8 
                   DO k1=0,torder2-k3-k2
C    ####################################################################
                    txyz=tx*ty*tz
                    index_1D = index_3Dto1Dmap(k1,k2,k3)
                    cluster_ms_pack(1:7,(j-1)*Nm+index_1D)=
     &                  cluster_ms_pack(1:7,(j-1)*Nm+index_1D)
     &                                                  +q(i,1:7)*txyz
                    cluster_ms_pack(8:14:3,(j-1)*Nm+index_1D)=
     &              cluster_ms_pack(8:14:3,(j-1)*Nm+index_1D)
     &                                                 +q(i,8:14:3)*txyz
C    #################################################################### 
                    tx=tx*dx
                   END DO
                   ty=ty*dy
                END DO
                tz=tz*dz
             END DO
          END DO
C    ####################################################################
      END DO
      cluster_ms_pack(9,:) =cluster_ms_pack(8,:)
      cluster_ms_pack(10,:)=cluster_ms_pack(8,:)
      cluster_ms_pack(12,:)=cluster_ms_pack(11,:)
      cluster_ms_pack(13,:)=cluster_ms_pack(11,:)
      cluster_ms_pack(15,:)=cluster_ms_pack(14,:)
      cluster_ms_pack(16,:)=cluster_ms_pack(14,:)
C####################################################################
      RETURN
      END SUBROUTINE COMPUTE_MOMENTS_PACKED

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE COMP_MS(p,x,y,z,q,numpars)
      IMPLICIT NONE
C
C COMP_MS computes the moments for node P needed in the Taylor approximation
C
      INTEGER,INTENT(IN) :: numpars
      TYPE(tnode),POINTER :: p 
      REAL(KIND=r8),DIMENSION(numpars),INTENT(IN) :: x,y,z
C##################################################################
      REAL(KIND=r8),DIMENSION(numpars,16),INTENT(IN) ::q
C##################################################################

C local variables

      INTEGER :: i,k1,k2,k3,j
      REAL(KIND=r8) :: dx,dy,dz,tx,ty,tz,txyz
     
      p%ms=0.0_r8
      DO i=p%ibeg,p%iend
         dx=x(i)-p%x_mid
         dy=y(i)-p%y_mid
         dz=z(i)-p%z_mid
         tz=1.0_r8
         DO k3=0,torder
            ty=1.0_r8
            DO k2=0,torder-k3
               tx=1.0_r8 
               DO k1=0,torder-k3-k2
C####################################################################
				txyz=tx*ty*tz
C				p%ms(k1,k2,k3,1:7)=p%ms(k1,k2,k3,1:7)+q(i,1:7)*txyz
C				p%ms(k1,k2,k3,8:14:3)=p%ms(k1,k2,k3,8:14:3)+q(i,8:14:3)*txyz
				p%ms(1:7,k1,k2,k3)=p%ms(1:7,k1,k2,k3)+q(i,1:7)*txyz
				p%ms(8:14:3,k1,k2,k3)=p%ms(8:14:3,k1,k2,k3)+q(i,8:14:3)*txyz
C#################################################################### 
				tx=tx*dx
               END DO
               ty=ty*dy
            END DO
            tz=tz*dz
         END DO
      END DO
C####################################################################
      p%ms(9,:,:,:) =p%ms(8,:,:,:)
      p%ms(10,:,:,:)=p%ms(8,:,:,:)
      p%ms(12,:,:,:)=p%ms(11,:,:,:)
      p%ms(13,:,:,:)=p%ms(11,:,:,:)
      p%ms(15,:,:,:)=p%ms(14,:,:,:)
      p%ms(16,:,:,:)=p%ms(14,:,:,:)
C      p%ms(:,:,:,9) =p%ms(:,:,:,8)
C      p%ms(:,:,:,10)=p%ms(:,:,:,8)
C      p%ms(:,:,:,12)=p%ms(:,:,:,11)
C      p%ms(:,:,:,13)=p%ms(:,:,:,11)
C      p%ms(:,:,:,15)=p%ms(:,:,:,14)
C      p%ms(:,:,:,16)=p%ms(:,:,:,14)
C####################################################################
      RETURN
      END SUBROUTINE COMP_MS
      
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE COMPP_DIRECT_PB(peng,ibeg,iend,x,y,z,
     & tpoten,kappa,numpars,eps)
      use treecode, only: tr_area,tr_q
      IMPLICIT NONE
      real*8, external:: H1,H2,H3,H4
C
C COMPF_DIRECT directly computes the force on the current target
C particle determined by the global variable TARPOS. 
C
      INTEGER,INTENT(IN) :: ibeg,iend,numpars
      REAL*8,DIMENSION(numpars),INTENT(IN) :: x,y,z
      REAL*8,DIMENSION(2*numpars),INTENT(IN) :: tpoten
      REAL*8,INTENT(IN) :: kappa,eps
      REAL*8,INTENT(OUT) :: peng(2)

C local variables

      INTEGER :: j
      REAL*8 :: dist2,dist,tx,ty,tz,soupos(3),souq(3)
      real*8 :: peng_old(2),L1,L2,L3,L4, area,temp_area

C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      real*8 :: r(3),s(3),v(3),v0(3),pi,rs,cos_theta,cos_theta0,kappa_rs
      real*8 :: G0,Gk,G10,G20,G1,G2,G3,G4,one_over_4pi,exp_kappa_rs
	real*8 :: tp1,tp2,tp3
      common // pi,one_over_4pi
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      peng=0.0_r8

      DO j=ibeg,iend
C###########################################
C The following content has to be precisely fast
C Since it is the part called most
         !soupos=(/x(j),y(j),z(j)/)
         !souq=tr_q(:,j)
C########################################### 
C Temperarily delete the singular part        
C         if (dist <1.d-10) then
C				goto 1022
C         endif
C############################################  
C		  L1=H1(souq,soupos,tarpos,eps,kappa)
C		  L2=H2(soupos,tarpos,kappa)
C		  L3=H3(tarq,souq,soupos,tarpos,kappa)
C		  L4=H4(tarq,soupos,tarpos,kappa,eps)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
          r=(/x(j),y(j),z(j)/)
          v=tr_q(:,j)
          
          s=tarpos
          v0=tarq
          rs=sqrt(dot_product(r-s,r-s))
!          if (rs<1.d-6) goto 1022    
          G0=one_over_4pi/rs
          kappa_rs=kappa*rs
          exp_kappa_rs=exp(-kappa_rs)
          Gk=exp_kappa_rs*G0
          
          cos_theta=dot_product(v,r-s)/rs
          cos_theta0=dot_product(v0,r-s)/rs

          tp1=G0/rs
          tp2=(1.d0+kappa_rs)*exp_kappa_rs
         
          G10=cos_theta0*tp1
          G20=tp2*G10

          G1=cos_theta*tp1
          G2=tp2*G1

          G3=(dot_product(v0,v)-3.d0*cos_theta0*cos_theta)/rs*tp1
          G4=tp2*G3-kappa**2*cos_theta0*cos_theta*Gk

          L1=G1-eps*G2
          L2=G0-Gk
          L3=G4-G3
          L4=G10-G20/eps
 
          peng_old(1)=tpoten(j)
          peng_old(2)=tpoten(j+numpars)
          area=tr_area(j)
          peng(1)=peng(1)+(L1*peng_old(1)+L2*peng_old(2))*area
          peng(2)=peng(2)+(L3*peng_old(1)+L4*peng_old(2))*area
C#############################################
!1022	continue         
      END DO   

	!print *,'finishing direct computing peng:= ', peng
      RETURN
      END SUBROUTINE COMPP_DIRECT_PB


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE CLEANUP(p)
      IMPLICIT NONE
C
C CLEANUP deallocates allocated global variables and then
C calls recursive routine REMOVE_NODE to delete the tree.
C
      TYPE(tnode),POINTER :: p      

C local variables
  
      INTEGER :: err

      DEALLOCATE(cf, STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error deallocating Taylor variables! '
         STOP
      END IF

      DEALLOCATE(cf1,cf2,cf3,STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error deallocating Taylor variables! '
         STOP
      END IF

      DEALLOCATE(a,b,STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error deallocating Taylor variables! '
         STOP
      END IF      

      DEALLOCATE(xcopy,ycopy,zcopy,qcopy,orderarr,STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error deallocating copy variables! '
         STOP
      END IF  

      CALL REMOVE_NODE(p)
      DEALLOCATE(p, STAT=err)
      IF (err .NE. 0) THEN
         WRITE(6,*) 'Error deallocating root node! '
         STOP
      END IF 
      NULLIFY(p)         

      RETURN
      END SUBROUTINE CLEANUP

CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      RECURSIVE SUBROUTINE REMOVE_NODE(p)
      IMPLICIT NONE
C
C REMOVE_NODE recursively removes each node from the
C tree and deallocates its memory for MS array if it
C exits.
C
      TYPE(tnode),POINTER :: p 

C local variables

      INTEGER :: i,err

      IF (p%exist_ms .EQ. 1) THEN
         DEALLOCATE(p%ms,STAT=err)
         IF (err .NE. 0) THEN
            WRITE(6,*) 'Error deallocating node MS! '
            STOP
         END IF               
      END IF

      IF (p%num_children .GT. 0) THEN
          DO i=1,p%num_children
            CALL REMOVE_NODE(p%child(i)%p_to_tnode)
            DEALLOCATE(p%child(i)%p_to_tnode,STAT=err)
            IF (err .NE. 0) THEN
               WRITE(6,*) 'Error deallocating node child! '
               STOP
            END IF                           
          END DO
      END IF 

      RETURN                
      END SUBROUTINE REMOVE_NODE    

C#######################################################################
      RECURSIVE SUBROUTINE REMOVE_MMT(p)
      IMPLICIT NONE
C
C REMOVE_NODE recursively removes each node from the
C tree and deallocates its memory for MS array if it
C exits.
C
      TYPE(tnode),POINTER :: p 

C local variables

      INTEGER :: i,err

      IF (p%exist_ms .EQ. 1) THEN
         DEALLOCATE(p%ms,STAT=err)
         IF (err .NE. 0) THEN
            WRITE(6,*) 'Error deallocating node MS! '
            STOP
         END IF
C########################
         p%exist_ms=0
C########################               
      END IF

      IF (p%num_children .GT. 0) THEN
          DO i=1,p%num_children
            CALL REMOVE_MMT(p%child(i)%p_to_tnode)
          END DO
      END IF 

      RETURN                
      END SUBROUTINE REMOVE_MMT     
 
C ##################################################################################################
      SUBROUTINE TREE_COMPP_PB(p,kappa,eps,tpoten)
      use treecode
      IMPLICIT NONE
C
C TREE_COMPF is the driver routine which calls COMPF_TREE for each
C particle, setting the global variable TARPOS and TARCHR before the call. 
C The current target particle's x coordinate and charge are changed
C so that it does not interact with itself. P is the root node of the tree. 
C

      TYPE(tnode),POINTER :: p  
C      REAL(KIND=r8),DIMENSION(numpars),INTENT(INOUT) :: tpoten
      REAL*8 :: kappa,eps
      REAL*8 :: tpoten(2*numpars)
	
C local variables

      INTEGER :: i,j,ikp,indx,kkk(3)
      REAL*8 :: peng(2),tempx,temp_area,sL(4),tpoten_old(2*numpars)
      REAL*8 :: pt_comp(numpars,16,2), kapa,time1,time2,tempq(16,2)
      REAL*8 :: pre1,pre2
      

C      print *,'entering TREE_COMPP_PB'
      pre1=0.5d0*(1.d0+eps)
      pre2=0.5d0*(1.d0+1.d0/eps)
      tpoten_old=tpoten
      call pb_kernel(tpoten_old)
      DO i=1,numpars      
		tarpos(1)=x(i)
		tarpos(2)=y(i)
		tarpos(3)=z(i)
		tarq=tr_q(:,i)
		tempq=tchg(i,:,:)
		peng_old(1)=tpoten_old(i)
		peng_old(2)=tpoten_old(i+numpars)
		peng=0.d0
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C Remove the singularities 
                tempx=x(i)
                temp_area=tr_area(i)
                x(i)=x(i)+100.123456789d0
                tr_area(i)=0.d0
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	
		CALL COMPP_TREE(p,peng,x,y,z,schg,tpoten_old,
     &		kappa,theta,numpars,kk,eps,tempq,der_cof)
		tpoten(i)=pre1*peng_old(1)-peng(1)
		tpoten(numpars+i)=pre2*peng_old(2)-peng(2)
C		call cpu_time(time2)
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        x(i)=tempx
        tr_area(i)=temp_area
C%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
C		print *,'cpu time for one p-c intercation: ',i,real(time2-time1)
	ENDDO
	

      RETURN
      END SUBROUTINE TREE_COMPP_PB
	 

      END MODULE treecode3d_procedures


CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      SUBROUTINE PARTITION(a,b,c,q,indarr,ibeg,iend,val,midind,numpars)
      IMPLICIT NONE
C
C PARTITION determines the index MIDIND, after partitioning
C in place the  arrays A,B,C and Q,  such that 
C A(IBEG:MIDIND) <= VAL and  A(MIDIND+1:IEND) > VAL. 
C If on entry IBEG > IEND or  A(IBEG:IEND) > VAL then MIDIND
C is returned as IBEG-1. 
C 
      INTEGER,PARAMETER :: r8=SELECTED_REAL_KIND(12)
      INTEGER, INTENT(IN) :: numpars,ibeg,iend
      REAL(KIND=r8),DIMENSION(numpars),INTENT(INOUT) :: a,b,c,q
      INTEGER,DIMENSION(numpars),INTENT(INOUT) :: indarr
C     INTEGER,DIMENSION(1),INTENT(INOUT) :: indarr   
      INTEGER, INTENT(INOUT) :: midind   
      REAL(KIND=r8) val

C local variables

      REAL(KIND=r8) ta,tb,tc,tq
      INTEGER lower,upper,tind

      IF (ibeg .LT. iend) THEN

C temporarily store IBEG entries and set A(IBEG)=VAL for 
C the partitoning algorithm.  

         ta=a(ibeg)
         tb=b(ibeg)
         tc=c(ibeg)
         tq=q(ibeg)
         tind=indarr(ibeg)
         a(ibeg)=val 
         upper=ibeg
         lower=iend

         DO WHILE (upper .NE. lower)
            DO WHILE ((upper .LT. lower) .AND. (val .LT. a(lower)))
                  lower=lower-1
            END DO
            IF (upper .NE. lower) THEN
               a(upper)=a(lower)
               b(upper)=b(lower)
               c(upper)=c(lower)
               q(upper)=q(lower)
               indarr(upper)=indarr(lower)
            END IF
            DO WHILE ((upper .LT. lower) .AND. (val .GE. a(upper)))
                  upper=upper+1
            END DO
            IF (upper .NE. lower) THEN
               a(lower)=a(upper)
               b(lower)=b(upper)
               c(lower)=c(upper)
               q(lower)=q(upper)
               indarr(lower)=indarr(upper)
            END IF
         END DO
         midind=upper

C replace TA in position UPPER and change MIDIND if TA > VAL 

         IF (ta .GT. val) THEN
            midind=upper-1
         END IF
         a(upper)=ta
         b(upper)=tb
         c(upper)=tc
         q(upper)=tq
         indarr(upper)=tind

      ELSEIF (ibeg .EQ. iend) THEN
         IF (a(ibeg) .LE. val) THEN
            midind=ibeg
         ELSE
            midind=ibeg-1
         END IF
      ELSE
         midind=ibeg-1
      END IF

      RETURN
      END SUBROUTINE PARTITION


      
