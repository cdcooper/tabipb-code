#include <stdio.h>
#include <math.h>
#include "cuda.h"

#define BSZ 128


__device__ void getCoeff_2(double *a, double *b, double dx, double dy, double dz, int P_low, double kappa, int threadID)
{
      int P_shift = P_low+2; // P required for derivatives
      int P = P_shift+1; // P goes from 0 to P_shift: P_shift +1 positions

      int P2 = P*P;
      int beg = P*P2*threadID;

//      double ddx=2.0*dx;
//      double ddy=2.0*dy;
//      double ddz=2.0*dz;

      double kappax=kappa*dx;
      double kappay=kappa*dy;
      double kappaz=kappa*dz;

      double dist2=dx*dx+dy*dy+dz*dz;
      double fac=1.0/dist2;
      
      double dist=sqrt(dist2);
      double C1,C2,C3;

      for (int i=0; i<P*P2; i++)
      {
        a[beg+i] = 0.0;
        b[beg+i] = 0.0;
      }


// 0th coeff or function val
      b[beg+0*P2+0*P+0]=exp(-kappa*dist);
      a[beg+0*P2+0*P+0]=b[beg+0*P2+0*P+0]/dist;

// 2 indices are 0

      b[beg+1*P2+0*P+0]=kappax*a[beg+0*P2+0*P+0];
      b[beg+0*P2+1*P+0]=kappay*a[beg+0*P2+0*P+0];
      b[beg+0*P2+0*P+1]=kappaz*a[beg+0*P2+0*P+0];

      a[beg+1*P2+0*P+0]=fac*dx*(a[beg+0*P2+0*P+0]+kappa*b[beg+0*P2+0*P+0]);
      a[beg+0*P2+1*P+0]=fac*dy*(a[beg+0*P2+0*P+0]+kappa*b[beg+0*P2+0*P+0]);
      a[beg+0*P2+0*P+1]=fac*dz*(a[beg+0*P2+0*P+0]+kappa*b[beg+0*P2+0*P+0]);

      for (int i=2; i<P_shift+1; i++)
      {
         C1 = 1.0/i;
         C2 = 1-0.5*C1;
         C3 = 1-C1;
         C1 *= kappa;

         b[beg+i*P2+0*P+0]=C1*(dx*a[beg+(i-1)*P2+0*P+0]-a[beg+(i-2)*P2+0*P+0]);
         b[beg+0*P2+i*P+0]=C1*(dy*a[beg+0*P2+(i-1)*P+0]-a[beg+0*P2+(i-2)*P+0]);
         b[beg+0*P2+0*P+i]=C1*(dz*a[beg+0*P2+0*P+i-1]-a[beg+0*P2+0*P+i-2]);

         a[beg+i*P2+0*P+0]=fac*((2.0*dx)*C2*a[beg+(i-1)*P2+0*P+0]-
                            C3*a[beg+(i-2)*P2+0*P+0]+
                            C1*(dx*b[beg+(i-1)*P2+0*P+0]-b[beg+(i-2)*P2+0*P+0]));
         a[beg+0*P2+i*P+0]=fac*((2.0*dy)*C2*a[beg+0*P2+(i-1)*P+0]-
                            C3*a[beg+0*P2+(i-2)*P+0]+
                            C1*(dy*b[beg+0*P2+(i-1)*P+0]-b[beg+0*P2+(i-2)*P+0]));
         a[beg+0*P2+0*P+i]=fac*((2.0*dz)*C2*a[beg+0*P2+0*P+i-1]-
                            C3*a[beg+0*P2+0*P+i-2]+
                            C1*(dz*b[beg+0*P2+0*P+i-1]-b[beg+0*P2+0*P+i-2]));
      }

// 1 index 0, 1 index 1, other >=1

      b[beg+1*P2+1*P+0]=kappax*a[beg+0*P2+1*P+0];
      b[beg+1*P2+0*P+1]=kappax*a[beg+0*P2+0*P+1];
      b[beg+0*P2+1*P+1]=kappay*a[beg+0*P2+0*P+1];

      a[beg+1*P2+1*P+0]=fac*(dx*a[beg+0*P2+1*P+0]+(2.0*dy)*a[beg+1*P2+0*P+0]+kappax*b[beg+0*P2+1*P+0]);
      a[beg+1*P2+0*P+1]=fac*(dx*a[beg+0*P2+0*P+1]+(2.0*dz)*a[beg+1*P2+0*P+0]+kappax*b[beg+0*P2+0*P+1]);
      a[beg+0*P2+1*P+1]=fac*(dy*a[beg+0*P2+0*P+1]+(2.0*dz)*a[beg+0*P2+1*P+0]+kappay*b[beg+0*P2+0*P+1]);

      for (int i=2; i<P_shift; i++)
      {
         b[beg+1*P2+0*P+i]=kappax*a[beg+0*P2+0*P+i];
         b[beg+0*P2+1*P+i]=kappay*a[beg+0*P2+0*P+i];
         b[beg+0*P2+i*P+1]=kappaz*a[beg+0*P2+i*P+0];
         b[beg+1*P2+i*P+0]=kappax*a[beg+0*P2+i*P+0];
         b[beg+i*P2+1*P+0]=kappay*a[beg+i*P2+0*P+0];
         b[beg+i*P2+0*P+1]=kappaz*a[beg+i*P2+0*P+0];

         a[beg+1*P2+0*P+i]=fac*(dx*a[beg+0*P2+0*P+i]+(2.0*dz)*a[beg+1*P2+0*P+i-1]-a[beg+1*P2+0*P+i-2]+
                 kappax*b[beg+0*P2+0*P+i]);
         a[beg+0*P2+1*P+i]=fac*(dy*a[beg+0*P2+0*P+i]+(2.0*dz)*a[beg+0*P2+1*P+i-1]-a[beg+0*P2+1*P+i-2]+
                 kappay*b[beg+0*P2+0*P+i]);
         a[beg+0*P2+i*P+1]=fac*(dz*a[beg+0*P2+i*P+0]+(2.0*dy)*a[beg+0*P2+(i-1)*P+1]-a[beg+0*P2+(i-2)*P+1]+
                 kappaz*b[beg+0*P2+i*P+0]);
         a[beg+1*P2+i*P+0]=fac*(dx*a[beg+0*P2+i*P+0]+(2.0*dy)*a[beg+1*P2+(i-1)*P+0]-a[beg+1*P2+(i-2)*P+0]+
                 kappax*b[beg+0*P2+i*P+0]);
         a[beg+i*P2+1*P+0]=fac*(dy*a[beg+i*P2+0*P+0]+(2.0*dx)*a[beg+(i-1)*P2+1*P+0]-a[beg+(i-2)*P2+1*P+0]+
                 kappay*b[beg+i*P2+0*P+0]);
         a[beg+i*P2+0*P+1]=fac*(dz*a[beg+i*P2+0*P+0]+(2.0*dx)*a[beg+(i-1)*P2+0*P+1]-a[beg+(i-2)*P2+0*P+1]+
                 kappaz*b[beg+i*P2+0*P+0]);         
      }

// 1 index 0, others >= 2

      for (int i=2; i<P_shift-1; i++)
      {
         C1 = 1.0/i;
         C2 = 1-0.5*C1;
         C3 = 1-C1;
         C1 *= kappa;

         for (int j=2; j<P_shift-i+1; j++)
         {
            b[beg+i*P2+j*P+0]=C1*(dx*a[beg+(i-1)*P2+j*P+0]-a[beg+(i-2)*P2+j*P+0]);
            b[beg+i*P2+0*P+j]=C1*(dx*a[beg+(i-1)*P2+0*P+j]-a[beg+(i-2)*P2+0*P+j]);
            b[beg+0*P2+i*P+j]=C1*(dy*a[beg+0*P2+(i-1)*P+j]-a[beg+0*P2+(i-2)*P+j]);

            a[beg+i*P2+j*P+0]=fac*((2.0*dx)*C2*a[beg+(i-1)*P2+j*P+0]+(2.0*dy)*a[beg+i*P2+(j-1)*P+0]
                    -C3*a[beg+(i-2)*P2+j*P+0]-a[beg+i*P2+(j-2)*P+0]+
                    C1*(dx*b[beg+(i-1)*P2+j*P+0]-b[beg+(i-2)*P2+j*P+0]));
            a[beg+i*P2+0*P+j]=fac*((2.0*dx)*C2*a[beg+(i-1)*P2+0*P+j]+(2.0*dz)*a[beg+i*P2+0*P+j-1]
                    -C3*a[beg+(i-2)*P2+0*P+j]-a[beg+i*P2+0*P+j-2]+
                    C1*(dx*b[beg+(i-1)*P2+0*P+j]-b[beg+(i-2)*P2+0*P+j]));
            a[beg+0*P2+i*P+j]=fac*((2.0*dy)*C2*a[beg+0*P2+(i-1)*P+j]+(2.0*dz)*a[beg+0*P2+i*P+j-1]
                    -C3*a[beg+0*P2+(i-2)*P+j]-a[beg+0*P2+i*P+j-2]+
                    C1*(dy*b[beg+0*P2+(i-1)*P+j]-b[beg+0*P2+(i-2)*P+j]));
         }
      } 

// 2 indices 1, other >= 1
// b[beg+1*P2+1*P+1] is correct, but a little tricky!
//      b[beg+1*P2+1*P+1]=5.0*dz*fac*b[beg+1*P2+1*P+0]

      b[beg+1*P2+1*P+1]=kappax*a[beg+0*P2+1*P+1];
      a[beg+1*P2+1*P+1]=fac*(dx*a[beg+0*P2+1*P+1]+(2.0*dy)*a[beg+1*P2+0*P+1]+(2.0*dz)*a[beg+1*P2+1*P+0]+
              kappax*b[beg+0*P2+1*P+1]);

      for (int i=2; i<P_shift-1; i++)
      {
         b[beg+1*P2+1*P+i]=kappax*a[beg+0*P2+1*P+i];
         b[beg+1*P2+i*P+1]=kappax*a[beg+0*P2+i*P+1];
         b[beg+i*P2+1*P+1]=kappay*a[beg+i*P2+0*P+1];

         a[beg+1*P2+1*P+i]=fac*(dx*a[beg+0*P2+1*P+i]+(2.0*dy)*a[beg+1*P2+0*P+i]+(2.0*dz)*a[beg+1*P2+1*P+i-1]
                -a[beg+1*P2+1*P+i-2]+kappax*b[beg+0*P2+1*P+i]);
         a[beg+1*P2+i*P+1]=fac*(dx*a[beg+0*P2+i*P+1]+(2.0*dy)*a[beg+1*P2+(i-1)*P+1]+(2.0*dz)*a[beg+1*P2+i*P+0]
                -a[beg+1*P2+(i-2)*P+1]+kappax*b[beg+0*P2+i*P+1]);
         a[beg+i*P2+1*P+1]=fac*(dy*a[beg+i*P2+0*P+1]+(2.0*dx)*a[beg+(i-1)*P2+1*P+1]+(2.0*dz)*a[beg+i*P2+1*P+0]
                -a[beg+(i-2)*P2+1*P+1]+kappay*b[beg+i*P2+0*P+1]);
      }

// 1 index 1, others >=2

      for (int i=2; i<P_shift-2; i++)
      {
         for (int j=2; j<P_shift-i+1; j++)
         {
            b[beg+1*P2+i*P+j]=kappax*a[beg+0*P2+i*P+j];
            b[beg+i*P2+1*P+j]=kappay*a[beg+i*P2+0*P+j];
            b[beg+i*P2+j*P+1]=kappaz*a[beg+i*P2+j*P+0];

            //a[beg+1*P2+i*P+j]=fac*(dx*a[beg+0*P2+i*P+j]+(2.0*dy)*a[beg+1*P2+(i-1)*P+j]+(2.0*dz)*a[beg+1*P2+i*P+j-1]
            //       -a[beg+1*P2+(i-2)*P+j]-a[beg+1*P2+i*P+j-2]+kappax*b[beg+0*P2+i*P+j]);
            //a[beg+i*P2+1*P+j]=fac*(dy*a[beg+i*P2+0*P+j]+(2.0*dx)*a[beg+(i-1)*P2+1*P+j]+(2.0*dz)*a[beg+i*P2+1*P+j-1]
            //       -a[beg+(i-2)*P2+1*P+j]-a[beg+i*P2+1*P+j-2]+kappay*b[beg+i*P2+0*P+j]);
            a[beg+i*P2+j*P+1]=0;//fac*(dz*a[beg+i*P2+j*P+0]+(2.0*dx)*a[beg+(i-1)*P2+j*P+1]+(2.0*dy)*a[beg+i*P2+(j-1)*P+1]
            //       -a[beg+(i-2)*P2+j*P+1]-a[beg+i*P2+(j-2)*P+1]+kappaz*b[beg+i*P2+j*P+0]);

         }
      }

// all indices >=2

      for (int i=2; i<P_shift-3; i++)
      {
         C1 = 1.0/i;
         C2 = 1-0.5*C1;
         C3 = 1-C1;
         C1 *= kappa;

         for (int j=2; j<P_shift-i-1; j++)
         {
            for (int k=2; k<P_shift-i-j+1; k++)
            {
               b[beg+i*P2+j*P+k]=C1*(dx*a[beg+(i-1)*P2+j*P+k]-a[beg+(i-2)*P2+j*P+k]);

               a[beg+i*P2+j*P+k]=fac*((2.0*dx)*C2*a[beg+(i-1)*P2+j*P+k]+(2.0*dy)*a[beg+i*P2+(j-1)*P+k]
                      +(2.0*dz)*a[beg+i*P2+j*P+k-1]-C3*a[beg+(i-2)*P2+j*P+k]
                      -a[beg+i*P2+(j-2)*P+k]-a[beg+i*P2+j*P+k-2]+
                      (kappa/i)*(dx*b[beg+(i-1)*P2+j*P+k]-b[beg+(i-2)*P2+j*P+k]));
            } 
         } 
      }
}

__device__ void getCoeff_1D_yuk(double *a, double *b, double dx, double dy, double dz, int P_low, int Nm2, double kappa, int threadID, int *index_3Dto1D)
{
      int P_shift = P_low+2; // P required for derivatives
      int P = P_shift+1; // P goes from 0 to P_shift: P_shift +1 positions

      int P2 = P*P;
      int beg = Nm2*threadID;

//      double ddx=2.0*dx;
//      double ddy=2.0*dy;
//      double ddz=2.0*dz;

      double kappax=kappa*dx;
      double kappay=kappa*dy;
      double kappaz=kappa*dz;

      double dist2=dx*dx+dy*dy+dz*dz;
      double fac=1.0/dist2;
      
      double dist=sqrt(dist2);
      double C1,C2,C3;

      int I, Im1x, Im2x, Im1y, Im2y, Im1z, Im2z;

      for (int i=0; i<Nm2; i++)
      {
        a[beg+i] = 0.0;
        b[beg+i] = 0.0;
      }


// 0th coeff or function val
      b[beg+index_3Dto1D[0*P2+0*P+0]]=exp(-kappa*dist);
      a[beg+index_3Dto1D[0*P2+0*P+0]]=b[beg+index_3Dto1D[0*P2+0*P+0]]/dist;

// 2 indices are 0

      b[beg+index_3Dto1D[1*P2+0*P+0]]=kappax*a[beg+index_3Dto1D[0*P2+0*P+0]];
      b[beg+index_3Dto1D[0*P2+1*P+0]]=kappay*a[beg+index_3Dto1D[0*P2+0*P+0]];
      b[beg+index_3Dto1D[0*P2+0*P+1]]=kappaz*a[beg+index_3Dto1D[0*P2+0*P+0]];

      a[beg+index_3Dto1D[1*P2+0*P+0]]=fac*dx*(a[beg+index_3Dto1D[0*P2+0*P+0]]+kappa*b[beg+index_3Dto1D[0*P2+0*P+0]]);
      a[beg+index_3Dto1D[0*P2+1*P+0]]=fac*dy*(a[beg+index_3Dto1D[0*P2+0*P+0]]+kappa*b[beg+index_3Dto1D[0*P2+0*P+0]]);
      a[beg+index_3Dto1D[0*P2+0*P+1]]=fac*dz*(a[beg+index_3Dto1D[0*P2+0*P+0]]+kappa*b[beg+index_3Dto1D[0*P2+0*P+0]]);
    
      for (int i=2; i<P_shift+1; i++)
      {
         C1 = 1.0/i;
         C2 = 1-0.5*C1;
         C3 = 1-C1;
         C1 *= kappa;

         b[beg+index_3Dto1D[i*P2+0*P+0]]=C1*(dx*a[beg+index_3Dto1D[(i-1)*P2+0*P+0]]-a[beg+index_3Dto1D[(i-2)*P2+0*P+0]]);
         b[beg+index_3Dto1D[0*P2+i*P+0]]=C1*(dy*a[beg+index_3Dto1D[0*P2+(i-1)*P+0]]-a[beg+index_3Dto1D[0*P2+(i-2)*P+0]]);
         b[beg+index_3Dto1D[0*P2+0*P+i]]=C1*(dz*a[beg+index_3Dto1D[0*P2+0*P+i-1]]-a[beg+index_3Dto1D[0*P2+0*P+i-2]]);

         a[beg+index_3Dto1D[i*P2+0*P+0]]=fac*((2.0*dx)*C2*a[beg+index_3Dto1D[(i-1)*P2+0*P+0]]-
                            C3*a[beg+index_3Dto1D[(i-2)*P2+0*P+0]]+
                            C1*(dx*b[beg+index_3Dto1D[(i-1)*P2+0*P+0]]-b[beg+index_3Dto1D[(i-2)*P2+0*P+0]]));
         a[beg+index_3Dto1D[0*P2+i*P+0]]=fac*((2.0*dy)*C2*a[beg+index_3Dto1D[0*P2+(i-1)*P+0]]-
                            C3*a[beg+index_3Dto1D[0*P2+(i-2)*P+0]]+
                            C1*(dy*b[beg+index_3Dto1D[0*P2+(i-1)*P+0]]-b[beg+index_3Dto1D[0*P2+(i-2)*P+0]]));
         a[beg+index_3Dto1D[0*P2+0*P+i]]=fac*((2.0*dz)*C2*a[beg+index_3Dto1D[0*P2+0*P+i-1]]-
                            C3*a[beg+index_3Dto1D[0*P2+0*P+i-2]]+
                            C1*(dz*b[beg+index_3Dto1D[0*P2+0*P+i-1]]-b[beg+index_3Dto1D[0*P2+0*P+i-2]]));
      }

// 1 index 0, 1 index 1, other >=1

      b[beg+index_3Dto1D[1*P2+1*P+0]]=kappax*a[beg+index_3Dto1D[0*P2+1*P+0]];
      b[beg+index_3Dto1D[1*P2+0*P+1]]=kappax*a[beg+index_3Dto1D[0*P2+0*P+1]];
      b[beg+index_3Dto1D[0*P2+1*P+1]]=kappay*a[beg+index_3Dto1D[0*P2+0*P+1]];

      a[beg+index_3Dto1D[1*P2+1*P+0]]=fac*(dx*a[beg+index_3Dto1D[0*P2+1*P+0]]+(2.0*dy)*a[beg+index_3Dto1D[1*P2+0*P+0]]+kappax*b[beg+index_3Dto1D[0*P2+1*P+0]]);
      a[beg+index_3Dto1D[1*P2+0*P+1]]=fac*(dx*a[beg+index_3Dto1D[0*P2+0*P+1]]+(2.0*dz)*a[beg+index_3Dto1D[1*P2+0*P+0]]+kappax*b[beg+index_3Dto1D[0*P2+0*P+1]]);
      a[beg+index_3Dto1D[0*P2+1*P+1]]=fac*(dy*a[beg+index_3Dto1D[0*P2+0*P+1]]+(2.0*dz)*a[beg+index_3Dto1D[0*P2+1*P+0]]+kappay*b[beg+index_3Dto1D[0*P2+0*P+1]]);


      for (int i=2; i<P_shift; i++)
      {
         
         b[beg+index_3Dto1D[P2+i]]=kappax*a[beg+i];
         b[beg+index_3Dto1D[P+i]]=kappay*a[beg+i];
         b[beg+index_3Dto1D[i*P+1]]=kappaz*a[beg+index_3Dto1D[i*P]];
         b[beg+index_3Dto1D[P2+i*P]]=kappax*a[beg+index_3Dto1D[i*P]];
         b[beg+index_3Dto1D[i*P2+P]]=kappay*a[beg+index_3Dto1D[i*P2]];
         b[beg+index_3Dto1D[i*P2+1]]=kappaz*a[beg+index_3Dto1D[i*P2]];

         a[beg+index_3Dto1D[P2+i]]=fac*(dx*a[beg+index_3Dto1D[i]]+(2.0*dz)*a[beg+index_3Dto1D[P2+i-1]]-a[beg+index_3Dto1D[P2+i-2]]+
                 kappax*b[beg+index_3Dto1D[i]]);
         a[beg+index_3Dto1D[P+i]]=fac*(dy*a[beg+index_3Dto1D[i]]+(2.0*dz)*a[beg+index_3Dto1D[P+i-1]]-a[beg+index_3Dto1D[P+i-2]]+
                 kappay*b[beg+index_3Dto1D[i]]);
         a[beg+index_3Dto1D[i*P+1]]=fac*(dz*a[beg+index_3Dto1D[i*P]]+(2.0*dy)*a[beg+index_3Dto1D[(i-1)*P+1]]-a[beg+index_3Dto1D[(i-2)*P+1]]+
                 kappaz*b[beg+index_3Dto1D[i*P]]);
         a[beg+index_3Dto1D[P2+i*P]]=fac*(dx*a[beg+index_3Dto1D[i*P]]+(2.0*dy)*a[beg+index_3Dto1D[P2+(i-1)*P]]-a[beg+index_3Dto1D[P2+(i-2)*P]]+
                 kappax*b[beg+index_3Dto1D[i*P]]);
         a[beg+index_3Dto1D[i*P2+P]]=fac*(dy*a[beg+index_3Dto1D[i*P2]]+(2.0*dx)*a[beg+index_3Dto1D[(i-1)*P2+P]]-a[beg+index_3Dto1D[(i-2)*P2+P]]+
                 kappay*b[beg+index_3Dto1D[i*P2]]);
         a[beg+index_3Dto1D[i*P2+1]]=fac*(dz*a[beg+index_3Dto1D[i*P2]]+(2.0*dx)*a[beg+index_3Dto1D[(i-1)*P2+1]]-a[beg+index_3Dto1D[(i-2)*P2+1]]+
                 kappaz*b[beg+index_3Dto1D[i*P2]]);         
      }

// 1 index 0, others >= 2

      for (int i=2; i<P_shift-1; i++)
      {
         C1 = 1.0/i;
         C2 = 1-0.5*C1;
         C3 = 1-C1;
         C1 *= kappa;

         for (int j=2; j<P_shift-i+1; j++)
         {
            I = index_3Dto1D[i*P2+j*P];
            Im1x = index_3Dto1D[(i-1)*P2+j*P];
            Im2x = index_3Dto1D[(i-2)*P2+j*P];
            Im1y = index_3Dto1D[i*P2+(j-1)*P];
            Im2y = index_3Dto1D[i*P2+(j-2)*P];
            b[beg+I]=C1*(dx*a[beg+Im1x]-a[beg+Im2x]);
            a[beg+I]=fac*((2.0*dx)*C2*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]
                    -C3*a[beg+Im2x]-a[beg+Im2y]+
                    C1*(dx*b[beg+Im1x]-b[beg+Im2x]));

            I = index_3Dto1D[i*P2+j];
            Im1x = index_3Dto1D[(i-1)*P2+j];
            Im2x = index_3Dto1D[(i-2)*P2+j];
            Im1z = index_3Dto1D[i*P2+j-1];
            Im2z = index_3Dto1D[i*P2+j-2];
            b[beg+I]=C1*(dx*a[beg+Im1x]-a[beg+Im2x]);
            a[beg+I]=fac*((2.0*dx)*C2*a[beg+Im1x]+(2.0*dz)*a[beg+Im1z]
                    -C3*a[beg+Im2x]-a[beg+Im2z]+
                    C1*(dx*b[beg+Im1x]-b[beg+Im2x]));

            I = index_3Dto1D[i*P+j];
            Im1y = index_3Dto1D[(i-1)*P+j];
            Im2y = index_3Dto1D[(i-2)*P+j];
            Im1z = index_3Dto1D[i*P+j-1];
            Im2z = index_3Dto1D[i*P+j-2];
            b[beg+I]=C1*(dy*a[beg+Im1y]-a[beg+Im2y]);
            a[beg+I]=fac*((2.0*dy)*C2*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]
                    -C3*a[beg+Im2y]-a[beg+Im2z]+
                    C1*(dy*b[beg+Im1y]-b[beg+Im2y]));
         }
      } 

// 2 indices 1, other >= 1
// b[beg+1*P2+1*P+1] is correct, but a little tricky!
//      b[beg+1*P2+1*P+1]=5.0*dz*fac*b[beg+1*P2+1*P+0]
      
      I = index_3Dto1D[P2+P+1];
      Im1x = index_3Dto1D[P+1];
      Im1y = index_3Dto1D[P2+1];
      Im1z =  index_3Dto1D[P2+P];
      b[beg+I]=kappax*a[beg+Im1x];
      a[beg+I]=fac*(dx*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]+
              kappax*b[beg+Im1x]);

      for (int i=2; i<P_shift-1; i++)
      {
         I = index_3Dto1D[P2+P+i];
         Im1x = index_3Dto1D[P+i];
         Im1y = index_3Dto1D[P2+i];
         Im1z = index_3Dto1D[P2+P+i-1];
         Im2z = index_3Dto1D[P2+P+i-2];
         b[beg+I]=kappax*a[beg+Im1x];
         a[beg+I]=fac*(dx*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]
                -a[beg+Im2z]+kappax*b[beg+Im1x]);


         I = index_3Dto1D[P2+i*P+1];
         Im1x = index_3Dto1D[i*P+1];
         Im1y = index_3Dto1D[P2+(i-1)*P+1];
         Im2y = index_3Dto1D[P2+(i-2)*P+1];
         Im1z = index_3Dto1D[P2+i*P];
         b[beg+I]=kappax*a[beg+Im1x];
         a[beg+I]=fac*(dx*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]
                -a[beg+Im2y]+kappax*b[beg+Im1x]);

         I = index_3Dto1D[i*P2+P+1];
         Im1x = index_3Dto1D[(i-1)*P2+P+1];
         Im2x = index_3Dto1D[(i-2)*P2+P+1];
         Im1y = index_3Dto1D[i*P2+1];
         Im1z = index_3Dto1D[i*P2+P];
         b[beg+I]=kappay*a[beg+Im1y];
         a[beg+I]=fac*(dy*a[beg+Im1y]+(2.0*dx)*a[beg+Im1x]+(2.0*dz)*a[beg+Im1z]
                -a[beg+Im2x]+kappay*b[beg+Im1y]);

      }
   

// 1 index 1, others >=2

      for (int i=2; i<P_shift-2; i++)
      {
         for (int j=2; j<P_shift-i; j++)
         {
            I    = index_3Dto1D[P2+i*P+j];
            Im1x = index_3Dto1D[i*P+j];
            Im1y = index_3Dto1D[P2+(i-1)*P+j];
            Im2y = index_3Dto1D[P2+(i-2)*P+j];
            Im1z = index_3Dto1D[P2+i*P+j-1];
            Im2z = index_3Dto1D[P2+i*P+j-2];
            b[beg+I]=kappax*a[beg+Im1x];
            a[beg+I]=fac*(dx*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]
                   -a[beg+Im2y]-a[beg+Im2z]+kappax*b[beg+Im1x]);

            I = index_3Dto1D[i*P2+1*P+j];
            Im1x = index_3Dto1D[(i-1)*P2+P+j]; 
            Im2x = index_3Dto1D[(i-2)*P2+P+j]; 
            Im1y = index_3Dto1D[i*P2+j];
            Im1z = index_3Dto1D[i*P2+1*P+j-1];
            Im2z = index_3Dto1D[i*P2+1*P+j-2];
            b[beg+I]=kappay*a[beg+Im1y];
            a[beg+I]=fac*(dy*a[beg+Im1y]+(2.0*dx)*a[beg+Im1x]+(2.0*dz)*a[beg+Im1z]
                   -a[beg+Im2x]-a[beg+Im2z]+kappay*b[beg+Im1y]);

            I = index_3Dto1D[i*P2+j*P+1];
            Im1x = index_3Dto1D[(i-1)*P2+j*P+1]; 
            Im2x = index_3Dto1D[(i-2)*P2+j*P+1]; 
            Im1y = index_3Dto1D[i*P2+(j-1)*P+1];
            Im2y = index_3Dto1D[i*P2+(j-2)*P+1];
            Im1z = index_3Dto1D[i*P2+j*P];
            b[beg+I]=kappaz*a[beg+Im1z];
            if (fabs(a[beg+I])>1e-10)
            a[beg+I]=fac*(dz*a[beg+Im1z]+(2.0*dx)*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]
                   -a[beg+Im2x]-a[beg+Im2y]+kappaz*b[beg+Im1z]);
         }
      }

// all indices >=2

      for (int i=2; i<P_shift-3; i++)
      {
         C1 = 1.0/i;
         C2 = 1-0.5*C1;
         C3 = 1-C1;
         C1 *= kappa;

         for (int j=2; j<P_shift-i-1; j++)
         {
            for (int k=2; k<P_shift-i-j+1; k++)
            {
                I = index_3Dto1D[i*P2+j*P+k];
                Im1x = index_3Dto1D[(i-1)*P2+j*P+k];
                Im2x = index_3Dto1D[(i-2)*P2+j*P+k];
                Im1y = index_3Dto1D[i*P2+(j-1)*P+k];
                Im2y = index_3Dto1D[i*P2+(j-2)*P+k];
                Im1z = index_3Dto1D[i*P2+j*P+k-1];
                Im2z = index_3Dto1D[i*P2+j*P+k-2];
                
                b[beg+I]=C1*(dx*a[beg+Im1x]-a[beg+Im2x]);

                a[beg+I]=fac*((2.0*dx)*C2*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]
                          +(2.0*dz)*a[beg+Im1z]-C3*a[beg+Im2x]
                          -a[beg+Im2y]-a[beg+Im2z]+
                        (kappa/i)*(dx*b[beg+Im1x]-b[beg+Im2x]));
            } 
         } 
      }
}

__device__ void getCoeff_1D_lap(double *a, double dx, double dy, double dz, int P_low, int Nm2, int threadID, int *index_3Dto1D)
{
      int P_shift = P_low+2; // P required for derivatives
      int P = P_shift+1; // P goes from 0 to P_shift: P_shift +1 positions

      int P2 = P*P;
      int beg = Nm2*threadID;

      double dist2=dx*dx+dy*dy+dz*dz;
      double fac=1.0/dist2;
      
      double dist=sqrt(dist2);
      double C1,C2,C3;

      int I, Im1x, Im2x, Im1y, Im2y, Im1z, Im2z;

      for (int i=0; i<Nm2; i++)
      {
        a[beg+i] = 0.0;
      }


// 0th coeff or function val
      a[beg+index_3Dto1D[0*P2+0*P+0]]=1/dist;

// 2 indices are 0

      a[beg+index_3Dto1D[1*P2+0*P+0]]=fac*dx*(a[beg+index_3Dto1D[0*P2+0*P+0]]);
      a[beg+index_3Dto1D[0*P2+1*P+0]]=fac*dy*(a[beg+index_3Dto1D[0*P2+0*P+0]]);
      a[beg+index_3Dto1D[0*P2+0*P+1]]=fac*dz*(a[beg+index_3Dto1D[0*P2+0*P+0]]);
    
      for (int i=2; i<P_shift+1; i++)
      {
         C1 = 1.0/i;
         C2 = 1-0.5*C1;
         C3 = 1-C1;

         a[beg+index_3Dto1D[i*P2+0*P+0]]=fac*((2.0*dx)*C2*a[beg+index_3Dto1D[(i-1)*P2+0*P+0]]-
                            C3*a[beg+index_3Dto1D[(i-2)*P2+0*P+0]]);
         a[beg+index_3Dto1D[0*P2+i*P+0]]=fac*((2.0*dy)*C2*a[beg+index_3Dto1D[0*P2+(i-1)*P+0]]-
                            C3*a[beg+index_3Dto1D[0*P2+(i-2)*P+0]]);
         a[beg+index_3Dto1D[0*P2+0*P+i]]=fac*((2.0*dz)*C2*a[beg+index_3Dto1D[0*P2+0*P+i-1]]-
                            C3*a[beg+index_3Dto1D[0*P2+0*P+i-2]]);
      }

// 1 index 0, 1 index 1, other >=1

      a[beg+index_3Dto1D[1*P2+1*P+0]]=fac*(dx*a[beg+index_3Dto1D[0*P2+1*P+0]]+(2.0*dy)*a[beg+index_3Dto1D[1*P2+0*P+0]]);
      a[beg+index_3Dto1D[1*P2+0*P+1]]=fac*(dx*a[beg+index_3Dto1D[0*P2+0*P+1]]+(2.0*dz)*a[beg+index_3Dto1D[1*P2+0*P+0]]);
      a[beg+index_3Dto1D[0*P2+1*P+1]]=fac*(dy*a[beg+index_3Dto1D[0*P2+0*P+1]]+(2.0*dz)*a[beg+index_3Dto1D[0*P2+1*P+0]]);


      for (int i=2; i<P_shift; i++)
      {
         
         a[beg+index_3Dto1D[P2+i]]=fac*(dx*a[beg+index_3Dto1D[i]]+(2.0*dz)*a[beg+index_3Dto1D[P2+i-1]]-a[beg+index_3Dto1D[P2+i-2]]);
         a[beg+index_3Dto1D[P+i]]=fac*(dy*a[beg+index_3Dto1D[i]]+(2.0*dz)*a[beg+index_3Dto1D[P+i-1]]-a[beg+index_3Dto1D[P+i-2]]);
         a[beg+index_3Dto1D[i*P+1]]=fac*(dz*a[beg+index_3Dto1D[i*P]]+(2.0*dy)*a[beg+index_3Dto1D[(i-1)*P+1]]-a[beg+index_3Dto1D[(i-2)*P+1]]);
         a[beg+index_3Dto1D[P2+i*P]]=fac*(dx*a[beg+index_3Dto1D[i*P]]+(2.0*dy)*a[beg+index_3Dto1D[P2+(i-1)*P]]-a[beg+index_3Dto1D[P2+(i-2)*P]]);
         a[beg+index_3Dto1D[i*P2+P]]=fac*(dy*a[beg+index_3Dto1D[i*P2]]+(2.0*dx)*a[beg+index_3Dto1D[(i-1)*P2+P]]-a[beg+index_3Dto1D[(i-2)*P2+P]]);
         a[beg+index_3Dto1D[i*P2+1]]=fac*(dz*a[beg+index_3Dto1D[i*P2]]+(2.0*dx)*a[beg+index_3Dto1D[(i-1)*P2+1]]-a[beg+index_3Dto1D[(i-2)*P2+1]]);
      }

// 1 index 0, others >= 2

      for (int i=2; i<P_shift-1; i++)
      {
         C1 = 1.0/i;
         C2 = 1-0.5*C1;
         C3 = 1-C1;

         for (int j=2; j<P_shift-i+1; j++)
         {
            I = index_3Dto1D[i*P2+j*P];
            Im1x = index_3Dto1D[(i-1)*P2+j*P];
            Im2x = index_3Dto1D[(i-2)*P2+j*P];
            Im1y = index_3Dto1D[i*P2+(j-1)*P];
            Im2y = index_3Dto1D[i*P2+(j-2)*P];
            a[beg+I]=fac*((2.0*dx)*C2*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]
                    -C3*a[beg+Im2x]-a[beg+Im2y]);

            I = index_3Dto1D[i*P2+j];
            Im1x = index_3Dto1D[(i-1)*P2+j];
            Im2x = index_3Dto1D[(i-2)*P2+j];
            Im1z = index_3Dto1D[i*P2+j-1];
            Im2z = index_3Dto1D[i*P2+j-2];
            a[beg+I]=fac*((2.0*dx)*C2*a[beg+Im1x]+(2.0*dz)*a[beg+Im1z]
                    -C3*a[beg+Im2x]-a[beg+Im2z]);

            I = index_3Dto1D[i*P+j];
            Im1y = index_3Dto1D[(i-1)*P+j];
            Im2y = index_3Dto1D[(i-2)*P+j];
            Im1z = index_3Dto1D[i*P+j-1];
            Im2z = index_3Dto1D[i*P+j-2];
            a[beg+I]=fac*((2.0*dy)*C2*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]
                    -C3*a[beg+Im2y]-a[beg+Im2z]);
         }
      } 

// 2 indices 1, other >= 1
// b[beg+1*P2+1*P+1] is correct, but a little tricky!
//      b[beg+1*P2+1*P+1]=5.0*dz*fac*b[beg+1*P2+1*P+0]
      
      I = index_3Dto1D[P2+P+1];
      Im1x = index_3Dto1D[P+1];
      Im1y = index_3Dto1D[P2+1];
      Im1z =  index_3Dto1D[P2+P];
      a[beg+I]=fac*(dx*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]);

      for (int i=2; i<P_shift-1; i++)
      {
         I = index_3Dto1D[P2+P+i];
         Im1x = index_3Dto1D[P+i];
         Im1y = index_3Dto1D[P2+i];
         Im1z = index_3Dto1D[P2+P+i-1];
         Im2z = index_3Dto1D[P2+P+i-2];
         a[beg+I]=fac*(dx*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]
                -a[beg+Im2z]);


         I = index_3Dto1D[P2+i*P+1];
         Im1x = index_3Dto1D[i*P+1];
         Im1y = index_3Dto1D[P2+(i-1)*P+1];
         Im2y = index_3Dto1D[P2+(i-2)*P+1];
         Im1z = index_3Dto1D[P2+i*P];
         a[beg+I]=fac*(dx*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]
                -a[beg+Im2y]);

         I = index_3Dto1D[i*P2+P+1];
         Im1x = index_3Dto1D[(i-1)*P2+P+1];
         Im2x = index_3Dto1D[(i-2)*P2+P+1];
         Im1y = index_3Dto1D[i*P2+1];
         Im1z = index_3Dto1D[i*P2+P];
         a[beg+I]=fac*(dy*a[beg+Im1y]+(2.0*dx)*a[beg+Im1x]+(2.0*dz)*a[beg+Im1z]
                -a[beg+Im2x]);

      }
   

// 1 index 1, others >=2

      for (int i=2; i<P_shift-2; i++)
      {
         for (int j=2; j<P_shift-i; j++)
         {
            I    = index_3Dto1D[P2+i*P+j];
            Im1x = index_3Dto1D[i*P+j];
            Im1y = index_3Dto1D[P2+(i-1)*P+j];
            Im2y = index_3Dto1D[P2+(i-2)*P+j];
            Im1z = index_3Dto1D[P2+i*P+j-1];
            Im2z = index_3Dto1D[P2+i*P+j-2];
            a[beg+I]=fac*(dx*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]+(2.0*dz)*a[beg+Im1z]
                   -a[beg+Im2y]-a[beg+Im2z]);

            I = index_3Dto1D[i*P2+1*P+j];
            Im1x = index_3Dto1D[(i-1)*P2+P+j]; 
            Im2x = index_3Dto1D[(i-2)*P2+P+j]; 
            Im1y = index_3Dto1D[i*P2+j];
            Im1z = index_3Dto1D[i*P2+1*P+j-1];
            Im2z = index_3Dto1D[i*P2+1*P+j-2];
            a[beg+I]=fac*(dy*a[beg+Im1y]+(2.0*dx)*a[beg+Im1x]+(2.0*dz)*a[beg+Im1z]
                   -a[beg+Im2x]-a[beg+Im2z]);

            I = index_3Dto1D[i*P2+j*P+1];
            Im1x = index_3Dto1D[(i-1)*P2+j*P+1]; 
            Im2x = index_3Dto1D[(i-2)*P2+j*P+1]; 
            Im1y = index_3Dto1D[i*P2+(j-1)*P+1];
            Im2y = index_3Dto1D[i*P2+(j-2)*P+1];
            Im1z = index_3Dto1D[i*P2+j*P];
            a[beg+I]=fac*(dz*a[beg+Im1z]+(2.0*dx)*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]
                   -a[beg+Im2x]-a[beg+Im2y]);
         }
      }

// all indices >=2

      for (int i=2; i<P_shift-3; i++)
      {
         C1 = 1.0/i;
         C2 = 1-0.5*C1;
         C3 = 1-C1;

         for (int j=2; j<P_shift-i-1; j++)
         {
            for (int k=2; k<P_shift-i-j+1; k++)
            {
                I = index_3Dto1D[i*P2+j*P+k];
                Im1x = index_3Dto1D[(i-1)*P2+j*P+k];
                Im2x = index_3Dto1D[(i-2)*P2+j*P+k];
                Im1y = index_3Dto1D[i*P2+(j-1)*P+k];
                Im2y = index_3Dto1D[i*P2+(j-2)*P+k];
                Im1z = index_3Dto1D[i*P2+j*P+k-1];
                Im2z = index_3Dto1D[i*P2+j*P+k-2];
                

                a[beg+I]=fac*((2.0*dx)*C2*a[beg+Im1x]+(2.0*dy)*a[beg+Im1y]
                          +(2.0*dz)*a[beg+Im1z]-C3*a[beg+Im2x]
                          -a[beg+Im2y]-a[beg+Im2z]);
            } 
         } 
      }
}

__device__ void getCoeff(double *a_lap, double *a_yuk, double *b, double dx, double dy, double dz, int P_low, double kappa, int threadID)
{
    int P = P_low+2; // to account for shifts due to derivatives
    int beg = P*P*P*threadID; //offset to beginning position of coefficients 
                              // for corresponding thread

    double R = sqrt(dx*dx+dy*dy+dz*dz);
    double R2 = R*R;
    double R3 = R2*R;

    int i,j,k;
    double C,C1,C2,Cb;
    
    b[beg+P*P*(0)+P*(0)+0] = exp(-kappa*R);
    a_yuk[beg+P*P*(0)+P*(0)+0] = b[beg+P*P*(0)+P*(0)+0]/R;

    a_lap[beg+P*P*(0)+P*(0)+0] = 1/R;

    // Two indices = 0

    b[beg+P*P*(1)+P*(0)+0] = -kappa * (dx*a_yuk[beg+P*P*(0)+P*(0)+0]); // 1,0,0
    b[beg+P*P*(0)+P*(1)+0] = -kappa * (dy*a_yuk[beg+P*P*(0)+P*(0)+0]); // 0,1,0
    b[beg+P*P*(0)+P*(0)+1] = -kappa * (dz*a_yuk[beg+P*P*(0)+P*(0)+0]); // 0,0,1

    a_yuk[beg+P*P*(1)+P*(0)+0] = -1/R2*(kappa*dx*b[beg+P*P*(0)+P*(0)+0]+dx*a_yuk[beg+P*P*(0)+P*(0)+0]);
    a_yuk[beg+P*P*(0)+P*(1)+0] = -1/R2*(kappa*dy*b[beg+P*P*(0)+P*(0)+0]+dy*a_yuk[beg+P*P*(0)+P*(0)+0]);
    a_yuk[beg+P*P*(0)+P*(0)+1] = -1/R2*(kappa*dz*b[beg+P*P*(0)+P*(0)+0]+dz*a_yuk[beg+P*P*(0)+P*(0)+0]);

    a_lap[beg+P*P*(1)+P*(0)+0] = -dx/R3;
    a_lap[beg+P*P*(0)+P*(1)+0] = -dy/R3;
    a_lap[beg+P*P*(0)+P*(0)+1] = -dz/R3;

    for (i=2; i<P+1; i++)
    {
        Cb   = -kappa/i;
        C    = 1./(i*R2);
        b    [beg+P*P*(i)+P*(0)+0] = Cb * (dx*a_yuk[beg+P*P*(i-1)+P*(0)+0] + a_yuk[beg+P*P*(i-2)+P*(0)+0]);
        a_yuk[beg+P*P*(i)+P*(0)+0] = C * ( -kappa*(dx*b[beg+P*P*(i-1)+P*(0)+0] + b[beg+P*P*(i-2)+P*(0)+0]) -(2*i-1)*dx*a_yuk[beg+P*P*(i-1)+P*(0)+0] - (i-1)*a_yuk[beg+P*P*(i-2)+P*(0)+0] );

        a_lap[beg+P*P*(i)+P*(0)+0] = C * ( -(2*i-1)*dx*a_lap[beg+P*P*(i-1)+P*(0)+0] - (i-1)*a_lap[beg+P*P*(i-2)+P*(0)+0] );

        b    [beg+P*P*(0)+P*(i)+0] = Cb * (dy*a_yuk[beg+P*P*(0)+P*(i-1)+0] + a_yuk[beg+P*P*(0)+P*(i-2)+0]);
        a_yuk[beg+P*P*(0)+P*(i)+0] = C * ( -kappa*(dy*b[beg+P*P*(0)+P*(i-1)+0] + b[beg+P*P*(0)+P*(i-2)+0]) -(2*i-1)*dy*a_yuk[beg+P*P*(0)+P*(i-1)+0] - (i-1)*a_yuk[beg+P*P*(0)+P*(i-2)+0] );
      
        a_lap[beg+P*P*(0)+P*(i)+0] = C * ( -(2*i-1)*dy*a_lap[beg+P*P*(0)+P*(i-1)+0] - (i-1)*a_lap[beg+P*P*(0)+P*(i-2)+0] );

        b    [beg+P*P*(0)+P*(0)+i] = Cb * (dz*a_yuk[beg+P*P*(0)+P*(0)+i-1] + a_yuk[beg+P*P*(0)+P*(0)+i-2]);
        a_yuk[beg+P*P*(0)+P*(0)+i] = C * ( -kappa*(dz*b[beg+P*P*(0)+P*(0)+i-1] + b[beg+P*P*(0)+P*(0)+i-2]) -(2*i-1)*dz*a_yuk[beg+P*P*(0)+P*(0)+i-1] - (i-1)*a_yuk[beg+P*P*(0)+P*(0)+i-2] );
        
        a_lap[beg+P*P*(0)+P*(0)+i] = C * ( -(2*i-1)*dz*a_lap[beg+P*P*(0)+P*(0)+i-1] - (i-1)*a_lap[beg+P*P*(0)+P*(0)+i-2] );
    }

    // One index = 0, one = 1 other >=1

    Cb   = -kappa/2;
   
    b    [beg+P*P*(1)+P*(1)+0] = Cb * (dx*a_yuk[beg+P*P*(0)+P*(1)+0] + dy*a_yuk[beg+P*P*(1)+P*(0)+0]);
    a_yuk[beg+P*P*(1)+P*(1)+0] = 1./(2*R2) * ( -kappa*(dx*b[beg+P*P*(0)+P*(1)+0]+dy*b[beg+P*P*(1)+P*(0)+0]) -(2*2-1)*(dx*a_yuk[beg+P*P*(0)+P*(1)+0]+dy*a_yuk[beg+P*P*(1)+P*(0)+0]) );

    a_lap[beg+P*P*(1)+P*(1)+0] = 1./(2*R2) * ( -(2*2-1)*(dx*a_lap[beg+P*P*(0)+P*(1)+0]+dy*a_lap[beg+P*P*(1)+P*(0)+0]) );

    b[beg+P*P*(1)+P*(0)+1] = Cb * (dx*a_yuk[beg+P*P*(0)+P*(0)+1] + dz*a_yuk[beg+P*P*(1)+P*(0)+0]);
    a_yuk[beg+P*P*(1)+P*(0)+1] = 1./(2*R2) * ( -kappa*(dx*b[beg+P*P*(0)+P*(0)+1]+dz*b[beg+P*P*(1)+P*(0)+0]) -(2*2-1)*(dx*a_yuk[beg+P*P*(0)+P*(0)+1]+dz*a_yuk[beg+P*P*(1)+P*(0)+0]) );

    a_lap[beg+P*P*(1)+P*(0)+1] = 1./(2*R2) * ( -(2*2-1)*(dx*a_lap[beg+P*P*(0)+P*(0)+1]+dz*a_lap[beg+P*P*(1)+P*(0)+0]) );

    b[beg+P*P*(0)+P*(1)+1] = Cb * (dy*a_yuk[beg+P*P*(0)+P*(0)+1] + dz*a_yuk[beg+P*P*(0)+P*(1)+0]);
    a_yuk[beg+P*P*(0)+P*(1)+1] = 1./(2*R2) * ( -kappa*(dy*b[beg+P*P*(0)+P*(0)+1]+dz*b[beg+P*P*(0)+P*(1)+0]) -(2*2-1)*(dy*a_yuk[beg+P*P*(0)+P*(0)+1]+dz*a_yuk[beg+P*P*(0)+P*(1)+0]) );

    a_lap[beg+P*P*(0)+P*(1)+1] = 1./(2*R2) * ( -(2*2-1)*(dy*a_lap[beg+P*P*(0)+P*(0)+1]+dz*a_lap[beg+P*P*(0)+P*(1)+0]) );

    for (i=2; i<P; i++)
    {
        Cb   = -kappa/(i+1);
        C    = 1./((1+i)*R2);

        b[beg+P*P*(1)+P*(i)+0] = Cb * (dx*a_yuk[beg+P*P*(0)+P*(i)+0] + dy*a_yuk[beg+P*P*(1)+P*(i-1)+0] + a_yuk[beg+P*P*(1)+P*(i-2)+0]);
        a_yuk[beg+P*P*(1)+P*(i)+0] = C * ( -kappa*(dx*b[beg+P*P*(0)+P*(i)+0]+dy*b[beg+P*P*(1)+P*(i-1)+0]+b[beg+P*P*(1)+P*(i-2)+0]) -(2*(1+i)-1)*(dx*a_yuk[beg+P*P*(0)+P*(i)+0]+dy*a_yuk[beg+P*P*(1)+P*(i-1)+0]) - (1+i-1)*(a_yuk[beg+P*P*(1)+P*(i-2)+0]) );

        a_lap[beg+P*P*(1)+P*(i)+0] = C * ( -(2*(1+i)-1)*(dx*a_lap[beg+P*P*(0)+P*(i)+0]+dy*a_lap[beg+P*P*(1)+P*(i-1)+0]) - (1+i-1)*(a_lap[beg+P*P*(1)+P*(i-2)+0]) );

        b[beg+P*P*(1)+P*(0)+i] = Cb * (dx*a_yuk[beg+P*P*(0)+P*(0)+i] + dz*a_yuk[beg+P*P*(1)+P*(0)+i-1] + a_yuk[beg+P*P*(1)+P*(0)+i-2]);
        a_yuk[beg+P*P*(1)+P*(0)+i] = C * ( -kappa*(dx*b[beg+P*P*(0)+P*(0)+i]+dz*b[beg+P*P*(1)+P*(0)+i-1]+b[beg+P*P*(1)+P*(0)+i-2]) -(2*(1+i)-1)*(dx*a_yuk[beg+P*P*(0)+P*(0)+i]+dz*a_yuk[beg+P*P*(1)+P*(0)+i-1]) - (1+i-1)*(a_yuk[beg+P*P*(1)+P*(0)+i-2]) );

        a_lap[beg+P*P*(1)+P*(0)+i] = C * ( -(2*(1+i)-1)*(dx*a_lap[beg+P*P*(0)+P*(0)+i]+dz*a_lap[beg+P*P*(1)+P*(0)+i-1]) - (1+i-1)*(a_lap[beg+P*P*(1)+P*(0)+i-2]) );

        b[beg+P*P*(0)+P*(1)+i] = Cb * (dy*a_yuk[beg+P*P*(0)+P*(0)+i] + dz*a_yuk[beg+P*P*(0)+P*(1)+i-1] + a_yuk[beg+P*P*(0)+P*(1)+i-2]);
        a_yuk[beg+P*P*(0)+P*(1)+i] = C * ( -kappa*(dy*b[beg+P*P*(0)+P*(0)+i]+dz*b[beg+P*P*(0)+P*(1)+i-1]+b[beg+P*P*(0)+P*(1)+i-2]) -(2*(1+i)-1)*(dy*a_yuk[beg+P*P*(0)+P*(0)+i]+dz*a_yuk[beg+P*P*(0)+P*(1)+i-1]) - (1+i-1)*(a_yuk[beg+P*P*(0)+P*(1)+i-2]) );

        a_lap[beg+P*P*(0)+P*(1)+i] = C * ( -(2*(1+i)-1)*(dy*a_lap[beg+P*P*(0)+P*(0)+i]+dz*a_lap[beg+P*P*(0)+P*(1)+i-1]) - (1+i-1)*(a_lap[beg+P*P*(0)+P*(1)+i-2]) );

        b[beg+P*P*(i)+P*(1)+0] = Cb * (dy*a_yuk[beg+P*P*(i)+P*(0)+0] + dx*a_yuk[beg+P*P*(i-1)+P*(1)+0] + a_yuk[beg+P*P*(i-2)+P*(1)+0]);
        a_yuk[beg+P*P*(i)+P*(1)+0] = C * ( -kappa*(dy*b[beg+P*P*(i)+P*(0)+0]+dx*b[beg+P*P*(i-1)+P*(1)+0]+b[beg+P*P*(i-2)+P*(1)+0]) -(2*(1+i)-1)*(dy*a_yuk[beg+P*P*(i)+P*(0)+0]+dx*a_yuk[beg+P*P*(i-1)+P*(1)+0]) - (1+i-1)*(a_yuk[beg+P*P*(i-2)+P*(1)+0]) );

        a_lap[beg+P*P*(i)+P*(1)+0] = C * ( -(2*(1+i)-1)*(dy*a_lap[beg+P*P*(i)+P*(0)+0]+dx*a_lap[beg+P*P*(i-1)+P*(1)+0]) - (1+i-1)*(a_lap[beg+P*P*(i-2)+P*(1)+0]) );

        b[beg+P*P*(i)+P*(0)+1] = Cb * (dz*a_yuk[beg+P*P*(i)+P*(0)+0] + dx*a_yuk[beg+P*P*(i-1)+P*(0)+1] + a_yuk[beg+P*P*(i-2)+P*(0)+1]);
        a_yuk[beg+P*P*(i)+P*(0)+1] = C * ( -kappa*(dz*b[beg+P*P*(i)+P*(0)+0]+dx*b[beg+P*P*(i-1)+P*(0)+1]+b[beg+P*P*(i-2)+P*(0)+1]) -(2*(1+i)-1)*(dz*a_yuk[beg+P*P*(i)+P*(0)+0]+dx*a_yuk[beg+P*P*(i-1)+P*(0)+1]) - (1+i-1)*(a_yuk[beg+P*P*(i-2)+P*(0)+1]) );
        
        a_lap[beg+P*P*(i)+P*(0)+1] = C * ( -(2*(1+i)-1)*(dz*a_lap[beg+P*P*(i)+P*(0)+0]+dx*a_lap[beg+P*P*(i-1)+P*(0)+1]) - (1+i-1)*(a_lap[beg+P*P*(i-2)+P*(0)+1]) );

        b[beg+P*P*(0)+P*(i)+1] = Cb * (dz*a_yuk[beg+P*P*(0)+P*(i)+0] + dy*a_yuk[beg+P*P*(0)+P*(i-1)+1] + a_yuk[beg+P*P*(0)+P*(i-2)+1]);
        a_yuk[beg+P*P*(0)+P*(i)+1] = C * ( -kappa*(dz*b[beg+P*P*(0)+P*(i)+0]+dy*b[beg+P*P*(0)+P*(i-1)+1]+b[beg+P*P*(0)+P*(i-2)+1]) -(2*(1+i)-1)*(dz*a_yuk[beg+P*P*(0)+P*(i)+0]+dy*a_yuk[beg+P*P*(0)+P*(i-1)+1]) - (1+i-1)*(a_yuk[beg+P*P*(0)+P*(i-2)+1]) );
        
        a_lap[beg+P*P*(0)+P*(i)+1] = C * ( -(2*(1+i)-1)*(dz*a_lap[beg+P*P*(0)+P*(i)+0]+dy*a_lap[beg+P*P*(0)+P*(i-1)+1]) - (1+i-1)*(a_lap[beg+P*P*(0)+P*(i-2)+1]) );
    }

    // One index 0, others >=2
    for (i=2; i<P+1; i++)
    {
        for (j=2; j<P+1-i; j++)
        {
            Cb   = -kappa/(i+j);
            C    = 1./((i+j)*R2);

            b[beg+P*P*(i)+P*(j)+0] = Cb * (dx*a_yuk[beg+P*P*(i-1)+P*(j)+0] + dy*a_yuk[beg+P*P*(i)+P*(j-1)+0] + a_yuk[beg+P*P*(i-2)+P*(j)+0] + a_yuk[beg+P*P*(i)+P*(j-2)+0]);
            a_yuk[beg+P*P*(i)+P*(j)+0] = C * ( -kappa*(dx*b[beg+P*P*(i-1)+P*(j)+0]+dy*b[beg+P*P*(i)+P*(j-1)+0]+b[beg+P*P*(i-2)+P*(j)+0]+b[beg+P*P*(i)+P*(j-2)+0]) -(2*(i+j)-1)*(dx*a_yuk[beg+P*P*(i-1)+P*(j)+0]+dy*a_yuk[beg+P*P*(i)+P*(j-1)+0]) -(i+j-1)*(a_yuk[beg+P*P*(i-2)+P*(j)+0]+a_yuk[beg+P*P*(i)+P*(j-2)+0]) );
            
            a_lap[beg+P*P*(i)+P*(j)+0] = C * ( -(2*(i+j)-1)*(dx*a_lap[beg+P*P*(i-1)+P*(j)+0]+dy*a_lap[beg+P*P*(i)+P*(j-1)+0]) -(i+j-1)*(a_lap[beg+P*P*(i-2)+P*(j)+0]+a_lap[beg+P*P*(i)+P*(j-2)+0]) );

            b[beg+P*P*(i)+P*(0)+j] = Cb * (dx*a_yuk[beg+P*P*(i-1)+P*(0)+j] + dz*a_yuk[beg+P*P*(i)+P*(0)+j-1] + a_yuk[beg+P*P*(i-2)+P*(0)+j] + a_yuk[beg+P*P*(i)+P*(0)+j-2]);
            a_yuk[beg+P*P*(i)+P*(0)+j] = C * ( -kappa*(dx*b[beg+P*P*(i-1)+P*(0)+j]+dz*b[beg+P*P*(i)+P*(0)+j-1]+b[beg+P*P*(i-2)+P*(0)+j]+b[beg+P*P*(i)+P*(0)+j-2]) -(2*(i+j)-1)*(dx*a_yuk[beg+P*P*(i-1)+P*(0)+j]+dz*a_yuk[beg+P*P*(i)+P*(0)+j-1]) -(i+j-1)*(a_yuk[beg+P*P*(i-2)+P*(0)+j]+a_yuk[beg+P*P*(i)+P*(0)+j-2]) );

            a_lap[beg+P*P*(i)+P*(0)+j] = C * ( -(2*(i+j)-1)*(dx*a_lap[beg+P*P*(i-1)+P*(0)+j]+dz*a_lap[beg+P*P*(i)+P*(0)+j-1]) -(i+j-1)*(a_lap[beg+P*P*(i-2)+P*(0)+j]+a_lap[beg+P*P*(i)+P*(0)+j-2]) );

            b[beg+P*P*(0)+P*(i)+j] = Cb * (dy*a_yuk[beg+P*P*(0)+P*(i-1)+j] + dz*a_yuk[beg+P*P*(0)+P*(i)+j-1] + a_yuk[beg+P*P*(0)+P*(i-2)+j] + a_yuk[beg+P*P*(0)+P*(i)+j-2]);
            a_yuk[beg+P*P*(0)+P*(i)+j] = C * ( -kappa*(dy*b[beg+P*P*(0)+P*(i-1)+j]+dz*b[beg+P*P*(0)+P*(i)+j-1]+b[beg+P*P*(0)+P*(i-2)+j]+b[beg+P*P*(0)+P*(i)+j-2]) -(2*(i+j)-1)*(dy*a_yuk[beg+P*P*(0)+P*(i-1)+j]+dz*a_yuk[beg+P*P*(0)+P*(i)+j-1]) -(i+j-1)*(a_yuk[beg+P*P*(0)+P*(i-2)+j]+a_yuk[beg+P*P*(0)+P*(i)+j-2]) );
            
            a_lap[beg+P*P*(0)+P*(i)+j] = C * ( -(2*(i+j)-1)*(dy*a_lap[beg+P*P*(0)+P*(i-1)+j]+dz*a_lap[beg+P*P*(0)+P*(i)+j-1]) -(i+j-1)*(a_lap[beg+P*P*(0)+P*(i-2)+j]+a_lap[beg+P*P*(0)+P*(i)+j-2]) );
        }
    }

    if (P>2)
    {
        // Two index = 1, other>=1
        Cb   = -kappa/3;

        b[beg+P*P*(1)+P*(1)+1] = Cb * (dx*a_yuk[beg+P*P*(0)+P*(1)+1] + dy*a_yuk[beg+P*P*(1)+P*(0)+1] + dz*a_yuk[beg+P*P*(1)+P*(1)+0]);
        a_yuk[beg+P*P*(1)+P*(1)+1] = 1/(3*R2) * ( -kappa*(dx*b[beg+P*P*(0)+P*(1)+1]+dy*b[beg+P*P*(1)+P*(0)+1]+dz*b[beg+P*P*(1)+P*(1)+0]) -5*(dx*a_yuk[beg+P*P*(0)+P*(1)+1]+dy*a_yuk[beg+P*P*(1)+P*(0)+1]+dz*a_yuk[beg+P*P*(1)+P*(1)+0]) );

        a_lap[beg+P*P*(1)+P*(1)+1] = 1/(3*R2) * ( -5*(dx*a_lap[beg+P*P*(0)+P*(1)+1]+dy*a_lap[beg+P*P*(1)+P*(0)+1]+dz*a_lap[beg+P*P*(1)+P*(1)+0]) );

        for (i=2; i<P-1; i++)
        {
            Cb   = -kappa/(2+i);
            C    = 1./((i+2)*R2);

            b[beg+P*P*(i)+P*(1)+1] = Cb * (dx*a_yuk[beg+P*P*(i-1)+P*(1)+1] + dy*a_yuk[beg+P*P*(i)+P*(0)+1] + dz*a_yuk[beg+P*P*(i)+P*(1)+0] + a_yuk[beg+P*P*(i-2)+P*(1)+1]);
            a_yuk[beg+P*P*(i)+P*(1)+1] = C * ( -kappa*(dx*b[beg+P*P*(i-1)+P*(1)+1]+dy*b[beg+P*P*(i)+P*(0)+1]+dz*b[beg+P*P*(i)+P*(1)+0]+b[beg+P*P*(i-2)+P*(1)+1]) -(2*(i+2)-1)*(dx*a_yuk[beg+P*P*(i-1)+P*(1)+1]+dy*a_yuk[beg+P*P*(i)+P*(0)+1]+dz*a_yuk[beg+P*P*(i)+P*(1)+0]) - (i+1)*(a_yuk[beg+P*P*(i-2)+P*(1)+1]) );
            
            a_lap[beg+P*P*(i)+P*(1)+1] = C * ( -(2*(i+2)-1)*(dx*a_lap[beg+P*P*(i-1)+P*(1)+1]+dy*a_lap[beg+P*P*(i)+P*(0)+1]+dz*a_lap[beg+P*P*(i)+P*(1)+0]) - (i+1)*(a_lap[beg+P*P*(i-2)+P*(1)+1]) );

            b[beg+P*P*(1)+P*(i)+1] = Cb * (dx*a_yuk[beg+P*P*(0)+P*(i)+1] + dy*a_yuk[beg+P*P*(1)+P*(i-1)+1] + dz*a_yuk[beg+P*P*(1)+P*(i)+0] + a_yuk[beg+P*P*(1)+P*(i-2)+1]);
            a_yuk[beg+P*P*(1)+P*(i)+1] = C * ( -kappa*(dx*b[beg+P*P*(0)+P*(i)+1]+dy*b[beg+P*P*(1)+P*(i-1)+1]+dz*b[beg+P*P*(1)+P*(i)+0]+b[beg+P*P*(1)+P*(i-2)+1]) -(2*(i+2)-1)*(dx*a_yuk[beg+P*P*(0)+P*(i)+1]+dy*a_yuk[beg+P*P*(1)+P*(i-1)+1]+dz*a_yuk[beg+P*P*(1)+P*(i)+0]) - (i+1)*(a_yuk[beg+P*P*(1)+P*(i-2)+1]) );
            
            a_lap[beg+P*P*(1)+P*(i)+1] = C * ( -(2*(i+2)-1)*(dx*a_lap[beg+P*P*(0)+P*(i)+1]+dy*a_lap[beg+P*P*(1)+P*(i-1)+1]+dz*a_lap[beg+P*P*(1)+P*(i)+0]) - (i+1)*(a_lap[beg+P*P*(1)+P*(i-2)+1]) );

            b[beg+P*P*(1)+P*(1)+i] = Cb * (dx*a_yuk[beg+P*P*(0)+P*(1)+i] + dy*a_yuk[beg+P*P*(1)+P*(0)+i] + dz*a_yuk[beg+P*P*(1)+P*(1)+i-1] + a_yuk[beg+P*P*(1)+P*(1)+i-2]);
            a_yuk[beg+P*P*(1)+P*(1)+i] = C * ( -kappa*(dx*b[beg+P*P*(0)+P*(1)+i]+dy*b[beg+P*P*(1)+P*(0)+i]+dz*b[beg+P*P*(1)+P*(1)+i-1]+b[beg+P*P*(1)+P*(1)+i-2]) -(2*(i+2)-1)*(dx*a_yuk[beg+P*P*(0)+P*(1)+i]+dy*a_yuk[beg+P*P*(1)+P*(0)+i]+dz*a_yuk[beg+P*P*(1)+P*(1)+i-1]) - (i+1)*(a_yuk[beg+P*P*(1)+P*(1)+i-2]) );
            
            a_lap[beg+P*P*(1)+P*(1)+i] = C * ( -(2*(i+2)-1)*(dx*a_lap[beg+P*P*(0)+P*(1)+i]+dy*a_lap[beg+P*P*(1)+P*(0)+i]+dz*a_lap[beg+P*P*(1)+P*(1)+i-1]) - (i+1)*(a_lap[beg+P*P*(1)+P*(1)+i-2]) );
        }
    }

    // One index = 1, others >=2
    if (P>4)
    {
        for (i=2; i<P-2; i++)
        {
            for (j=2; j<P-i; j++)
            {
                Cb = -kappa/(1+i+j);
                C  = 1./((1+i+j)*R2);
                C1 = -(2.*(1+i+j)-1);
                C2 = (i+j);

                b[beg+P*P*(1)+P*(i)+j] = Cb * (dx*a_yuk[beg+P*P*(0)+P*(i)+j] + dy*a_yuk[beg+P*P*(1)+P*(i-1)+j] + dz*a_yuk[beg+P*P*(1)+P*(i)+j-1] + a_yuk[beg+P*P*(1)+P*(i-2)+j] + a_yuk[beg+P*P*(1)+P*(i)+j-2]);
                a_yuk[beg+P*P*(1)+P*(i)+j] = C * ( -kappa*(dx*b[beg+P*P*(0)+P*(i)+j]+dy*b[beg+P*P*(1)+P*(i-1)+j]+dz*b[beg+P*P*(1)+P*(i)+j-1]+b[beg+P*P*(1)+P*(i-2)+j]+b[beg+P*P*(1)+P*(i)+j-2]) + C1*(dx*a_yuk[beg+P*P*(0)+P*(i)+j]+dy*a_yuk[beg+P*P*(1)+P*(i-1)+j]+dz*a_yuk[beg+P*P*(1)+P*(i)+j-1]) - C2*(a_yuk[beg+P*P*(1)+P*(i-2)+j]+a_yuk[beg+P*P*(1)+P*(i)+j-2]) );
                
                a_lap[beg+P*P*(1)+P*(i)+j] = C * ( C1*(dx*a_lap[beg+P*P*(0)+P*(i)+j]+dy*a_lap[beg+P*P*(1)+P*(i-1)+j]+dz*a_lap[beg+P*P*(1)+P*(i)+j-1]) - C2*(a_lap[beg+P*P*(1)+P*(i-2)+j]+a_lap[beg+P*P*(1)+P*(i)+j-2]) );

                b[beg+P*P*(i)+P*(1)+j] = Cb * (dx*a_yuk[beg+P*P*(i-1)+P*(1)+j] + dy*a_yuk[beg+P*P*(i)+P*(0)+j] + dz*a_yuk[beg+P*P*(i)+P*(1)+j-1] + a_yuk[beg+P*P*(i-2)+P*(1)+j] + a_yuk[beg+P*P*(i)+P*(1)+j-2]);
                a_yuk[beg+P*P*(i)+P*(1)+j] = C * ( -kappa*(dx*b[beg+P*P*(i-1)+P*(1)+j]+dy*b[beg+P*P*(i)+P*(0)+j]+dz*b[beg+P*P*(i)+P*(1)+j-1]+b[beg+P*P*(i-2)+P*(1)+j]+b[beg+P*P*(i)+P*(1)+j-2]) + C1*(dx*a_yuk[beg+P*P*(i-1)+P*(1)+j]+dy*a_yuk[beg+P*P*(i)+P*(0)+j]+dz*a_yuk[beg+P*P*(i)+P*(1)+j-1]) - C2*(a_yuk[beg+P*P*(i-2)+P*(1)+j]+a_yuk[beg+P*P*(i)+P*(1)+j-2]) );
                
                a_lap[beg+P*P*(i)+P*(1)+j] = C * ( C1*(dx*a_lap[beg+P*P*(i-1)+P*(1)+j]+dy*a_lap[beg+P*P*(i)+P*(0)+j]+dz*a_lap[beg+P*P*(i)+P*(1)+j-1]) - C2*(a_lap[beg+P*P*(i-2)+P*(1)+j]+a_lap[beg+P*P*(i)+P*(1)+j-2]) );

                b[beg+P*P*(i)+P*(j)+1] = Cb * (dx*a_yuk[beg+P*P*(i-1)+P*(j)+1] + dy*a_yuk[beg+P*P*(i)+P*(j-1)+1] + dz*a_yuk[beg+P*P*(i)+P*(j)+0] + a_yuk[beg+P*P*(i-2)+P*(j)+1] + a_yuk[beg+P*P*(i)+P*(j-2)+1]);
                a_yuk[beg+P*P*(i)+P*(j)+1] = C * ( -kappa*(dx*b[beg+P*P*(i-1)+P*(j)+1]+dy*b[beg+P*P*(i)+P*(j-1)+1]+dz*b[beg+P*P*(i)+P*(j)+0]+b[beg+P*P*(i-2)+P*(j)+1]+b[beg+P*P*(i)+P*(j-2)+1]) + C1*(dx*a_yuk[beg+P*P*(i-1)+P*(j)+1]+dy*a_yuk[beg+P*P*(i)+P*(j-1)+1]+dz*a_yuk[beg+P*P*(i)+P*(j)+0]) - C2*(a_yuk[beg+P*P*(i-2)+P*(j)+1]+a_yuk[beg+P*P*(i)+P*(j-2)+1]) );
                
                a_lap[beg+P*P*(i)+P*(j)+1] = C * ( C1*(dx*a_lap[beg+P*P*(i-1)+P*(j)+1]+dy*a_lap[beg+P*P*(i)+P*(j-1)+1]+dz*a_lap[beg+P*P*(i)+P*(j)+0]) - C2*(a_lap[beg+P*P*(i-2)+P*(j)+1]+a_lap[beg+P*P*(i)+P*(j-2)+1]) );
            }
        }
    }

    // All indices >= 2
    if (P>5)
    {
        for (i=2;i<P-3;i++)
        {
            for (j=2;j<P-1-i;j++)
            {
                for (k=2;k<P+1-i-j;k++)
                {
                    Cb = -kappa/(i+j+k);
                    C  = 1./((i+j+k)*R2);
                    C1 = -(2.*(i+j+k)-1);
                    C2 = i+j+k-1.;

                    b[beg+P*P*(i)+P*(j)+k] = Cb * (dx*a_yuk[beg+P*P*(i-1)+P*(j)+k] + dy*a_yuk[beg+P*P*(i)+P*(j-1)+k] + dz*a_yuk[beg+P*P*(i)+P*(j)+k-1] + a_yuk[beg+P*P*(i-2)+P*(j)+k] + a_yuk[beg+P*P*(i)+P*(j-2)+k] + a_yuk[beg+P*P*(i)+P*(j)+k-2]);
                    a_yuk[beg+P*P*(i)+P*(j)+k] = C * ( -kappa*(dx*b[beg+P*P*(i-1)+P*(j)+k]+dy*b[beg+P*P*(i)+P*(j-1)+k]+dz*b[beg+P*P*(i)+P*(j)+k-1]+b[beg+P*P*(i-2)+P*(j)+k]+b[beg+P*P*(i)+P*(j-2)+k]+b[beg+P*P*(i)+P*(j)+k-2]) + C1*(dx*a_yuk[beg+P*P*(i-1)+P*(j)+k]+dy*a_yuk[beg+P*P*(i)+P*(j-1)+k]+dz*a_yuk[beg+P*P*(i)+P*(j)+k-1]) - C2*(a_yuk[beg+P*P*(i-2)+P*(j)+k]+a_yuk[beg+P*P*(i)+P*(j-2)+k]+a_yuk[beg+P*P*(i)+P*(j)+k-2]) );

                    a_lap[beg+P*P*(i)+P*(j)+k] = C * ( C1*(dx*a_lap[beg+P*P*(i-1)+P*(j)+k]+dy*a_lap[beg+P*P*(i)+P*(j-1)+k]+dz*a_lap[beg+P*P*(i)+P*(j)+k-1]) - C2*(a_lap[beg+P*P*(i-2)+P*(j)+k]+a_lap[beg+P*P*(i)+P*(j-2)+k]+a_lap[beg+P*P*(i)+P*(j)+k-2]) );
                }
            }
        }
    }
}

__global__ void direct_kernel(double *peng1, double *peng2, int *direct_off, 
            int *direct_list_beg, int *direct_list_end, int *sizeTar, int *twig_beg,
            double *x, double *y, double *z, double *m1, double *m2, 
            double *nx, double *ny, double *nz, double *Area, 
	     double kappa, double Ehat)
{
    int I = threadIdx.x + twig_beg[blockIdx.x]-1; // -1 fortran to C indexing
//    for (int ii=0; ii<blockIdx.x; ii++)
//    {
//        I += sizeTar[ii];
//    }

//    int I = threadIdx.x + blockIdx.x*NCRIT;
    int list_start = direct_off[blockIdx.x]-1; // fortran to C indexing
    int list_end   = direct_off[blockIdx.x+1]-1; // -1 for fortran to C indexing

    double xi, yi, zi;
    double dx, dy, dz, nxi, nyi, nzi;
    double sum_peng1, sum_peng2;
    double G0, Gk, L1, L2, L3, L4;
    double G1, G2, G3, G4, G10, G20;

    __shared__ double xj_sh[BSZ], yj_sh[BSZ], zj_sh[BSZ], A_sh[BSZ], 
                      m1_sh[BSZ], m2_sh[BSZ], nx_sh[BSZ], ny_sh[BSZ], nz_sh[BSZ];

    int i, CJ_start, Nsrc, BpT;
    BpT = sizeTar[blockIdx.x]/BSZ+1;

    for (int iblock=0; iblock<BpT; iblock++)
    {
        sum_peng1 = 0.; 
        sum_peng2 = 0.;
        i  = I + iblock*BSZ;

        xi = x[i]; // collocation point
        yi = y[i];
        zi = z[i];
        nxi = nx[i]; // normal at collocation panel
        nyi = ny[i];
        nzi = nz[i];

        for (int lst=list_start; lst<list_end; lst++)
        {
            CJ_start = direct_list_beg[lst]-1 ;//-1 fortran to C indexing
            Nsrc = direct_list_end[lst] - CJ_start;//no need for-1 fortran
                                                // to C indexing, as it points
                                                // at 'end'in fortran which is
                                                // already one past end in C

            for(int jblock=0; jblock<(Nsrc-1)/BSZ; jblock++)
            {
                __syncthreads();
                xj_sh[threadIdx.x] = x[CJ_start + jblock*BSZ + threadIdx.x];
                yj_sh[threadIdx.x] = y[CJ_start + jblock*BSZ + threadIdx.x];
                zj_sh[threadIdx.x] = z[CJ_start + jblock*BSZ + threadIdx.x];
                m1_sh[threadIdx.x] = m1[CJ_start + jblock*BSZ + threadIdx.x];
                m2_sh[threadIdx.x] = m2[CJ_start + jblock*BSZ + threadIdx.x];
                nx_sh[threadIdx.x] = nx[CJ_start + jblock*BSZ + threadIdx.x];
                ny_sh[threadIdx.x] = ny[CJ_start + jblock*BSZ + threadIdx.x];
                nz_sh[threadIdx.x] = nz[CJ_start + jblock*BSZ + threadIdx.x];
                A_sh[threadIdx.x]  = Area[CJ_start + jblock*BSZ + threadIdx.x];

                __syncthreads();

                if (threadIdx.x+iblock*BSZ<sizeTar[blockIdx.x])
                {
                    for (int j=0; j<BSZ; j++)
                    {
                        dx = xi - xj_sh[j];
                        dy = yi - yj_sh[j];
                        dz = zi - zj_sh[j];
                        G0 = rsqrt(dx*dx + dy*dy + dz*dz); // G0 is 1/r!!!!

                        //if (G0<1e-10)
                        //{
                        //    dx = 1.23456; G0=1/1.23456;
                       // }
                        if (G0<1e10)
                        {
                            Gk  = exp(-kappa*1/G0);  // partial Gk
                            G10 = -(dx*nxi+dy*nyi+dz*nzi)*G0*G0*G0;// minus sign because derivative is on collocation point
                            G20 = G10 * Gk * (1 + kappa/G0); 

                            G1 = -(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j])*G0*G0*G0;// according to me this minus sign shouldnt be here, but this matches TABI-CPU
                            G2 = G1 * Gk * (1 + kappa/G0); 


                            G3  = G0*G0 * ( G0*(nxi*nx_sh[j]+nyi*ny_sh[j]+nzi*nz_sh[j]) + 3*G10*(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j]) ); // sign difference with TABI-CPU in last term

                            G4  = G3*Gk*(1 + kappa/G0) + kappa*kappa*G10*Gk*(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j]);// sign difference with TABI-CPU in last term

                            Gk *= G0; // full Gk

                            L1 = G1-Ehat*G2;
                            L2 = G0-Gk;
                            L3 = G4-G3;
                            L4 = G10-G20/Ehat;
            
                            /*
                            L4 = -(dx*nxi+dy*nyi+dz*nzi)*G0*G0*G0;// G10 in TABI-CPUminus sign because derivative is on collocation point 
                            L1 = -(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j])*G0*G0*G0;// Corresponds to G1 in TABI-CPU. According to me this minus sign shouldnt be here, but this matches TABI-CPU.
                            L1 = L1 - Ehat*(L1 * Gk * (1 + kappa/G0)); // L1 = H1-Ehat*G2
                            L4 = L4 -  L4 * Gk * (1 + kappa/G0)/Ehat;
                            L3 = G0*G0 * ( G0*(nxi*nx_sh[j]+nyi*ny_sh[j]+nzi*nz_sh[j]) + 3*L4*(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j]) ); // G3 in TABI-CPU. Sign difference with TABI-CPU in last term
                            L3 = -L3 + L3*Gk*(1 + kappa/G0) + kappa*kappa*L4*Gk*(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j]);// sign difference with TABI-CPU in last term
                            Gk *= G0; // full Gk
                            L2 = G0-Gk;
                            */
                            

                            sum_peng1 += (L1*m1_sh[j] + L2*m2_sh[j])*A_sh[j];
                            sum_peng2 += (L3*m1_sh[j] + L4*m2_sh[j])*A_sh[j];
                        }
                    }
                }
            }
            __syncthreads();
            int jblock = (Nsrc-1)/BSZ;
            if (jblock*BSZ + threadIdx.x < Nsrc)
            {
                xj_sh[threadIdx.x] = x[CJ_start + jblock*BSZ + threadIdx.x];
                yj_sh[threadIdx.x] = y[CJ_start + jblock*BSZ + threadIdx.x];
                zj_sh[threadIdx.x] = z[CJ_start + jblock*BSZ + threadIdx.x];
                m1_sh[threadIdx.x] = m1[CJ_start + jblock*BSZ + threadIdx.x];
                m2_sh[threadIdx.x] = m2[CJ_start + jblock*BSZ + threadIdx.x];
                nx_sh[threadIdx.x] = nx[CJ_start + jblock*BSZ + threadIdx.x];
                ny_sh[threadIdx.x] = ny[CJ_start + jblock*BSZ + threadIdx.x];
                nz_sh[threadIdx.x] = nz[CJ_start + jblock*BSZ + threadIdx.x];
                A_sh[threadIdx.x]  = Area[CJ_start + jblock*BSZ + threadIdx.x];
            }
            __syncthreads();

            if (threadIdx.x+iblock*BSZ<sizeTar[blockIdx.x])
            {
                for (int j=0; j<Nsrc-(jblock*BSZ); j++)
                {
                    dx = xi - xj_sh[j];
                    dy = yi - yj_sh[j];
                    dz = zi - zj_sh[j];
                    G0 = rsqrt(dx*dx + dy*dy + dz*dz); // G0 is 1/r!!!!

                    if (G0<1e10)
                    {
                        Gk  = exp(-kappa*1/G0);  // partial Gk
                        G10 = -(dx*nxi+dy*nyi+dz*nzi)*G0*G0*G0; // minus sign because derivatice is on collocation
                        G20 = G10 * Gk * (1 + kappa/G0); 

                        G1 = -(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j])*G0*G0*G0; // according to me this minus sign shouldnt be there, but matches TABI-CPU
                        G2 = G1 * Gk * (1 + kappa/G0); 

                        G3  = G0*G0 * ( G0*(nxi*nx_sh[j]+nyi*ny_sh[j]+nzi*nz_sh[j]) + 3*G10*(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j]) ); // sign difference with TABI-CPU in last term

                        G4  = G3*Gk*(1 + kappa/G0) + kappa*kappa*G10*Gk*(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j]);// sign difference with TABI-CPU in last term

                        Gk *= G0; // full Gk

                        L1 = G1-Ehat*G2;
                        L2 = G0-Gk;
                        L3 = G4-G3;
                        L4 = G10-G20/Ehat;

                        /*
                        L4 = -(dx*nxi+dy*nyi+dz*nzi)*G0*G0*G0;// G10 in TABI-CPUminus sign because derivative is on collocation point 
                        L1 = -(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j])*G0*G0*G0;// Corresponds to G1 in TABI-CPU. According to me this minus sign shouldnt be here, but this matches TABI-CPU.
                        L1 = L1 - Ehat*(L1 * Gk * (1 + kappa/G0)); // L1 = H1-Ehat*G2
                        L4 = L4 -  L4 * Gk * (1 + kappa/G0)/Ehat;
                        L3 = G0*G0 * ( G0*(nxi*nx_sh[j]+nyi*ny_sh[j]+nzi*nz_sh[j]) + 3*L4*(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j]) ); // G3 in TABI-CPU. Sign difference with TABI-CPU in last term
                        L3 = -L3 + L3*Gk*(1 + kappa/G0) + kappa*kappa*L4*Gk*(dx*nx_sh[j]+dy*ny_sh[j]+dz*nz_sh[j]);// sign difference with TABI-CPU in last term
                        Gk *= G0; // full Gk
                        L2 = G0-Gk;
                        */

                        sum_peng1 += (L1*m1_sh[j] + L2*m2_sh[j])*A_sh[j];
                        sum_peng2 += (L3*m1_sh[j] + L4*m2_sh[j])*A_sh[j];
                    }
                }
            }
        }

        if (threadIdx.x+iblock*BSZ<sizeTar[blockIdx.x])
        {
            peng1[i] += sum_peng1/(4*M_PI);
            peng2[i] += sum_peng2/(4*M_PI);
        }
    }
}

__global__ void cluster_kernel_1D(double *peng1, double *peng2, int *twig_beg, int *sizeTar,
                double *x, double *y, double *z, int *cluster_off, double *der_cof_1D, int *kk1,
                int *kk2, int *kk3,
                double *cluster_x, double *cluster_y, double *cluster_z, double *cluster_ms,
                double *tchg1, double *tchg2, int *index_1Dx, int *index_1Dy, int *index_1Dz,
                int *index_3Dto1D,
                double *coeff_lap, double *coeff_yuk, double *b, 
                double Ehat, int P, int Nm, int Nm2, double kappa, int numpars, int n_cluster)
{
    int I = threadIdx.x + twig_beg[blockIdx.x]-1; // points at first target of
                                                // current twig 
                                                //-1 fortran to C indexing

    int CJ_start = cluster_off[blockIdx.x]-1; // points at beginning of interaction
                                                // list for current twig 
                                                // -1 from fortran to C indexing
    int Nmlt = cluster_off[blockIdx.x+1]-CJ_start-1; // number of boxes that interact 
                                                // with current twig
                                                //-1 fortran to C indexing

    int threadID = blockIdx.x*blockDim.x + threadIdx.x; // Unique thread ID

    double xi, yi, zi, dx, dy, dz, sum_peng1, sum_peng2;
    double peng1_aux[16], peng2_aux[16], tempq1[16], tempq2[16];
//    double coeff_lap[P+2][P+2][P+2], coeff_yuk[P+2][P+2][P+2];
    double L1, L2, L3, L4;
    int i, jblock, jjblock, L;
    int BpT = sizeTar[blockIdx.x]/BSZ+1;


    __shared__ double xc_sh[BSZ], yc_sh[BSZ], zc_sh[BSZ];
    __shared__ int kk1_sh[16], kk2_sh[16], kk3_sh[16];
    __shared__ double cluster_ms_sh[BSZ*16];

    extern __shared__ int  shared_data_dyn[];
    int *index_1Dx_sh = shared_data_dyn;
    int *index_1Dy_sh = (int*)&index_1Dx_sh[Nm];
    int *index_1Dz_sh = (int*)&index_1Dy_sh[Nm];
    double *der_cof_sh = (double*)&index_1Dz_sh[Nm];
    int *index_3Dto1D_sh = (int*)&der_cof_sh[16*Nm];
    
    for(jjblock=0; jjblock<(Nm-1)/BSZ; jjblock++)
    {
        index_1Dx_sh[jjblock*BSZ+threadIdx.x] = index_1Dx[jjblock*BSZ+threadIdx.x];
        index_1Dy_sh[jjblock*BSZ+threadIdx.x] = index_1Dy[jjblock*BSZ+threadIdx.x];
        index_1Dz_sh[jjblock*BSZ+threadIdx.x] = index_1Dz[jjblock*BSZ+threadIdx.x];

        #pragma unroll
        for(i=0; i<16; i++)
        {
            der_cof_sh[i*Nm + jjblock*BSZ+threadIdx.x] = der_cof_1D[i*Nm + jjblock*BSZ+threadIdx.x];
        }
    
    }
    jjblock=(Nm-1)/BSZ;
    if (jjblock*BSZ + threadIdx.x < Nm)
    {
        index_1Dx_sh[jjblock*BSZ+threadIdx.x] = index_1Dx[jjblock*BSZ+threadIdx.x];
        index_1Dy_sh[jjblock*BSZ+threadIdx.x] = index_1Dy[jjblock*BSZ+threadIdx.x];
        index_1Dz_sh[jjblock*BSZ+threadIdx.x] = index_1Dz[jjblock*BSZ+threadIdx.x];

        #pragma unroll
        for(i=0; i<16; i++)
        {
            der_cof_sh[i*Nm + jjblock*BSZ+threadIdx.x] = der_cof_1D[i*Nm + jjblock*BSZ+threadIdx.x];
        }
    }
    
    for(jjblock=0; jjblock<((P+3)*(P+3)*(P+3)-1)/BSZ; jjblock++)
    {
        index_3Dto1D_sh[jjblock*BSZ+threadIdx.x] = index_3Dto1D[jjblock*BSZ+threadIdx.x];
    }
    jjblock=((P+3)*(P+3)*(P+3)-1)/BSZ;
    if (jjblock*BSZ + threadIdx.x < (P+3)*(P+3)*(P+3))
    {
        index_3Dto1D_sh[jjblock*BSZ+threadIdx.x] = index_3Dto1D[jjblock*BSZ+threadIdx.x];
    }

    if (threadIdx.x<16)
    {
        kk1_sh[threadIdx.x] = kk1[threadIdx.x];
        kk2_sh[threadIdx.x] = kk2[threadIdx.x];
        kk3_sh[threadIdx.x] = kk3[threadIdx.x];
    }
    __syncthreads();
   
    for (int iblock=0; iblock<BpT; iblock++)
    {

        i  = I + iblock*BSZ;
        
        #pragma unroll
        for (int k=0; k<16; k++)
        {
            peng1_aux[k] = 0.0; 
            peng2_aux[k] = 0.0; 
            tempq1[k] = tchg1[k*numpars + i];
            tempq2[k] = tchg2[k*numpars + i];
        }

        xi = x[i]; // collocation point
        yi = y[i];
        zi = z[i];

        for(jblock=0; jblock<(Nmlt-1)/BSZ; jblock++)
        {
            __syncthreads();
            xc_sh[threadIdx.x] = cluster_x[CJ_start + jblock*BSZ + threadIdx.x];
            yc_sh[threadIdx.x] = cluster_y[CJ_start + jblock*BSZ + threadIdx.x];
            zc_sh[threadIdx.x] = cluster_z[CJ_start + jblock*BSZ + threadIdx.x];
    
            __syncthreads();

            for (int j=0; j<BSZ; j++)
            {
                // FOR THIS BLOCK OF CODE TO WORK, the if statemente three lines back has
                // to go in, after this block of code
                // Putting moments into shared memory
                __syncthreads();
                for(jjblock=0; jjblock<(Nm-1)/BSZ; jjblock++)
                {   
                    #pragma unroll
                    for (int k=0; k<16; k++)
                    {
                        cluster_ms_sh[k*BSZ+jjblock*BSZ+threadIdx.x] = cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+jjblock*BSZ+threadIdx.x];
                    }
                }
                jjblock = (Nm-1)/BSZ;
                if (jjblock*BSZ + threadIdx.x < Nm)
                {
                    #pragma unroll
                    for (int k=0; k<16; k++)
                    {
                        cluster_ms_sh[k*BSZ+jjblock*BSZ+threadIdx.x] = cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+jjblock*BSZ+threadIdx.x];
                    }
                }
                __syncthreads();
        
                if (threadIdx.x+iblock*BSZ<sizeTar[blockIdx.x])
                {
                    dx = xi - xc_sh[j];
                    dy = yi - yc_sh[j];
                    dz = zi - zc_sh[j];

                    //getCoeff(coeff_lap, coeff_yuk, b, dx, dy, dz, P, kappa, threadID);
                    getCoeff_1D_lap(coeff_lap, dx, dy, dz, P, Nm2, threadID, index_3Dto1D_sh);
                    getCoeff_1D_yuk(coeff_yuk, b, dx, dy, dz, P, Nm2, kappa, threadID, index_3Dto1D_sh);

                    #pragma unroll
                    for (int k=0; k<16; k++)
                    {
                        sum_peng1 = 0.; 
                        sum_peng2 = 0.;
                        for (int l=0; l<Nm; l++)
                        {
                            L = index_3Dto1D_sh[  (P+3)*(P+3)*(index_1Dx_sh[l]+kk1_sh[k])
                                               +       (P+3)*(index_1Dy_sh[l]+kk2_sh[k])
                                               +             (index_1Dz_sh[l]+kk3_sh[k])];

                            // CHECK IF DIFFERENT ORDERING (j,k,l) IS MORE EFFICIENT...
                            sum_peng1 += der_cof_sh[k*Nm + l]
                                         * coeff_lap[Nm2*threadID + L]
                                         * cluster_ms_sh[k*BSZ+l];
//                                         * cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+l];

                            sum_peng2 += der_cof_sh[k*Nm + l]
                                         * coeff_yuk[Nm2*threadID + L] 
                                         * cluster_ms_sh[k*BSZ+l];
//                                         * cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+l];
                        }   
                        peng1_aux[k] += tempq1[k] * sum_peng1;
                        peng2_aux[k] += tempq2[k] * sum_peng2;
                    }
                }
            }
        }
        __syncthreads();
        jblock = (Nmlt-1)/BSZ;
        if (jblock*BSZ + threadIdx.x < Nmlt)
        {
            xc_sh[threadIdx.x] = cluster_x[CJ_start + jblock*BSZ + threadIdx.x];
            yc_sh[threadIdx.x] = cluster_y[CJ_start + jblock*BSZ + threadIdx.x];
            zc_sh[threadIdx.x] = cluster_z[CJ_start + jblock*BSZ + threadIdx.x];
        }
        __syncthreads();

        for (int j=0; j<Nmlt-(jblock*BSZ); j++)
        {
            // FOR THIS BLOCK OF CODE TO WORK, the if statemente three lines back has
            // to go in, after this block of code
            // Putting moments into shared memory
            __syncthreads();
            for(jjblock=0; jjblock<(Nm-1)/BSZ; jjblock++)
            {   
                #pragma unroll
                for (int k=0; k<16; k++)
                {
                    cluster_ms_sh[k*BSZ+jjblock*BSZ+threadIdx.x] = cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+jjblock*BSZ+threadIdx.x];
                }
            }
            jjblock = (Nm-1)/BSZ;
            if (jjblock*BSZ + threadIdx.x < Nm)
            {
                #pragma unroll
                for (int k=0; k<16; k++)
                {
                    cluster_ms_sh[k*BSZ+jjblock*BSZ+threadIdx.x] = cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+jjblock*BSZ+threadIdx.x];
                }
            }
            __syncthreads();
               
            if (threadIdx.x+iblock*BSZ<sizeTar[blockIdx.x])
            {

                dx = xi - xc_sh[j];
                dy = yi - yc_sh[j];
                dz = zi - zc_sh[j];

                //getCoeff(coeff_lap, coeff_yuk, b, dx, dy, dz, P, kappa, threadID);
                getCoeff_1D_lap(coeff_lap, dx, dy, dz, P, Nm2, threadID, index_3Dto1D_sh);
                getCoeff_1D_yuk(coeff_yuk, b, dx, dy, dz, P, Nm2, kappa, threadID, index_3Dto1D_sh);

                #pragma unroll 
                for (int k=0; k<16; k++)
                {
                    sum_peng1 = 0;
                    sum_peng2 = 0;
                    for (int l=0; l<Nm; l++)
                    {
                        L = index_3Dto1D_sh[  (P+3)*(P+3)*(index_1Dx_sh[l]+kk1_sh[k])
                                           +       (P+3)*(index_1Dy_sh[l]+kk2_sh[k])
                                           +             (index_1Dz_sh[l]+kk3_sh[k])];

                        sum_peng1 += der_cof_sh[k*Nm + l]
                                     * coeff_lap[Nm2*threadID + L]
                                     * cluster_ms_sh[k*BSZ+l];
//                                     * cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+l];

                        sum_peng2 += der_cof_sh[k*Nm + l]
                                     * coeff_yuk[Nm2*threadID + L] 
                                     * cluster_ms_sh[k*BSZ+l];
//                                     * cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+l];
                    }   
                    peng1_aux[k] += tempq1[k] * sum_peng1;
                    peng2_aux[k] += tempq2[k] * sum_peng2;
                }
            }
        }
    
        if (threadIdx.x+iblock*BSZ<sizeTar[blockIdx.x])
        {
   
            L1 = peng1_aux[0] - peng2_aux[0];
            L2 = 0;
            L3 = 0;
            L4 = 0;
            #pragma unroll
            for (int k=0; k<3; k++)
            {
                L2 += Ehat*peng2_aux[k+1] - peng1_aux[k+1];
                L3 += -(peng1_aux[k+4] - 1/Ehat*peng2_aux[k+4]);
                L4 += peng2_aux[k+7] - peng1_aux[k+7]; 
                L4 += peng2_aux[k+10] - peng1_aux[k+10]; 
                L4 += peng2_aux[k+13] - peng1_aux[k+13]; 
            }           

            peng1[i] += L1+L2; 
            peng2[i] += L3+L4; 
        }
    }
}

__global__ void cluster_kernel(double *peng1, double *peng2, int *twig_beg, int *sizeTar,
                double *x, double *y, double *z, int *cluster_off, double *der_cof_1D, int *kk1,
                int *kk2, int *kk3,
                double *cluster_x, double *cluster_y, double *cluster_z, double *cluster_ms,
                double *tchg1, double *tchg2, int *index_1Dx, int *index_1Dy, int *index_1Dz,
                double *coeff_lap, double *coeff_yuk, double *b, 
                double Ehat, int P, int Nm, double kappa, int numpars, int n_cluster)
{
    int I = threadIdx.x + twig_beg[blockIdx.x]-1; // points at first target of
                                                // current twig 
                                                //-1 fortran to C indexing

    int CJ_start = cluster_off[blockIdx.x]-1; // points at beginning of interaction
                                                // list for current twig 
                                                // -1 from fortran to C indexing
    int Nmlt = cluster_off[blockIdx.x+1]-CJ_start-1; // number of boxes that interact 
                                                // with current twig
                                                //-1 fortran to C indexing

    int threadID = blockIdx.x*blockDim.x + threadIdx.x; // Unique thread ID

    double xi, yi, zi, dx, dy, dz, sum_peng1, sum_peng2;
    double peng1_aux[16], peng2_aux[16], tempq1[16], tempq2[16];
//    double coeff_lap[P+2][P+2][P+2], coeff_yuk[P+2][P+2][P+2];
    double L1, L2, L3, L4;
    int i, jblock, jjblock;
    int BpT = sizeTar[blockIdx.x]/BSZ+1;


    __shared__ double xc_sh[BSZ], yc_sh[BSZ], zc_sh[BSZ];//, cluster_moment_sh[BSZ][16];
    __shared__ int kk1_sh[16], kk2_sh[16], kk3_sh[16];
    //__shared__ double cluster_ms_sh[BSZ*16];

    extern __shared__ int  shared_data_dyn[];
    int *index_1Dx_sh = shared_data_dyn;
    int *index_1Dy_sh = (int*)&index_1Dx_sh[Nm];
    int *index_1Dz_sh = (int*)&index_1Dy_sh[Nm];
    double *der_cof_sh = (double*)&index_1Dz_sh[Nm];
    
    for(jjblock=0; jjblock<(Nm-1)/BSZ; jjblock++)
    {
        index_1Dx_sh[jjblock*BSZ+threadIdx.x] = index_1Dx[jjblock*BSZ+threadIdx.x];
        index_1Dy_sh[jjblock*BSZ+threadIdx.x] = index_1Dy[jjblock*BSZ+threadIdx.x];
        index_1Dz_sh[jjblock*BSZ+threadIdx.x] = index_1Dz[jjblock*BSZ+threadIdx.x];

        for(i=0; i<16; i++)
        {
            der_cof_sh[i*Nm + jjblock*BSZ+threadIdx.x] = der_cof_1D[i*Nm + jjblock*BSZ+threadIdx.x];
        }
    
    }
    jjblock=(Nm-1)/BSZ;
    if (jjblock*BSZ + threadIdx.x < Nm)
    {
        index_1Dx_sh[jjblock*BSZ+threadIdx.x] = index_1Dx[jjblock*BSZ+threadIdx.x];
        index_1Dy_sh[jjblock*BSZ+threadIdx.x] = index_1Dy[jjblock*BSZ+threadIdx.x];
        index_1Dz_sh[jjblock*BSZ+threadIdx.x] = index_1Dz[jjblock*BSZ+threadIdx.x];

        for(i=0; i<16; i++)
        {
            der_cof_sh[i*Nm + jjblock*BSZ+threadIdx.x] = der_cof_1D[i*Nm + jjblock*BSZ+threadIdx.x];
        }
    }

    if (threadIdx.x<16)
    {
        kk1_sh[threadIdx.x] = kk1[threadIdx.x];
        kk2_sh[threadIdx.x] = kk2[threadIdx.x];
        kk3_sh[threadIdx.x] = kk3[threadIdx.x];
    }
    
   
    for (int iblock=0; iblock<BpT; iblock++)
    {

        i  = I + iblock*BSZ;
        
        for (int k=0; k<16; k++)
        {
            peng1_aux[k] = 0.0; 
            peng2_aux[k] = 0.0; 
            tempq1[k] = tchg1[k*numpars + i];
            tempq2[k] = tchg2[k*numpars + i];
        }

        xi = x[i]; // collocation point
        yi = y[i];
        zi = z[i];

        for(jblock=0; jblock<(Nmlt-1)/BSZ; jblock++)
        {
            __syncthreads();
            xc_sh[threadIdx.x] = cluster_x[CJ_start + jblock*BSZ + threadIdx.x];
            yc_sh[threadIdx.x] = cluster_y[CJ_start + jblock*BSZ + threadIdx.x];
            zc_sh[threadIdx.x] = cluster_z[CJ_start + jblock*BSZ + threadIdx.x];
    
            __syncthreads();

            if (threadIdx.x+iblock*BSZ<sizeTar[blockIdx.x])
            {
                for (int j=0; j<BSZ; j++)
                {
                    /*
                    // FOR THIS BLOCK OF CODE TO WORK, the if statemente three lines back has
                    // to go in, after this block of code
                    // Putting moments into shared memory
                    __syncthreads();
                    for(jjblock=0; jjblock<(Nm-1)/BSZ; jjblock++)
                    {   
                        for (int k=0; k<16; k++)
                        {
                            cluster_ms_sh[k*BSZ+jjblock*BSZ+threadIdx.x] = cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+jjblock*BSZ+threadIdx.x];
                        }
                    }
                    jjblock = (Nm-1)/BSZ;
                    if (jjblock*BSZ + threadIdx.x < Nm)
                    {
                        for (int k=0; k<16; k++)
                        {
                            cluster_ms_sh[k*BSZ+jjblock*BSZ+threadIdx.x] = cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+jjblock*BSZ+threadIdx.x];
                        }
                    }
                    __syncthreads();
                    */
            
                    dx = xi - xc_sh[j];
                    dy = yi - yc_sh[j];
                    dz = zi - zc_sh[j];

                    //getCoeff(coeff_lap, coeff_yuk, b, dx, dy, dz, P, kappa, threadID);
                    getCoeff_2(coeff_lap, b, dx, dy, dz, P, 0.0, threadID);
                    getCoeff_2(coeff_yuk, b, dx, dy, dz, P, kappa, threadID);

                    for (int k=0; k<16; k++)
                    {
                        sum_peng1 = 0.; 
                        sum_peng2 = 0.;
                        for (int l=0; l<Nm; l++)
                        {
                            // CHECK IF DIFFERENT ORDERING (j,k,l) IS MORE EFFICIENT...
                            sum_peng1 += der_cof_sh[k*Nm + l]
                                         * coeff_lap[(P+3)*(P+3)*(P+3)*threadID + 
                                                     (P+3)*(P+3)*(index_1Dx_sh[l]+kk1_sh[k])+
                                                           (P+3)*(index_1Dy_sh[l]+kk2_sh[k])+
                                                                 (index_1Dz_sh[l]+kk3_sh[k])]
//                                         * cluster_ms_sh[k*BSZ+l];
                                         * cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+l];

                            sum_peng2 += der_cof_sh[k*Nm + l]
                                         * coeff_yuk[(P+3)*(P+3)*(P+3)*threadID + 
                                                     (P+3)*(P+3)*(index_1Dx_sh[l]+kk1_sh[k])+
                                                           (P+3)*(index_1Dy_sh[l]+kk2_sh[k])+
                                                                 (index_1Dz_sh[l]+kk3_sh[k])]
//                                         * cluster_ms_sh[k*BSZ+l];
                                         * cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+l];
                        }   
                        peng1_aux[k] += tempq1[k] * sum_peng1;
                        peng2_aux[k] += tempq2[k] * sum_peng2;
                    }
                }
            }
        }
        __syncthreads();
        jblock = (Nmlt-1)/BSZ;
        if (jblock*BSZ + threadIdx.x < Nmlt)
        {
            xc_sh[threadIdx.x] = cluster_x[CJ_start + jblock*BSZ + threadIdx.x];
            yc_sh[threadIdx.x] = cluster_y[CJ_start + jblock*BSZ + threadIdx.x];
            zc_sh[threadIdx.x] = cluster_z[CJ_start + jblock*BSZ + threadIdx.x];
        }
        __syncthreads();

        if (threadIdx.x+iblock*BSZ<sizeTar[blockIdx.x])
        {
            for (int j=0; j<Nmlt-(jblock*BSZ); j++)
            {
                /*
                // FOR THIS BLOCK OF CODE TO WORK, the if statemente three lines back has
                // to go in, after this block of code
                // Putting moments into shared memory
                __syncthreads();
                for(jjblock=0; jjblock<(Nm-1)/BSZ; jjblock++)
                {   
                    for (int k=0; k<16; k++)
                    {
                        cluster_ms_sh[k*BSZ+jjblock*BSZ+threadIdx.x] = cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+jjblock*BSZ+threadIdx.x];
                    }
                }
                jjblock = (Nm-1)/BSZ;
                if (jjblock*BSZ + threadIdx.x < Nm)
                {
                    for (int k=0; k<16; k++)
                    {
                        cluster_ms_sh[k*BSZ+jjblock*BSZ+threadIdx.x] = cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+jjblock*BSZ+threadIdx.x];
                    }
                }
                __syncthreads();
                */

                dx = xi - xc_sh[j];
                dy = yi - yc_sh[j];
                dz = zi - zc_sh[j];

                //getCoeff(coeff_lap, coeff_yuk, b, dx, dy, dz, P, kappa, threadID);
                getCoeff_2(coeff_lap, b, dx, dy, dz, P, 0.0, threadID);
                getCoeff_2(coeff_yuk, b, dx, dy, dz, P, kappa, threadID);

                for (int k=0; k<16; k++)
                {
                    sum_peng1 = 0;
                    sum_peng2 = 0;
                    for (int l=0; l<Nm; l++)
                    {
                       sum_peng1 += der_cof_sh[k*Nm + l]
                                     * coeff_lap[(P+3)*(P+3)*(P+3)*threadID + 
                                                 (P+3)*(P+3)*(index_1Dx_sh[l]+kk1_sh[k])+
                                                       (P+3)*(index_1Dy_sh[l]+kk2_sh[k])+
                                                             (index_1Dz_sh[l]+kk3_sh[k])]
//                                     * cluster_ms_sh[k*BSZ+l];
                                     * cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+l];

                        sum_peng2 += der_cof_sh[k*Nm + l]
                                     * coeff_yuk[(P+3)*(P+3)*(P+3)*threadID + 
                                                 (P+3)*(P+3)*(index_1Dx_sh[l]+kk1_sh[k])+
                                                       (P+3)*(index_1Dy_sh[l]+kk2_sh[k])+
                                                             (index_1Dz_sh[l]+kk3_sh[k])] 
//                                     * cluster_ms_sh[k*BSZ+l];
                                     * cluster_ms[k*Nm*(n_cluster-1) + (CJ_start+j+jblock*BSZ)*Nm+l];
                    }   
                    peng1_aux[k] += tempq1[k] * sum_peng1;
                    peng2_aux[k] += tempq2[k] * sum_peng2;
                }
            }
        }
    
        if (threadIdx.x+iblock*BSZ<sizeTar[blockIdx.x])
        {
   
            L1 = peng1_aux[0] - peng2_aux[0];
            L2 = 0;
            L3 = 0;
            L4 = 0;
            for (int k=0; k<3; k++)
            {
                L2 += Ehat*peng2_aux[k+1] - peng1_aux[k+1];
                L3 += -(peng1_aux[k+4] - 1/Ehat*peng2_aux[k+4]);
                L4 += peng2_aux[k+7] - peng1_aux[k+7]; 
                L4 += peng2_aux[k+10] - peng1_aux[k+10]; 
                L4 += peng2_aux[k+13] - peng1_aux[k+13]; 
            }           

            peng1[i] += L1+L2; 
            peng2[i] += L3+L4; 
        }
    }
}


__global__ void vec_add(double *a, int N)
{
    int idx = blockIdx.x*blockDim.x+threadIdx.x; 
    if (idx<N) 
        a[idx] += 2;// a[idx] + a[idx];
}

extern "C" void direct_gpu_(double **peng1, double **peng2, int **direct_off,
            int **direct_list_beg, int **direct_list_end, int **sizeTar, int **twig_beg,
            double **x, double **y, double **z, double **m1, double **m2, 
            double **nx, double **ny, double **nz, double **Area, int *numpars_p,
	    double *kappa_p, double *Ehat_p, int *NCRIT_p, int *Ntwig_p)
{
    double kappa = *kappa_p;
    double Ehat = *Ehat_p;
    int NCRIT = *NCRIT_p;
    int numpars = *numpars_p;
    //int BpT = int(ceil(NCRIT*1.0/BSZ));
    double Nround = *Ntwig_p * NCRIT * 1.0;
    int blocks = int(ceil(Nround/NCRIT));

    direct_kernel<<<blocks,BSZ>>>(*peng1, *peng2, *direct_off, 
                                 *direct_list_beg, *direct_list_end, *sizeTar, *twig_beg,
                                 *x, *y, *z, *m1, *m2, *nx, *ny, *nz, *Area, 
                                 kappa, Ehat);
}


extern "C" void cluster_gpu_(double **peng1, double **peng2, int **twig_beg, int **sizeTar,
            int **cluster_off, double **cluster_center_x, double **cluster_center_y, double **cluster_center_z, 
            double **cluster_ms, double **x, double **y, double **z, int **kk1,
            int **kk2, int **kk3, double **tchg1, double **tchg2, int **index_1Dx, int **index_1Dy, int **index_1Dz, int **index_3Dto1D, 
            double **der_cof_1D, double *kappa_p, int *Ntwig_p, double *Ehat_p, int *NCRIT_p, int *P_p, int *Nm, int*numpars_p, 
            int *n_cluster_p, double **coeff_lap, double **coeff_yuk, double **b)
{
    double kappa  = *kappa_p;
    double Ehat   = *Ehat_p;
    int    NCRIT  = *NCRIT_p;   
    //int    BpT    = int(ceil(NCRIT*1.0/BSZ));   
    double Nround = *Ntwig_p * NCRIT * 1.0;
    int blocks    = int(ceil(Nround/NCRIT));
    int P         = *P_p;
    int numpars   = *numpars_p;
    int n_cluster = *n_cluster_p;
    int Nm2       = (P+3)*(P+4)*(P+5)/6; // Nm for the shifted coeff (P+2, for derivatives) 
    /*
    int *test = (int*)malloc((P+3)*(P+3)*(P+3)*sizeof(int));
    int *test2 = (int*)malloc(*Nm*sizeof(int));
    cudaMemcpy(test, *index_3Dto1D, (P+3)*(P+3)*(P+3)*sizeof(int), cudaMemcpyDeviceToHost);
    cudaMemcpy(test2, *index_1Dx, *Nm*sizeof(int), cudaMemcpyDeviceToHost);

    printf("%i\n",*Nm);
    for (int i=0; i<*Nm; i++)
        printf("%i:%i\t",i,test2[i]);
    printf("\n");

    printf("%i\n",P);
    for (int i=0; i<P+3; i++)
    {
        for (int j=0; j<P+3; j++)
        {
            for (int k=0; k<P+3; k++)
            {
                if (i+j+k<P+3) 
                    printf("%i,%i,%i:%i\n",i,j,k,test[i*(P+3)*(P+3)+j*(P+3)+k]);
            }
        }
    }
    
   for (int i=2; i<P+2-2; i++)
   {
      for (int j=2; j<P+2-i+1; j++)
      {
          printf("%i,%i,1: %i\n", i,j,test[i*(P+3)*(P+3)+j*(P+3)+1]);
      }
    }
    exit(0);
    */
    
    int shared_mem = *Nm*(3*sizeof(int)+16*sizeof(double)) + (P+3)*(P+3)*(P+3)*sizeof(int);
    cluster_kernel_1D<<<blocks,BSZ,shared_mem>>>(*peng1, *peng2, *twig_beg, *sizeTar,
                                   *x, *y, *z, *cluster_off, *der_cof_1D, *kk1, *kk2, *kk3,
                                   *cluster_center_x, *cluster_center_y, *cluster_center_z, 
                                   *cluster_ms, *tchg1, *tchg2, *index_1Dx, *index_1Dy, *index_1Dz, 
                                   *index_3Dto1D, *coeff_lap, *coeff_yuk, *b, 
                                   Ehat, P, *Nm, Nm2, kappa, numpars, n_cluster);
/*
    int shared_mem = *Nm*(3*sizeof(int)+16*sizeof(double));
    cluster_kernel<<<blocks,BSZ,shared_mem>>>(*peng1, *peng2, *twig_beg, *sizeTar,
                                   *x, *y, *z, *cluster_off, *der_cof_1D, *kk1, *kk2, *kk3,
                                   *cluster_center_x, *cluster_center_y, *cluster_center_z, 
                                   *cluster_ms, *tchg1, *tchg2, *index_1Dx, *index_1Dy, *index_1Dz, 
                                   *coeff_lap, *coeff_yuk, *b, 
                                   Ehat, P, *Nm, kappa, numpars, n_cluster);
*/

}

extern "C"
void cuda_malloc_coeff_(double **a_lap, double **a_yuk, double **b, 
                        int *P_p, int *Ntwig_p, int *NCRIT_p)
{
    int    NCRIT  = *NCRIT_p;   
    double Nround = *Ntwig_p * NCRIT * 1.0;
    int blocks    = int(ceil(Nround/NCRIT));
    int P         = *P_p;
    int P_shift   = P+2; // P required for derivatives
    int Nm = (P_shift+1)*(P_shift+2)*(P_shift+3)/6;
    
    int size      = blocks*BSZ*Nm;
    //int size      = blocks*BSZ*(P_shift+1)*(P_shift+1)*(P_shift+1);
    // +1 because it goes from 0 to P_shift: P_shift+1 places

    cudaMalloc((void**) &(*a_lap), size*sizeof(double));
    cudaMalloc((void**) &(*a_yuk), size*sizeof(double));
    cudaMalloc((void**) &(*b),     size*sizeof(double));

//    cudaMemset(*a_lap, 0.0, size*sizeof(double));
//    cudaMemset(*a_yuk, 0.0, size*sizeof(double));
//    cudaMemset(*b    , 0.0, size*sizeof(double));
}

extern "C" 
void cuda_setdevicelimit_(int *size_p)
{
//    size_t size_lim; 
//    cudaDeviceGetLimit(&size_lim, cudaLimitMallocHeapSize);
//    printf("Heap size found to be %d\n",(int)size_lim);  
    // Size in Megabytes
    int size = *size_p;
    cudaDeviceSetLimit(cudaLimitMallocHeapSize, size*1024*1024);
}

extern "C" 
void cuda_malloc_int_(int **var, int *size_p)
{
    int size = *size_p;
    cudaMalloc((void**) &(*var), size);
}

extern "C" 
void cuda_malloc_real_(double **var, int *size_p)
{
    int size = *size_p;
    cudaMalloc((void**) &(*var), size);
}

extern "C" 
void cuda_memcpy_htd_int_(int **var_d, int *var_h, int *size_p)
{
    cudaMemcpy(*var_d, var_h, *size_p, cudaMemcpyHostToDevice);
}

extern "C" 
void cuda_memcpy_htd_real_(double **var_d, double *var_h, int *size_p)
{
    cudaMemcpy(*var_d, var_h, *size_p, cudaMemcpyHostToDevice);
}

extern "C" 
void cuda_memcpy_dth_int_(int *var_h, int **var_d, int *size_p)
{
    cudaMemcpy(var_h, *var_d, *size_p, cudaMemcpyDeviceToHost);
}

extern "C" 
void cuda_memcpy_dth_real_(double *var_h, double **var_d, int *size_p)
{
    cudaMemcpy(var_h, *var_d, *size_p, cudaMemcpyDeviceToHost);
}

extern "C" 
void cuda_free_int_(int **var)
{
    cudaFree(*var);
}

extern "C" 
void cuda_free_real_(double **var)
{
    cudaFree(*var);
}

