MODULE MOLECULE
!    real*8, parameter  :: pi = 3.141592653589793238462643383279d0

    integer :: natm, nspt, nface, nchr, nt

    real*8  :: eps, kappa, rds, eps0, eps1, para

    real*8,  dimension(:),     allocatable :: atmrad, atmchr
    real*8,  dimension(:,:),   allocatable :: atmpos , sptpos ,sptnrm, chrpos, chrpos_sph 
    real*8,  dimension(:,:,:), allocatable :: chgmnx
	
    integer, dimension(:),     allocatable :: natmaff, nsftype, mface !, natmsf, nsfatm,  npture, nsptno
    integer, dimension(:,:),   allocatable :: nvert

END MODULE MOLECULE

MODULE COMDATA   

   character(100) :: fname,pathname,den
   
   integer :: lenpath,lenfname 

   real*8, dimension(:), allocatable :: bvct,xvct,F_exa, xtemp
   real*8, dimension(:,:), allocatable :: amtrx
   integer, dimension(:), allocatable :: indx
!######################################################
integer,allocatable,dimension(:):: iwork,IGWK
real*8,allocatable,dimension(:):: rwork,sb,sx,RGWK
!######################################################
           
END MODULE COMDATA 


module bicg
integer nmax
real*8, dimension(:), allocatable:: sa
integer, dimension(:), allocatable:: ijka
end module bicg


!------------------------------------------------------------------
module treecode
use iso_c_binding

! r8 is 8-byte (double precision) real

      !INTEGER,PARAMETER :: r8=SELECTED_REAL_KIND(12)

! runtime parameters

      INTEGER :: numpars,order,maxparnode,iflag,forcedim,Ntwig,Ncell_inter, n_direct, n_cluster, Nm, Nm2
      INTEGER, dimension(:), allocatable:: twig_beg, twig_end
      INTEGER, dimension(:), allocatable:: exist_cell_ms
      REAL*8, dimension(:,:), allocatable:: twig_ctr
      REAL*8, dimension(:), allocatable:: twig_rad
      INTEGER, dimension(:,:), allocatable:: direct_list
      INTEGER, dimension(:), allocatable, target:: direct_list_beg, direct_list_end, sizeTar
      INTEGER, dimension(:), allocatable, target:: cluster_list, &
                                        direct_off, cluster_off
      REAL*8, dimension(:,:), allocatable:: cluster_center_list, &
                                            cluster_center_pack
      INTEGER, dimension(:,:), allocatable:: cluster_beg_list
      REAL*8, dimension(:,:,:,:,:), allocatable:: cluster_ms_beg 
      REAL*8, dimension(:,:), allocatable:: cluster_ms_pack 
      REAL*8, dimension(:), allocatable:: cluster_ms_pack_1D, &
                cluster_center_x, cluster_center_y, cluster_center_z
      INTEGER, dimension(:,:), allocatable:: index_1Dto3Dmap, index_1D_shift
      INTEGER, dimension(:,:,:), allocatable:: index_3Dto1Dmap
      
      !REAL(KIND=r8) :: theta 
      real*8 :: theta
! arrays for coordinates, charge, potential & force (tree and direct) 

      REAL*8,ALLOCATABLE,DIMENSION(:),TARGET :: x,y,z,q,nx,ny,nz,x_test
      !REAL(KIND=r8),ALLOCATABLE,DIMENSION(:) :: tpoten,dpoten
      !REAL*8,ALLOCATABLE,DIMENSION(:,:) :: tforce,dforce
      INTEGER,ALLOCATABLE,DIMENSION(:) :: orderind

! Pointers to GPU arrays
      type(c_ptr) :: x_d, y_d, z_d, m1_d, m2_d, nx_d, ny_d, nz_d, Area_d, &
                    peng1_d, peng2_d, direct_off_d, direct_list_beg_d, &
                    direct_list_end_d, sizeTar_d, twig_beg_d, tchg1_d, tchg2_d, &
                    index_1Dx_d, index_1Dy_d, index_1Dz_d, index_3Dto1D_d, &
                    kk1_d,kk2_d,kk3_d, der_cof_1D_d, cluster_ms_d, cluster_center_x_d, &
                    cluster_center_y_d, cluster_center_z_d, cluster_off_d, &
                    coeff_lap_d, coeff_yuk_d, b_d
                
        

! timing variables

      REAL*8 :: timebeg,timeend

! local variables

      !INTEGER :: i,j,err
      REAL*8 :: xyzminmax(6)
      REAL*8 :: t1,abserr,relerr,absinf_err,relinf_err
      REAL*8,DIMENSION(3) :: f_inferr,f_relinferr,t


      real*8, dimension(:), allocatable, target:: tr_area
      real*8, dimension(:,:), allocatable:: tr_xyz, tr_q
      real*8, dimension(:,:,:), allocatable:: tchg,schg
      real*8, dimension(:), allocatable:: tchg1, tchg2
      real*8, dimension(:,:,:,:), allocatable:: der_cof
      real*8, dimension(:), allocatable:: der_cof_1D
      integer, dimension(:,:), allocatable:: kk
!      integer, dimension(:), allocatable:: kk1_h
!      integer, dimension(:), allocatable:: kk2_h
!      integer, dimension(:), allocatable:: kk3_h
end module treecode

!MODULE CUDA_CONSTANTS
! Taken from http://www.culatools.com/blog/2012/03/02/cuda-and-fortran/
!    USE ISO_C_BINDING
!    ENUM, BIND(C)
!        ENUMERATOR :: cudaMemcpyHostToHost=0, &
!        cudaMemcpyHostToDevice, &
!        cudaMemcpyDeviceToHost, &
!        cudaMemcpyDeviceToDevice, &
!        cudaNotUsedInFortran
!    END ENUM
!END MODULE
!
!MODULE CUDA_MEMORY_MANAGEMENT
! Taken from http://www.culatools.com/blog/2012/03/02/cuda-and-fortran/
!    IMPLICIT NONE
!    INTERFACE
!        INTEGER(C_INT) FUNCTION CUDA_MALLOC(BUFFER, SZ) &
!        BIND(C,NAME="cudaMalloc")
!            USE ISO_C_BINDING
!            TYPE (C_PTR) :: BUFFER
!            INTEGER (C_SIZE_T), VALUE :: SZ
!        END FUNCTION
!    END INTERFACE
!    INTERFACE
!        FUNCTION CUDA_MEMCPY(DST,SRC,CO,KI) RESULT(R) &
!        BIND(C,NAME="cudaMemcpy")
!            USE CUDA_CONSTANTS
!            INTEGER (C_INT) :: R
!            TYPE (C_PTR), VALUE :: DST
!            TYPE (C_PTR), VALUE :: SRC
!            INTEGER (C_SIZE_T), VALUE :: CO
!            INTEGER (C_INT), VALUE :: KI
!        END FUNCTION
!    END INTERFACE
!    INTERFACE
!        FUNCTION CUDA_FREE(PTR) RESULT(R) &
!        BIND(C,NAME="cudaFree")
!            USE CUDA_CONSTANTS
!            INTEGER (C_INT) :: R
!            TYPE (C_PTR), VALUE :: PTR
!        END FUNCTION
!    END INTERFACE
!END MODULE

!MODULE CALL_KERNEL
!    USE ISO_C_BINDING
!    IMPLICIT NONE
!    INTERFACE
!        subroutine add_gpu(x_d, N) bind(C,name="add_gpu")
!        USE, intrinsic:: ISO_C_BINDING
!        REAL(c_float) :: x_d
!        INTEGER(C_INT), VALUE :: N
!    END SUBROUTINE add_gpu
!    END INTERFACE
!END MODULE
